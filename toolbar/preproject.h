/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: preproject.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef PREPROJECT_H
#define PREPROJECT_H

/* ------------------------------------------------------------------
// project all nary constraints of arity k or less to binary
// constraints which are malloc'd as needed. If BN = TRUE, then only
// the projection to pairs of variables where the second element is
// the target of the corresponding conditional probability are
// computed 
// ----------------------------------------------------------------*/
void Project2Binary(int k, int BN);


#endif
