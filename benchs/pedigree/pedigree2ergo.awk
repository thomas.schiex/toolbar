
# convert a pedigree in pre format into MPE in ergo format

# awk -f pedigree2ergo.awk pedigree.pre [number of alleles] [genotyping error rate] > pedigree.erg

# e.g. awk -f pedigree2ergo.awk eye.pre 6 0.05 > eye.erg

# there are two possible models depending on prior on allele frequency distribution
# depending on the value of equalfreq, a global variable defined later:
# equalfreq = 0 : every allele has a frequency prior equal to the number of typings containing this allele divided by the total number of typings
# equalfreq = 1 : every allele has the same frequency prior (uniform distribution)

function min(x,y) {
    if (x < y) return(x); 
    else return(y);
}

function max(x,y) {
    if (x > y) return(x); 
    else return(y);
}

BEGIN {
    equalfreq = 1;
    nballele = ARGV[2];
    ARGV[2] = "";
    errorg = ARGV[3];
    ARGV[3] = "";
    printf("/* NBALLELE=%d */\n", nballele);
    domsize = nballele * (nballele + 1) / 2;
    mendel = "";
    msize = 0;	    
    ifather = 0;
    for (i=1; i<= nballele; i++) {
	for (j=i; j <= nballele; j++) {
	    ifather++;
	    imother = 0;
	    for (k=1; k <= nballele; k++) {
		for (l=k; l <= nballele; l++) {
		    imother++;
		    if (i <= k) a = i "" k;
		    else a = k "" i;
		    if (i <= l) b = i "" l;
		    else b = l "" i;
		    if (j <= k) c = j "" k;
		    else c = k "" j;
		    if (j <= l) d = j "" l;
		    else d = l "" j;
		    pa = 0.25;
		    pb = 0.25;
		    pc = 0.25;
		    pd = 0.25;
		    ison = 0;
		    for (m=1; m <= nballele; m++) {
			for (n=m; n <= nballele; n++) {
			    ison++;
			    p = 0;
			    e = m "" n;
			    if (e == a) p += pa;
			    if (e == b) p += pb;
			    if (e == c) p += pc;
			    if (e == d) p += pd;
			    if (ison==1) mendel = mendel "" p;
			    else mendel = mendel " " p;
			    msize++;
			}
		    }
		    mendel = mendel "\n";
		}
	    }
	}
    }
#    print mendel;
#    print msize;

    num =0;
    Tot = 0;
}

FNR==NR && NF==7 {
    num++;
    name[num] = $2;
    pedigree[num] = $1;
    father[num] = $3;
    mother[num] = $4;
    sex[num] = $5;
    if ($6 <= $7) {
	allele1[num] = $6;
	allele2[num] = $7;
    } else {
	allele1[num] = $7;
	allele2[num] = $6;
    }
    Freq[allele1[num]]++;
    Freq[allele2[num]]++;
    if (allele1[num] > 0) Tot++;
}

END {
#    for (f in Freq) print f,Freq[f],Tot*2,Freq[f]/Tot/2;
    print num + Tot;
    printf("%d", domsize);
    for (i=2; i<=num; i++) {
	printf(" %d", domsize);
    }
    for (; i<=num + Tot; i++) {
	printf(" %d", 1);
    }
    print "";
    for (i=1; i<=num; i++) {
	if (father[i]!=0 && mother[i]!=0) {
	    print 2, father[i], mother[i];
	} else {
	    print 0;
	}
    }
    for (i=1; i<=num; i++) {
	if (allele1[i] > 0) {
	    print 1,i;
	}
    }
    for (i=1; i<=num; i++) {
	if (father[i] && mother[i]) {
	    print msize;
	    print mendel;
	} else {
	    print domsize;
	    printf("%g", (equalfreq)?(1/domsize):((Freq[1] * Freq[1]) / (Tot * Tot * 4)));
	    for (x=1;x<= nballele; x++) {
		for (y=x; y <= nballele; y++) {
		    if (x==1&&y==1) continue;
		    printf(" %g", (equalfreq)?(1/domsize):(((x!=y)?2:1) * (Freq[x] * Freq[y]) / (Tot * Tot * 4)));
		}
	    }
	    print "";
	    print "";
	}
    }
    count = 0;
    for (i=1; i<=num; i++) {
	if (allele1[i] > 0) {
	    count++;
	    print domsize;
	    for (m=1; m <= nballele; m++) {
		for (n=m; n <= nballele; n++) {
		    if (allele1[i] "" allele2[i] == m "" n) printf("%g\n", (1 - errorg));
		    else printf("%g\n", errorg / (domsize - 1));
		}
	    }
	}
    }
#    print Tot, count;
}
