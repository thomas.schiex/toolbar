#!/bin/sh

# Usage:
# runcplex.sh problem.mps timelimit

# location of cplex executable
cplex="/usr/local/Ilog/cplex91/bin/x86_rhel4.0_3.4/cplex"

# log filename 
name=`basename $1 .mps` 
logname="${name}.log"
logname="*" # do not save logfile

(echo "set logfile ${logname}" ; echo "set timelimit $2" ; echo "read $1 mps" ; echo "mipopt" ; echo "quit" ) | $cplex

# other cplex options: 
# force exact mip gaps ; exploit an upperbound ; lowerbound emphasis
# runcplex.sh problem.mps timelimit upperbound
#(echo "set mip tolerances mipgap 0" ; echo "set mip tolerances absmipgap 0.999" ; echo "set logfile ${logname}" ; echo "set timelimit $2" ; echo "set mip tolerances uppercutoff $3" ; echo "set mip emphasis 2" ; echo "read $1 mps" ; echo "mipopt" ; echo "quit" ) | $cplex
