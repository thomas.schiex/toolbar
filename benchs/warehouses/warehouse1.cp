19warehouses_20stores_50000
store0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store2 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store3 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store4 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store5 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store6 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store7 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store8 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store9 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store10 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store11 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store12 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store13 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store14 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store15 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store18 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
store19 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
warehouse0 0 1
soft(50000, warehouse0 == 0)
hard( implies(store0 == 0, warehouse0 == 1) )
hard( implies(store1 == 0, warehouse0 == 1) )
hard( implies(store2 == 0, warehouse0 == 1) )
hard( implies(store3 == 0, warehouse0 == 1) )
hard( implies(store4 == 0, warehouse0 == 1) )
hard( implies(store5 == 0, warehouse0 == 1) )
hard( implies(store6 == 0, warehouse0 == 1) )
hard( implies(store7 == 0, warehouse0 == 1) )
hard( implies(store8 == 0, warehouse0 == 1) )
hard( implies(store9 == 0, warehouse0 == 1) )
hard( implies(store10 == 0, warehouse0 == 1) )
hard( implies(store11 == 0, warehouse0 == 1) )
hard( implies(store12 == 0, warehouse0 == 1) )
hard( implies(store13 == 0, warehouse0 == 1) )
hard( implies(store14 == 0, warehouse0 == 1) )
hard( implies(store15 == 0, warehouse0 == 1) )
hard( implies(store16 == 0, warehouse0 == 1) )
hard( implies(store17 == 0, warehouse0 == 1) )
hard( implies(store18 == 0, warehouse0 == 1) )
hard( implies(store19 == 0, warehouse0 == 1) )
warehouse1 0 1
soft(50000, warehouse1 == 0)
hard( implies(store0 == 1, warehouse1 == 1) )
hard( implies(store1 == 1, warehouse1 == 1) )
hard( implies(store2 == 1, warehouse1 == 1) )
hard( implies(store3 == 1, warehouse1 == 1) )
hard( implies(store4 == 1, warehouse1 == 1) )
hard( implies(store5 == 1, warehouse1 == 1) )
hard( implies(store6 == 1, warehouse1 == 1) )
hard( implies(store7 == 1, warehouse1 == 1) )
hard( implies(store8 == 1, warehouse1 == 1) )
hard( implies(store9 == 1, warehouse1 == 1) )
hard( implies(store10 == 1, warehouse1 == 1) )
hard( implies(store11 == 1, warehouse1 == 1) )
hard( implies(store12 == 1, warehouse1 == 1) )
hard( implies(store13 == 1, warehouse1 == 1) )
hard( implies(store14 == 1, warehouse1 == 1) )
hard( implies(store15 == 1, warehouse1 == 1) )
hard( implies(store16 == 1, warehouse1 == 1) )
hard( implies(store17 == 1, warehouse1 == 1) )
hard( implies(store18 == 1, warehouse1 == 1) )
hard( implies(store19 == 1, warehouse1 == 1) )
warehouse2 0 1
soft(50000, warehouse2 == 0)
hard( implies(store0 == 2, warehouse2 == 1) )
hard( implies(store1 == 2, warehouse2 == 1) )
hard( implies(store2 == 2, warehouse2 == 1) )
hard( implies(store3 == 2, warehouse2 == 1) )
hard( implies(store4 == 2, warehouse2 == 1) )
hard( implies(store5 == 2, warehouse2 == 1) )
hard( implies(store6 == 2, warehouse2 == 1) )
hard( implies(store7 == 2, warehouse2 == 1) )
hard( implies(store8 == 2, warehouse2 == 1) )
hard( implies(store9 == 2, warehouse2 == 1) )
hard( implies(store10 == 2, warehouse2 == 1) )
hard( implies(store11 == 2, warehouse2 == 1) )
hard( implies(store12 == 2, warehouse2 == 1) )
hard( implies(store13 == 2, warehouse2 == 1) )
hard( implies(store14 == 2, warehouse2 == 1) )
hard( implies(store15 == 2, warehouse2 == 1) )
hard( implies(store16 == 2, warehouse2 == 1) )
hard( implies(store17 == 2, warehouse2 == 1) )
hard( implies(store18 == 2, warehouse2 == 1) )
hard( implies(store19 == 2, warehouse2 == 1) )
warehouse3 0 1
soft(50000, warehouse3 == 0)
hard( implies(store0 == 3, warehouse3 == 1) )
hard( implies(store1 == 3, warehouse3 == 1) )
hard( implies(store2 == 3, warehouse3 == 1) )
hard( implies(store3 == 3, warehouse3 == 1) )
hard( implies(store4 == 3, warehouse3 == 1) )
hard( implies(store5 == 3, warehouse3 == 1) )
hard( implies(store6 == 3, warehouse3 == 1) )
hard( implies(store7 == 3, warehouse3 == 1) )
hard( implies(store8 == 3, warehouse3 == 1) )
hard( implies(store9 == 3, warehouse3 == 1) )
hard( implies(store10 == 3, warehouse3 == 1) )
hard( implies(store11 == 3, warehouse3 == 1) )
hard( implies(store12 == 3, warehouse3 == 1) )
hard( implies(store13 == 3, warehouse3 == 1) )
hard( implies(store14 == 3, warehouse3 == 1) )
hard( implies(store15 == 3, warehouse3 == 1) )
hard( implies(store16 == 3, warehouse3 == 1) )
hard( implies(store17 == 3, warehouse3 == 1) )
hard( implies(store18 == 3, warehouse3 == 1) )
hard( implies(store19 == 3, warehouse3 == 1) )
warehouse4 0 1
soft(50000, warehouse4 == 0)
hard( implies(store0 == 4, warehouse4 == 1) )
hard( implies(store1 == 4, warehouse4 == 1) )
hard( implies(store2 == 4, warehouse4 == 1) )
hard( implies(store3 == 4, warehouse4 == 1) )
hard( implies(store4 == 4, warehouse4 == 1) )
hard( implies(store5 == 4, warehouse4 == 1) )
hard( implies(store6 == 4, warehouse4 == 1) )
hard( implies(store7 == 4, warehouse4 == 1) )
hard( implies(store8 == 4, warehouse4 == 1) )
hard( implies(store9 == 4, warehouse4 == 1) )
hard( implies(store10 == 4, warehouse4 == 1) )
hard( implies(store11 == 4, warehouse4 == 1) )
hard( implies(store12 == 4, warehouse4 == 1) )
hard( implies(store13 == 4, warehouse4 == 1) )
hard( implies(store14 == 4, warehouse4 == 1) )
hard( implies(store15 == 4, warehouse4 == 1) )
hard( implies(store16 == 4, warehouse4 == 1) )
hard( implies(store17 == 4, warehouse4 == 1) )
hard( implies(store18 == 4, warehouse4 == 1) )
hard( implies(store19 == 4, warehouse4 == 1) )
warehouse5 0 1
soft(50000, warehouse5 == 0)
hard( implies(store0 == 5, warehouse5 == 1) )
hard( implies(store1 == 5, warehouse5 == 1) )
hard( implies(store2 == 5, warehouse5 == 1) )
hard( implies(store3 == 5, warehouse5 == 1) )
hard( implies(store4 == 5, warehouse5 == 1) )
hard( implies(store5 == 5, warehouse5 == 1) )
hard( implies(store6 == 5, warehouse5 == 1) )
hard( implies(store7 == 5, warehouse5 == 1) )
hard( implies(store8 == 5, warehouse5 == 1) )
hard( implies(store9 == 5, warehouse5 == 1) )
hard( implies(store10 == 5, warehouse5 == 1) )
hard( implies(store11 == 5, warehouse5 == 1) )
hard( implies(store12 == 5, warehouse5 == 1) )
hard( implies(store13 == 5, warehouse5 == 1) )
hard( implies(store14 == 5, warehouse5 == 1) )
hard( implies(store15 == 5, warehouse5 == 1) )
hard( implies(store16 == 5, warehouse5 == 1) )
hard( implies(store17 == 5, warehouse5 == 1) )
hard( implies(store18 == 5, warehouse5 == 1) )
hard( implies(store19 == 5, warehouse5 == 1) )
warehouse6 0 1
soft(50000, warehouse6 == 0)
hard( implies(store0 == 6, warehouse6 == 1) )
hard( implies(store1 == 6, warehouse6 == 1) )
hard( implies(store2 == 6, warehouse6 == 1) )
hard( implies(store3 == 6, warehouse6 == 1) )
hard( implies(store4 == 6, warehouse6 == 1) )
hard( implies(store5 == 6, warehouse6 == 1) )
hard( implies(store6 == 6, warehouse6 == 1) )
hard( implies(store7 == 6, warehouse6 == 1) )
hard( implies(store8 == 6, warehouse6 == 1) )
hard( implies(store9 == 6, warehouse6 == 1) )
hard( implies(store10 == 6, warehouse6 == 1) )
hard( implies(store11 == 6, warehouse6 == 1) )
hard( implies(store12 == 6, warehouse6 == 1) )
hard( implies(store13 == 6, warehouse6 == 1) )
hard( implies(store14 == 6, warehouse6 == 1) )
hard( implies(store15 == 6, warehouse6 == 1) )
hard( implies(store16 == 6, warehouse6 == 1) )
hard( implies(store17 == 6, warehouse6 == 1) )
hard( implies(store18 == 6, warehouse6 == 1) )
hard( implies(store19 == 6, warehouse6 == 1) )
warehouse7 0 1
soft(50000, warehouse7 == 0)
hard( implies(store0 == 7, warehouse7 == 1) )
hard( implies(store1 == 7, warehouse7 == 1) )
hard( implies(store2 == 7, warehouse7 == 1) )
hard( implies(store3 == 7, warehouse7 == 1) )
hard( implies(store4 == 7, warehouse7 == 1) )
hard( implies(store5 == 7, warehouse7 == 1) )
hard( implies(store6 == 7, warehouse7 == 1) )
hard( implies(store7 == 7, warehouse7 == 1) )
hard( implies(store8 == 7, warehouse7 == 1) )
hard( implies(store9 == 7, warehouse7 == 1) )
hard( implies(store10 == 7, warehouse7 == 1) )
hard( implies(store11 == 7, warehouse7 == 1) )
hard( implies(store12 == 7, warehouse7 == 1) )
hard( implies(store13 == 7, warehouse7 == 1) )
hard( implies(store14 == 7, warehouse7 == 1) )
hard( implies(store15 == 7, warehouse7 == 1) )
hard( implies(store16 == 7, warehouse7 == 1) )
hard( implies(store17 == 7, warehouse7 == 1) )
hard( implies(store18 == 7, warehouse7 == 1) )
hard( implies(store19 == 7, warehouse7 == 1) )
warehouse8 0 1
soft(50000, warehouse8 == 0)
hard( implies(store0 == 8, warehouse8 == 1) )
hard( implies(store1 == 8, warehouse8 == 1) )
hard( implies(store2 == 8, warehouse8 == 1) )
hard( implies(store3 == 8, warehouse8 == 1) )
hard( implies(store4 == 8, warehouse8 == 1) )
hard( implies(store5 == 8, warehouse8 == 1) )
hard( implies(store6 == 8, warehouse8 == 1) )
hard( implies(store7 == 8, warehouse8 == 1) )
hard( implies(store8 == 8, warehouse8 == 1) )
hard( implies(store9 == 8, warehouse8 == 1) )
hard( implies(store10 == 8, warehouse8 == 1) )
hard( implies(store11 == 8, warehouse8 == 1) )
hard( implies(store12 == 8, warehouse8 == 1) )
hard( implies(store13 == 8, warehouse8 == 1) )
hard( implies(store14 == 8, warehouse8 == 1) )
hard( implies(store15 == 8, warehouse8 == 1) )
hard( implies(store16 == 8, warehouse8 == 1) )
hard( implies(store17 == 8, warehouse8 == 1) )
hard( implies(store18 == 8, warehouse8 == 1) )
hard( implies(store19 == 8, warehouse8 == 1) )
warehouse9 0 1
soft(50000, warehouse9 == 0)
hard( implies(store0 == 9, warehouse9 == 1) )
hard( implies(store1 == 9, warehouse9 == 1) )
hard( implies(store2 == 9, warehouse9 == 1) )
hard( implies(store3 == 9, warehouse9 == 1) )
hard( implies(store4 == 9, warehouse9 == 1) )
hard( implies(store5 == 9, warehouse9 == 1) )
hard( implies(store6 == 9, warehouse9 == 1) )
hard( implies(store7 == 9, warehouse9 == 1) )
hard( implies(store8 == 9, warehouse9 == 1) )
hard( implies(store9 == 9, warehouse9 == 1) )
hard( implies(store10 == 9, warehouse9 == 1) )
hard( implies(store11 == 9, warehouse9 == 1) )
hard( implies(store12 == 9, warehouse9 == 1) )
hard( implies(store13 == 9, warehouse9 == 1) )
hard( implies(store14 == 9, warehouse9 == 1) )
hard( implies(store15 == 9, warehouse9 == 1) )
hard( implies(store16 == 9, warehouse9 == 1) )
hard( implies(store17 == 9, warehouse9 == 1) )
hard( implies(store18 == 9, warehouse9 == 1) )
hard( implies(store19 == 9, warehouse9 == 1) )
warehouse10 0 1
soft(50000, warehouse10 == 0)
hard( implies(store0 == 10, warehouse10 == 1) )
hard( implies(store1 == 10, warehouse10 == 1) )
hard( implies(store2 == 10, warehouse10 == 1) )
hard( implies(store3 == 10, warehouse10 == 1) )
hard( implies(store4 == 10, warehouse10 == 1) )
hard( implies(store5 == 10, warehouse10 == 1) )
hard( implies(store6 == 10, warehouse10 == 1) )
hard( implies(store7 == 10, warehouse10 == 1) )
hard( implies(store8 == 10, warehouse10 == 1) )
hard( implies(store9 == 10, warehouse10 == 1) )
hard( implies(store10 == 10, warehouse10 == 1) )
hard( implies(store11 == 10, warehouse10 == 1) )
hard( implies(store12 == 10, warehouse10 == 1) )
hard( implies(store13 == 10, warehouse10 == 1) )
hard( implies(store14 == 10, warehouse10 == 1) )
hard( implies(store15 == 10, warehouse10 == 1) )
hard( implies(store16 == 10, warehouse10 == 1) )
hard( implies(store17 == 10, warehouse10 == 1) )
hard( implies(store18 == 10, warehouse10 == 1) )
hard( implies(store19 == 10, warehouse10 == 1) )
warehouse11 0 1
soft(50000, warehouse11 == 0)
hard( implies(store0 == 11, warehouse11 == 1) )
hard( implies(store1 == 11, warehouse11 == 1) )
hard( implies(store2 == 11, warehouse11 == 1) )
hard( implies(store3 == 11, warehouse11 == 1) )
hard( implies(store4 == 11, warehouse11 == 1) )
hard( implies(store5 == 11, warehouse11 == 1) )
hard( implies(store6 == 11, warehouse11 == 1) )
hard( implies(store7 == 11, warehouse11 == 1) )
hard( implies(store8 == 11, warehouse11 == 1) )
hard( implies(store9 == 11, warehouse11 == 1) )
hard( implies(store10 == 11, warehouse11 == 1) )
hard( implies(store11 == 11, warehouse11 == 1) )
hard( implies(store12 == 11, warehouse11 == 1) )
hard( implies(store13 == 11, warehouse11 == 1) )
hard( implies(store14 == 11, warehouse11 == 1) )
hard( implies(store15 == 11, warehouse11 == 1) )
hard( implies(store16 == 11, warehouse11 == 1) )
hard( implies(store17 == 11, warehouse11 == 1) )
hard( implies(store18 == 11, warehouse11 == 1) )
hard( implies(store19 == 11, warehouse11 == 1) )
warehouse12 0 1
soft(50000, warehouse12 == 0)
hard( implies(store0 == 12, warehouse12 == 1) )
hard( implies(store1 == 12, warehouse12 == 1) )
hard( implies(store2 == 12, warehouse12 == 1) )
hard( implies(store3 == 12, warehouse12 == 1) )
hard( implies(store4 == 12, warehouse12 == 1) )
hard( implies(store5 == 12, warehouse12 == 1) )
hard( implies(store6 == 12, warehouse12 == 1) )
hard( implies(store7 == 12, warehouse12 == 1) )
hard( implies(store8 == 12, warehouse12 == 1) )
hard( implies(store9 == 12, warehouse12 == 1) )
hard( implies(store10 == 12, warehouse12 == 1) )
hard( implies(store11 == 12, warehouse12 == 1) )
hard( implies(store12 == 12, warehouse12 == 1) )
hard( implies(store13 == 12, warehouse12 == 1) )
hard( implies(store14 == 12, warehouse12 == 1) )
hard( implies(store15 == 12, warehouse12 == 1) )
hard( implies(store16 == 12, warehouse12 == 1) )
hard( implies(store17 == 12, warehouse12 == 1) )
hard( implies(store18 == 12, warehouse12 == 1) )
hard( implies(store19 == 12, warehouse12 == 1) )
warehouse13 0 1
soft(50000, warehouse13 == 0)
hard( implies(store0 == 13, warehouse13 == 1) )
hard( implies(store1 == 13, warehouse13 == 1) )
hard( implies(store2 == 13, warehouse13 == 1) )
hard( implies(store3 == 13, warehouse13 == 1) )
hard( implies(store4 == 13, warehouse13 == 1) )
hard( implies(store5 == 13, warehouse13 == 1) )
hard( implies(store6 == 13, warehouse13 == 1) )
hard( implies(store7 == 13, warehouse13 == 1) )
hard( implies(store8 == 13, warehouse13 == 1) )
hard( implies(store9 == 13, warehouse13 == 1) )
hard( implies(store10 == 13, warehouse13 == 1) )
hard( implies(store11 == 13, warehouse13 == 1) )
hard( implies(store12 == 13, warehouse13 == 1) )
hard( implies(store13 == 13, warehouse13 == 1) )
hard( implies(store14 == 13, warehouse13 == 1) )
hard( implies(store15 == 13, warehouse13 == 1) )
hard( implies(store16 == 13, warehouse13 == 1) )
hard( implies(store17 == 13, warehouse13 == 1) )
hard( implies(store18 == 13, warehouse13 == 1) )
hard( implies(store19 == 13, warehouse13 == 1) )
warehouse14 0 1
soft(50000, warehouse14 == 0)
hard( implies(store0 == 14, warehouse14 == 1) )
hard( implies(store1 == 14, warehouse14 == 1) )
hard( implies(store2 == 14, warehouse14 == 1) )
hard( implies(store3 == 14, warehouse14 == 1) )
hard( implies(store4 == 14, warehouse14 == 1) )
hard( implies(store5 == 14, warehouse14 == 1) )
hard( implies(store6 == 14, warehouse14 == 1) )
hard( implies(store7 == 14, warehouse14 == 1) )
hard( implies(store8 == 14, warehouse14 == 1) )
hard( implies(store9 == 14, warehouse14 == 1) )
hard( implies(store10 == 14, warehouse14 == 1) )
hard( implies(store11 == 14, warehouse14 == 1) )
hard( implies(store12 == 14, warehouse14 == 1) )
hard( implies(store13 == 14, warehouse14 == 1) )
hard( implies(store14 == 14, warehouse14 == 1) )
hard( implies(store15 == 14, warehouse14 == 1) )
hard( implies(store16 == 14, warehouse14 == 1) )
hard( implies(store17 == 14, warehouse14 == 1) )
hard( implies(store18 == 14, warehouse14 == 1) )
hard( implies(store19 == 14, warehouse14 == 1) )
warehouse15 0 1
soft(50000, warehouse15 == 0)
hard( implies(store0 == 15, warehouse15 == 1) )
hard( implies(store1 == 15, warehouse15 == 1) )
hard( implies(store2 == 15, warehouse15 == 1) )
hard( implies(store3 == 15, warehouse15 == 1) )
hard( implies(store4 == 15, warehouse15 == 1) )
hard( implies(store5 == 15, warehouse15 == 1) )
hard( implies(store6 == 15, warehouse15 == 1) )
hard( implies(store7 == 15, warehouse15 == 1) )
hard( implies(store8 == 15, warehouse15 == 1) )
hard( implies(store9 == 15, warehouse15 == 1) )
hard( implies(store10 == 15, warehouse15 == 1) )
hard( implies(store11 == 15, warehouse15 == 1) )
hard( implies(store12 == 15, warehouse15 == 1) )
hard( implies(store13 == 15, warehouse15 == 1) )
hard( implies(store14 == 15, warehouse15 == 1) )
hard( implies(store15 == 15, warehouse15 == 1) )
hard( implies(store16 == 15, warehouse15 == 1) )
hard( implies(store17 == 15, warehouse15 == 1) )
hard( implies(store18 == 15, warehouse15 == 1) )
hard( implies(store19 == 15, warehouse15 == 1) )
warehouse16 0 1
soft(50000, warehouse16 == 0)
hard( implies(store0 == 16, warehouse16 == 1) )
hard( implies(store1 == 16, warehouse16 == 1) )
hard( implies(store2 == 16, warehouse16 == 1) )
hard( implies(store3 == 16, warehouse16 == 1) )
hard( implies(store4 == 16, warehouse16 == 1) )
hard( implies(store5 == 16, warehouse16 == 1) )
hard( implies(store6 == 16, warehouse16 == 1) )
hard( implies(store7 == 16, warehouse16 == 1) )
hard( implies(store8 == 16, warehouse16 == 1) )
hard( implies(store9 == 16, warehouse16 == 1) )
hard( implies(store10 == 16, warehouse16 == 1) )
hard( implies(store11 == 16, warehouse16 == 1) )
hard( implies(store12 == 16, warehouse16 == 1) )
hard( implies(store13 == 16, warehouse16 == 1) )
hard( implies(store14 == 16, warehouse16 == 1) )
hard( implies(store15 == 16, warehouse16 == 1) )
hard( implies(store16 == 16, warehouse16 == 1) )
hard( implies(store17 == 16, warehouse16 == 1) )
hard( implies(store18 == 16, warehouse16 == 1) )
hard( implies(store19 == 16, warehouse16 == 1) )
warehouse17 0 1
soft(50000, warehouse17 == 0)
hard( implies(store0 == 17, warehouse17 == 1) )
hard( implies(store1 == 17, warehouse17 == 1) )
hard( implies(store2 == 17, warehouse17 == 1) )
hard( implies(store3 == 17, warehouse17 == 1) )
hard( implies(store4 == 17, warehouse17 == 1) )
hard( implies(store5 == 17, warehouse17 == 1) )
hard( implies(store6 == 17, warehouse17 == 1) )
hard( implies(store7 == 17, warehouse17 == 1) )
hard( implies(store8 == 17, warehouse17 == 1) )
hard( implies(store9 == 17, warehouse17 == 1) )
hard( implies(store10 == 17, warehouse17 == 1) )
hard( implies(store11 == 17, warehouse17 == 1) )
hard( implies(store12 == 17, warehouse17 == 1) )
hard( implies(store13 == 17, warehouse17 == 1) )
hard( implies(store14 == 17, warehouse17 == 1) )
hard( implies(store15 == 17, warehouse17 == 1) )
hard( implies(store16 == 17, warehouse17 == 1) )
hard( implies(store17 == 17, warehouse17 == 1) )
hard( implies(store18 == 17, warehouse17 == 1) )
hard( implies(store19 == 17, warehouse17 == 1) )
warehouse18 0 1
soft(50000, warehouse18 == 0)
hard( implies(store0 == 18, warehouse18 == 1) )
hard( implies(store1 == 18, warehouse18 == 1) )
hard( implies(store2 == 18, warehouse18 == 1) )
hard( implies(store3 == 18, warehouse18 == 1) )
hard( implies(store4 == 18, warehouse18 == 1) )
hard( implies(store5 == 18, warehouse18 == 1) )
hard( implies(store6 == 18, warehouse18 == 1) )
hard( implies(store7 == 18, warehouse18 == 1) )
hard( implies(store8 == 18, warehouse18 == 1) )
hard( implies(store9 == 18, warehouse18 == 1) )
hard( implies(store10 == 18, warehouse18 == 1) )
hard( implies(store11 == 18, warehouse18 == 1) )
hard( implies(store12 == 18, warehouse18 == 1) )
hard( implies(store13 == 18, warehouse18 == 1) )
hard( implies(store14 == 18, warehouse18 == 1) )
hard( implies(store15 == 18, warehouse18 == 1) )
hard( implies(store16 == 18, warehouse18 == 1) )
hard( implies(store17 == 18, warehouse18 == 1) )
hard( implies(store18 == 18, warehouse18 == 1) )
hard( implies(store19 == 18, warehouse18 == 1) )
store0 0
0 68948
1 68948
2 68948
3 68948
4 35101
5 68948
6 24524
7 24524
8 35101
9 68948
10 26639
11 35101
12 68948
13 68948
14 68948
15 68948
16 68948
17 26639
18 35101
store1 0
0 15724
1 8634
2 17850
3 17850
4 23520
5 16433
6 46200
7 46200
8 23520
9 17850
10 46200
11 46200
12 17850
13 17850
14 17850
15 15724
16 16433
17 17850
18 23520
store2 0
0 24300
1 24300
2 12600
3 24300
4 60300
5 24300
6 60300
7 60300
8 60300
9 60300
10 60300
11 60300
12 31500
13 31500
14 31500
15 24300
16 24300
17 60300
18 60300
store3 0
0 4852
1 4852
2 4852
3 2817
4 4852
5 6104
6 10486
7 10486
8 10486
9 10486
10 10486
11 10486
12 4852
13 10486
14 6104
15 4852
16 4539
17 6104
18 4852
store4 0
0 40950
1 40950
2 78390
3 31590
4 16380
5 40950
6 40950
7 31590
8 40950
9 78390
10 31590
11 31590
12 29250
13 78390
14 40950
15 31590
16 40950
17 29250
18 28080
store5 0
0 66330
1 66330
2 66330
3 66330
4 34650
5 66330
6 13860
7 24750
8 26730
9 34650
10 26730
11 34650
12 34650
13 66330
14 34650
15 34650
16 66330
17 26730
18 34650
store6 0
0 39698
1 39698
2 39698
3 39698
4 15998
5 39698
6 14813
7 8295
8 20738
9 39698
10 14813
11 15998
12 20738
13 39698
14 20738
15 20738
16 39698
17 15998
18 15998
store7 0
0 45895
1 23975
2 45895
3 45895
4 23975
5 45895
6 18495
7 23975
8 9590
9 18495
10 23975
11 45895
12 45895
13 23975
14 18495
15 23975
16 45895
17 18495
18 45895
store8 0
0 5519
1 4387
2 9481
3 9481
4 9481
5 4387
6 5519
7 9481
8 4387
9 2547
10 9481
11 9481
12 5519
13 4104
14 4387
15 4387
16 5519
17 5519
18 9481
store9 0
0 53433
1 53433
2 53433
3 53433
4 21533
5 53433
6 21533
7 19938
8 27913
9 53433
10 11165
11 19938
12 27913
13 53433
14 53433
15 53433
16 53433
17 21533
18 21533
store10 0
0 47235
1 47235
2 47235
3 47235
4 19035
5 47235
6 24675
7 19035
8 47235
9 47235
10 17625
11 9870
12 24675
13 47235
14 47235
15 47235
16 47235
17 24675
18 19035
store11 0
0 13125
1 13125
2 14175
3 18375
4 18375
5 7350
6 35175
7 35175
8 35175
9 14175
10 35175
11 35175
12 18375
13 14175
14 14175
15 14175
16 14175
17 18375
18 35175
store12 0
0 138176
1 159783
2 181390
3 181390
4 239008
5 166985
6 469480
7 469480
8 239008
9 239008
10 469480
11 469480
12 181390
13 181390
14 181390
15 166985
16 166985
17 239008
18 239008
store13 0
0 106993
1 123723
2 140454
3 140454
4 185069
5 129300
6 363528
7 363528
8 185069
9 140454
10 363528
11 363528
12 140454
13 140454
14 140454
15 129300
16 129300
17 185069
18 185069
store14 0
0 55408
1 47915
2 62900
3 62900
4 82880
5 57905
6 162800
7 162800
8 82880
9 82880
10 162800
11 162800
12 62900
13 62900
14 62900
15 57905
16 57905
17 62900
18 82880
store15 0
0 55786
1 93864
2 106556
3 106556
4 140403
5 98095
6 275792
7 275792
8 140403
9 140403
10 275792
11 275792
12 106556
13 106556
14 106556
15 98095
16 98095
17 140403
18 140403
store16 0
0 16847
1 16847
2 19125
3 19125
4 25200
5 17607
6 49500
7 49500
8 25200
9 19125
10 49500
11 49500
12 19125
13 19125
14 19125
15 17607
16 19125
17 25200
18 25200
store17 0
0 27780
1 24308
2 31253
3 31253
4 31253
5 28938
6 77553
7 77553
8 40513
9 40513
10 77553
11 77553
12 31253
13 31253
14 31253
15 28938
16 28938
17 31253
18 40513
store18 0
0 4278
1 3983
2 4573
3 4573
4 4573
5 4573
6 5753
7 5753
8 5753
9 4573
10 9883
11 9883
12 4573
13 4573
14 4278
15 2655
16 4573
17 4573
18 5753
store19 0
0 9672
1 9005
2 12340
3 12340
4 9672
5 9672
6 12340
7 12340
8 9672
9 9672
10 22345
11 22345
12 9672
13 9672
14 8671
15 8671
16 9672
17 9672
18 12340
