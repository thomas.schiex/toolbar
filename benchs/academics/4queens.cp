# problem name (and possibly global upper-bound)
4-QUEENS

# variables with their explicit domains
queen_row1 1 2 3 4
queen_row2 1 2 3 4
queen_row3 1 2 3 4
queen_row4 1 2 3 4

# constraints defined by a formula or by a list of tuples

hard( alldiff(queen_row1, queen_row2, queen_row3, queen_row4) )

hard( abs(queen_row1 - queen_row2) != 1 )
hard( abs(queen_row1 - queen_row3) != 2 )

# hard( abs(queen_row1 - queen_row4) != 3 )
# is equivalent to:
queen_row1 queen_row4 0
1 4 -1
4 1 -1

hard( abs(queen_row2 - queen_row3) != 1 )
hard( abs(queen_row2 - queen_row4) != 2 )
hard( abs(queen_row3 - queen_row4) != 1 )
