
# Translator from ds format to cp format

# Usage: awk -f ds2cp.awk problem.ds > problem.cp

# See the file ds_format.txt for a description of the ds format

# Warning! constraints in intension are not allowed
# Warning! domain values should be integer values only
# Warning! instancy block (like "% 0 constraint names") is ignored
#          (i.e. all the constraints are selected)

function perror(i,message) {
  print message;
  print "line " NR ": " $0;
  error = i;
  exit(i);
}

BEGIN {
    variablemode = 0;
    constraintmode = 0;
    softmode = 0;
    tabumode = 0;
    tuplemode = 0;
}

/^[%] / {
    exit(0);
}

!/\#/ && tuplemode {
    print $0,cost;
}

constraintmode && !/ extension / {
    perror(1, "Error: cannot translate constraint in intension!");
}

constraintmode && / extension / {
    if (softmode) {
	if ($2 == "top") {
	    cost = -1;
	} else {
	    cost = $2;
	}
	pos = 4;
    } else {
	cost = 1;
	pos = 3;
    }
    for (i = pos; i <= NF; i++) {
	printf("_%s ", $i);
    }
    if (tabumode) {
	print 0;
    } else {
	print cost;
	cost = 0;
    }
    constraintmode = 0;
    tuplemode = 1;
}

/\#/ {
    variablemode = 0;
    constraintmode = 1;
    tuplemode = 0;
}

variablemode {
    print "_" $0;
}

NR == 1 {
    if (match($2,"valued")) {
	print $1;
	softmode = 1;
    } else {
	print $1,1;
	softmode = 0;
    }
    if (match($2,"tabu")) {
	tabumode = 1;
    } else {
	tabumode = 0;
    }
    variablemode = 1;
}

END {
}
