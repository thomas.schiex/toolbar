#!/bin/sh

problem=`basename $1 .pre`

na=`./nballeles.sh $1 | tail -1`
tot=`awk 'BEGIN{print ('${na}' * ('${na}' + 1) / 2); exit}'`

echo -n "" > tmpres

for var in `./getmonocritics.sh $1 | awk '{print $2-1}'` ; do
  typings=`awk 'ok{res=res"="$1;ok=0} /^1 [0-9]* 1 1$/&&$2!='${var}'{ok=1;res=res" -A"$2} END{print res}' ${problem}.wcsp`
  n=0
  while (( $n < $tot )) ; do
       ./toolbar -p3 -f3 ${problem}.ergo -v $typings -A${var}=${n} | awk -f solution2cp.awk ${problem}.cp - | awk '/Loglikelihood/{proba=$0} /Optimal solution/{print "individual",'$var'+1," : typing ",$('$var'+3),proba}' | tee -a tmpres
       n=`expr $n + 1`
  done
done

best=`sort -g -k7,7 tmpres | tail -1 | awk '{sub("[)]","");print $10}'`

echo ""
echo "The most probable solution as a probability equal to $best"
echo ""

sort -g -k2,2 -k5,5 -k7,7 tmpres | awk '{res=$10;sub("[)]","",res);print $0,'$best'/res}'
