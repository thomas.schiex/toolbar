/*
 * Purpose: This module provides a solveDomainAbstractionBE() function which
 * implements bucket elemination with domain abstraction.
 * 
 * 
 * author Paul Maier
 */

#include <string.h>

#include "domainAbstractionBE.h"
#include "abstractionFunction.h"
#include "CuTest.h"
#include "DAsearch.h"
#include "dynamicTau.h"
#include "measurementsBE.h" 




// number of domain abstraction functions
int NoTaus = DEFAULT_NUMBER_ABSTRACTION_FUNCTIONS;
//int TupleBound = MAX_TUPLES;

nodofunc* varProjection(bucket b);
void initStructures();

// the following functions are from be.c
cost_t constantFunctionsSum (bucket *b); 
void assignBucket (nodofunc *g); 
cost_t evaluateBucket(bucket b, short * completeAssignment, short * posVar); 
cost_t evalua(nodofunc *nodo, short *completeAssignment, short *pos_var);

/**
 * Evaluate the given abstracted constraint function in the given tuple.
 */
cost_t evaluateAbstracted(abstracted_constraint_t *ac, short *assignment,
        short *pos_var) {
    short i;
    long int pos;
    long int av = 0;

    pos=0;
    for (i=0; i<ac->aridad; i++) {
        av = ac->vtaus[ac->vars[i]](assignment[ pos_var[ac->vars[i]] ]);
        pos=pos+(av)*(ac->despl_var[i]);
    }
    return ac->costs[pos];
}

/**
 * Compute the real costs which the given bucket/var adds to the assignment. I.e.
 * g_new = g_old + deltaG. 
 * 
 * It's not clear yet whether this computation is correct for BOTTOM > 0, since
 * I don't know yet how BOTTOM is actually handled. 
 */
cost_t beda_computeDeltaG(bucket b, full_assignment_t assignment, 
        short * posVar) {
    nodofunc *node;
    cost_t sum=BOTTOM;

    if ((node = b.primera_func_original)==NULL) {
        return sum;
    }
    while (node!=NULL) {
        sum+=evalua(node, assignment, posVar);
        node=node->sig_func;
    }
    //return sum > Top ? Top : sum;
    return sum;
}

/**
 * Compute the estimated costs h from the given node/var to the complete solution.
 * This function actually works on abstracted constraints. Every generated 
 * constraint in an arbitrary bucket is expected to be an abstracted constraint. 
 * 
 * It's not clear yet whether this computation is correct for BOTTOM > 0, since
 * I don't know yet how BOTTOM is actually handled. 
 */
cost_t beda_computeH(bucket b, full_assignment_t assignment, short * posVar) {
    abstracted_constraint_t *node;
    cost_t sum=BOTTOM;

    if ((node = (abstracted_constraint_t *)b.primera_func_anadida)==NULL) {
        return sum;
    }
    while (node!=NULL) {
        sum+=evaluateAbstracted(node,assignment,posVar);
        node=node->sig_func;
    }
    //return sum > Top ? Top : sum;
    return sum;
//    return 0;
}


bool_t advanceAssignment(short num_vars, short *variables, short *at,
        int *domsizes) {
    short i;

    i=0;
    while ((i<num_vars) && (at[i]==domsizes[ variables[i] ]-1)) {
        at[i]=0;
        i++;
    }
    if (i<num_vars) {
        at[i]++;
        return FALSE;
    } else
        return TRUE;
}


/**
 * Version of the varProjection() function in be.c which works on abstracted
 * constraints. Actually, this function works almost exactly like
 * varProjection() (the source is indeed copied from this function). 
 * The single difference is that this function doesn't rely on global variables 
 * (at least where it hinders the use of abstracted constraints).
 * 
 * PRE: All constraints in the given bucket are added and abstracted constraints
 * PRE: All constraints have the same set of value abstraction functions
 */
abstracted_constraint_t* varProjection_Abstracted (bucket b, int *domsizes) {
    short i;
    long int pos;
    abstracted_constraint_t *nodo_res;
    short *completeAssignment;
    short var_proy;
    short *func_vars, *posVar, arity=0;
    bool_t *inScope, overflow;
    abstracted_constraint_t *node;
    value_tau_func_t * vtaus = NULL;
    cost_t value, v;
    int dominio_var_elim;

    //Variables of the result function in decrease order
    func_vars = mycalloc (NVar, sizeof(short));
    inScope = mycalloc (NVar, sizeof(bool_t));

    // only added constraint functions (which are abstracted) are processed here
    
    node = (abstracted_constraint_t *)b.primera_func_anadida;
    // the vtaus of all abstracted constraints in the given bucket are the same
    if (node != NULL) vtaus = node->vtaus;
    while (node!=NULL) {
        for (i=0; i<node->aridad; i++) {
            if (!inScope[node->vars[i]]) {
                inScope[node->vars[i]]=TRUE;
                func_vars[arity++]=node->vars[i];
            }
        }
        node=node->sig_func;
    }
    
    decreaseOrder(func_vars,arity);
    posVar = mycalloc (NVar, sizeof(short));
    for (i=0; i<arity; i++) posVar[func_vars[i]]=i;

    var_proy = func_vars[0];
    nodo_res=newAbstractedConstraint(arity-1, func_vars+1, domsizes, vtaus);

    completeAssignment=mycalloc(arity,sizeof(short));

    //Domain size of the variable to be projected
    dominio_var_elim=domsizes[ func_vars[0] ];

    overflow=FALSE;
    pos = 0;
    while (!overflow) {
        completeAssignment[0]=0;
        value = Top;
        while (completeAssignment[0]<dominio_var_elim) {
            v = evaluateBucket (b, completeAssignment, posVar);
            if (value>v) value=v;
            completeAssignment[0]++;
        }
        nodo_res->costs[pos]=value;
        pos++;
        completeAssignment[0]--;
        overflow=advanceAssignment(arity, func_vars, completeAssignment,
                domsizes);
    }

    free (posVar);
    free (completeAssignment);
    return (nodo_res);
}


full_assignment_t solveDomainAbstractionBE() {

	/*
	 * This array of buckets takes the abstracted functions generated with the
	 * Tau[i] abstraction functions.
	 */
	bucket abstract_fs[NoTaus];
	
	/* 
	 * Tau[i] is the ith abstraction function, which maps a constraint cost
	 * function f to an abstracted function f' which operates on abstracted 
	 * domains
	 * 
	 * TODO: Currently only one Abstraction function at once is supported, i.e.
	 *       NoTaus == 1
	 */
	tau_func_t Tau[NoTaus];
	dynamic_tau_data_t dtauData[NoTaus];
	int bi = 0, i = 0;

	// initialise structures using the function from be.c, since we need the same
	// initial setup here
	initStructures();
    initMeasurementStructures();
	
	
	// allocate abstracted functions matrix, Tau array and g functions
	for (i = 0; i < NoTaus; ++i) {
		abstract_fs[i].num_funcs = 0;
		abstract_fs[i].primera_func_anadida = NULL;
		abstract_fs[i].primera_func_original = NULL;
		abstract_fs[i].ultima_func_original = NULL;
		
		// use Tau specified by the user (or default)
		if (UserTau == tauTypeDynamic) {
			/*
			 */
			
			dtauData[i].vtaus = mycalloc(NVar, sizeof(value_tau_func_t));
			dtauData[i].domsizes = mycalloc(NVar, sizeof(int));
			
			generateSimpleAbstractionData(&dtauData[i], Bucket,
                    DomainAbstractionBEMaxTuples);
			Tau[i] = TauFunctions[UserTau];
		} else {
			//dummy values here, since dtauData won't be used
			dtauData[i].vtaus = NULL;
			dtauData[i].domsizes = NULL;
			
	        Tau[i] = TauFunctions[UserTau];
		}
	}
	
	
	double SolvingTime = cpuTime();

	/*
	 * The idea behind this algorithm is to replace the mini bucket mechanism
	 * with a more general one. The base is still bucket elimination, i.e.
	 * the constraints are put into buckets which are then processed in the
	 * reverse order of the given variable ordering. Up to here everything works
	 * like normal bucket elimination, which means it's not much different from
	 * the algorithm in be.c.
	 * 
	 * But now the abstraction functions Tau[i] (i = 1 .. NoTaus) come into play.
	 * The constraint cost functions stored in the buckets are processed by all
	 * Taus yielding a matrix abstract_fs of abstracted functions, which contains  
	 * a row of abstracted functions for all Taus. A single row contains all
	 * abstracted functions of a bucket.
	 * Each row of the said matrix yields a function g[i] through 
	 * varProjection(abstract_fs[i]), which joins all functions in a bucket and 
	 * projects them onto the unified scope of them minus the variable of the
	 * bucket (the matrix rows are implemented using the bucket structure of be.c
	 * here).
	 * Now all gs are joined into a single constraint which is then added to 
	 * the appropriate bucket, i.e. the one associated to the variable appearing
	 * last in the scope of the joint constraint function.
	 */
	for (bi = NVar-1; bi >= 0; --bi) { //over all Buckets in reverse order
		if (Verbose) {
			printf("Bucket %d\n", bi);
		}
		if (Bucket[bi].num_funcs == 0) {
			continue;
		}
		
		//first, build the matrix of abstracted functions
		for (i = 0; i < NoTaus; ++i) {
			abstract_fs[i].num_funcs = 0;
			abstract_fs[i].primera_func_anadida = NULL;
			abstract_fs[i].primera_func_original = NULL;
			abstract_fs[i].ultima_func_original = NULL;			
			
			nodofunc* func = NULL;
			abstracted_constraint_t* abstractedFunc = NULL;
			abstracted_constraint_t* lastFunc = NULL;
			for (func = Bucket[bi].primera_func_original; 
				func != NULL; func = func->sig_func) {
				abstractedFunc = Tau[i](func, &dtauData[i]);

				// remember the first processed function (which is the last in 
				// the LL) , so we may later add the generated functions from 
				// the current bucket
				if (lastFunc == NULL) lastFunc = abstractedFunc;
				
				abstractedFunc->sig_func = abstract_fs[i].primera_func_anadida;
				abstract_fs[i].primera_func_anadida = (nodofunc*)abstractedFunc;
				abstract_fs[i].num_funcs++;
			}

			nodofunc *msg = NULL;
			if (abstract_fs[i].num_funcs > 0) {
				// the added/generated constraints of the current bucket also need 
				// to be processed
				lastFunc->sig_func = Bucket[bi].primera_func_anadida;
				// this assumes that the same abstraction function was applied to 
				// all functions in Bucket[bi]
				
				msg = (nodofunc *)varProjection_Abstracted(abstract_fs[i],
	                    ((abstracted_constraint_t*)
	                    abstract_fs[i].primera_func_anadida)->domsizes); 
				assignBucket(msg);

				/* 
				 * compute the would-be size of this buckets message if 
				 * abstraction was not applied.
				 */
				BucketMsgSizes[bi] = computeBucketMsgSize(&abstract_fs[i],bi);
				
				// Disconnect the functions from Bucket[bi] again, so they won't
				// get freed along with abstract_fs[i]
	            lastFunc->sig_func = NULL;
				freeBucket(&abstract_fs[i]);
			} else {
				/*
				 * the current bucket Bucket[bi] didn't contain any original 
				 * constraints, so no abstracted constraints were created. Only
				 * the abstracted constraints created previously and which 
				 * were added to this bucket need to be processed.
				 */
				msg = (nodofunc *)varProjection_Abstracted(Bucket[bi],
	                    ((abstracted_constraint_t*)
	                    Bucket[bi].primera_func_anadida)->domsizes); 
				assignBucket(msg);

				/* 
				 * compute the would-be size of this buckets message if 
				 * abstraction was not applied.
				 */
				BucketMsgSizes[bi] = computeBucketMsgSize(&Bucket[bi], bi);
			}

			//measure abstract message size
			// TODO if there are more than one Taus only for the last one the msg size is stored -> discard the possibility to have multiple Tau's?
			BucketAbstractMsgSizes[bi] = msg->ntuples;
		}
	}
	
	double inferenceTime = cpuTime() - SolvingTime; 

    cost_t s = constantFunctionsSum(Result);
    printf("lower bound found by BEDA: ");
    PRINTCOST(s);
    printf("\n");
    printf("\n");
	
	
	if (Verbose) {
	    printf("Now searching...\n");
	}
	
	full_assignment_t solution = generateSolutionAStar(Bucket,
            beda_computeDeltaG, beda_computeH);

//    full_assignment_t solution = generateSolutionBAB(Bucket, Top,
//            beda_computeDeltaG, beda_computeH);
	
	
	if (Verbose) {
        printf("solution:\n");
	    for (i = 0; i < NVar; ++i) {
            printf("variable %d: %d\n", i, solution[i]);
        }
	}
	
	SolvingTime = cpuTime() - SolvingTime;
	printf("\nproblem solved in %f seconds\n", SolvingTime);

	///////////////////// outputs which get parsed by algobench.py
	printf("inference: %f sec\n search: %f sec\n", inferenceTime, 
			SolvingTime-inferenceTime);
//	printf("number computed messages: %d\n", NComputedMessages);
	printf("BE message sizes: ");
	for (i = 0; i < NVar; i++) {
		printf("%d ", BucketMsgSizes[i]);
	}
	printf("\nabstract message sizes: ");
	for (i = 0; i < NVar; i++) {
		printf("%d ", BucketAbstractMsgSizes[i]);
	}
	printf("\n");
	// end output for algobench.py

	
	for (bi = 0; bi < NVar; ++bi) {
	    freeBucket(&Bucket[bi]);
    }
	freeMeasurementStructures();

	return solution;
}



/*****************************************************************************
 * Test case functions.
 *****************************************************************************/

void TestSolveDomainAbstractionBE(CuTest *tc) {
//	const char *testFile = "testProblem.wcsp";
    int i = 0;
	const char *testFile = "../benchs/academics/4wqueens.wcsp";
	
    UserTau = 1;

    FILE *fileHandle = fopen(testFile, "r");
    if(fileHandle == NULL) {
      perror("Error: test wcsp file could not be opened!"); 
      abort();
    }
    
	readWCSP(fileHandle); allocateWCSP();

	printf("using Tau %s\n", UserTauName[UserTau]);
	
	for (i = 0; i < NVar; ++i) {
        printf("dom size %d: %d\n", i, DomainSize[i]);
    }
    full_assignment_t solution = solveDomainAbstractionBE();
	CuAssertPtrNotNull(tc, Bucket);
	
	CuAssertPtrNotNull(tc, solution);
	CuAssert(tc, "NO_SOLUTION returned!", solution != NO_SOLUTION);
	// assert the correct solution. This solution is produced by toolbar when
	// using other solver methods
	CuAssertIntEquals(tc, 2, solution[0]);
    CuAssertIntEquals(tc, 0, solution[1]);
    CuAssertIntEquals(tc, 3, solution[2]);
    CuAssertIntEquals(tc, 1, solution[3]);
	
	int s = constantFunctionsSum(Result);
	printf("s: %d\n", s);

	for (i = 0; i < NVar; ++i) {
		printf("variable %d: %d\n", i, solution[i]);
	}
	
	free(solution);
	freeWCSP();
}
