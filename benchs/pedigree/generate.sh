#!/bin/sh

ninst=50
num=0
aux=0

ngen=4
n=$1
male=6

nerrors=6

echo "ngen=" $ngen, " n=" $n, " male=" $male
 
if [[ $n == ""  ]] ; then
 echo "usage: ./generate.sh number_of_founders"
 exit
fi

aux=`expr $n - $male`
aux=`expr $aux % $male`
if [[ $aux != 0  ]] ; then
 echo "incorrect number of males"
 exit
fi
 
while (( $nerrors < 11 )) ; do
    num=0
    printf "errors: " 
    printf $nerrors
    printf " "
    zeros=""
    len=`expr length $nerrors`
    if [[ $len == 1  ]] ; then
	    zeros="0"
    fi    
    base=errors${zeros}${nerrors}
    \rm -rf $base
    \mkdir $base
    while (( $num < $ninst )) ; do
	    ./pedigreeSim $ngen $n $male &> /dev/null
	    if [[ $num > 0 ]] ; then
		res0=`diff ped.pre  ${base}/ped${num}.pre`
		res1=`cat ped.errors | wc -l`
		if [[ $res1 != $nerrors ]] ; then 
			res0=""
		fi 
	    else 
		res0="OK"
		res1="OK"
	    fi
	    if [[ $res0 != "" ]] ; then
		num=`expr $num + 1`
		\mv ped.pre ${base}/ped${num}.pre
		\mv ped.errors ${base}/ped${num}.errors
		printf "."	
	    fi
    done	
    nerrors=`expr $nerrors + 1`
done
