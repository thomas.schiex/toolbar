#!/bin/bash

# Auto generate single AllTests file for CuTest.
# Searches through all *.c files in the current directory.
# Prints to allTests.c
# Author: Asim Jalis
# Modified by: Paul Maier, 16.10.2007
# Date: 01/08/2003

if test $# -eq 0 ; then FILES=*.c ; else FILES=$* ; fi

echo '

/* This is auto-generated code. Edit at your own peril. */

#include "CuTest.h"
#include "stdio.h"

' > allTests.c

cat $FILES | grep '^void Test' | 
    sed -e 's/(.*$//' \
        -e 's/$/(CuTest*);/' \
        -e 's/^/extern /' >> allTests.c

echo \
'

void RunAllTests(void) 
{
    CuString *output = CuStringNew();
    CuSuite* suite = CuSuiteNew();

' >> allTests.c
cat $FILES | grep '^void Test' | 
    sed -e 's/^void //' \
        -e 's/(.*$//' \
        -e 's/^/    SUITE_ADD_TEST(suite, /' \
        -e 's/$/);/' >> allTests.c

echo \
'
    CuSuiteRun(suite);
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);
}

' >> allTests.c

# generate allTests.h

echo '

/* This is auto-generated code. Edit at your own peril. */

#ifndef ALL_TESTS_H
#define ALL_TESTS_H

void RunAllTests(void);

#endif

' > allTests.h
