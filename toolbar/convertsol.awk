
# convert a solution in wcnf format to wcsp format using a translation file

# usage: awk -f convertsol.awk convertsol solcnf > sol

FNR == NR {
  if ($3 > n) n = $3;
  if ($1 == -1) {
    encoding[$3] = -1;
    variable[$3,0+nbprop[$3]] = $2;
    value[$3,0+nbprop[$3]] = $4;
    nbprop[$3]++;
  } else {
    encoding[$3] = 0;
    variable[$3,$1] = $2;
    if ($1==0) {
      for (i=4;i<=NF;i++) {
	domain[$3,i-4] = $i;
      }
    }
    nbprop[$3]++;
  }
}

FNR != NR {
  for (i=1; i<=NF;i++) {
    if ($i < 0) booleanvar[-$i] = 0;
    else booleanvar[$i] = 1;
  }
}

END {
  for (i=0;i<=n;i++) {
    if (encoding[i]==-1) {
      ok=0;
      for (j=0; j<nbprop[i]; j++) {
	if (booleanvar[variable[i,j]]==1) {
	  printf(" %d", value[i,j]);
	  ok=1;
	  break;
	}
      }
      if (!ok) print " variable",i,"is not assigned!!!";
    } else {
      sum = 0;
      for (j=0; j<nbprop[i]; j++) {
	if (booleanvar[variable[i,j]]==0) sum += 2^j;
      }
      printf(" %d", domain[i,sum]);
    }
  }
  print "";
}

