/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmlist.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

// Simple Lists.

typedef struct Node_LS
{
	void * elem;
	struct Node_LS *next;

} NodeLS;

typedef struct
{
	NodeLS * first;
	NodeLS * last;
	NodeLS * actual;
} ListS;

// Double lists

typedef struct Node_LD
{
	void * elem;
	struct Node_LD *next;
	struct Node_LD *prev;
} NodeLD;

typedef struct
{
	NodeLD * first;
	NodeLD * last;
	NodeLD * actual;

} ListD;

typedef struct Node_LR
{
	long long int val1;
	long long int val2;
	long long int val3;
	long long int val4;
	long long int val5;
	struct Node_LR *next;
	struct Node_LR *prev;
} NodeR;

typedef struct
{
	NodeR * first;
	NodeR * last;
	NodeR * actual;
	int memory;
} ListR;

/* Functions for simple lists */
ListS * newListS();
void createListS(ListS * l);
void setFirstLS(ListS * l);
void getNextLS(ListS * l);
int endListLS(ListS * l);
int isEmptyLS(ListS * l);

/* Functions for Double lists */

ListD * newListD();
void createListD(ListD * l);
void setFirstLD(ListD * l);
void getNextLD(ListD * l);
void getPreviousLD(ListD * l);
void insertExistentNode (ListD *l,NodeLD * n);
NodeLD * extractActual(ListD * l);
int endListLD(ListD * l);
int isEmptyLD(ListD * l);
void showListD(ListD *l);

/* Functions for restore Lists */

void createListR(ListR * l,int mem);
void createListRAux(ListR * l);
NodeR * createElement();
void insertElement(ListR * l,NodeR *n);
NodeR * getActualElment (ListR * l);
void setFirstLR(ListR * l);
void getNextLR(ListR * l);
void createXNodes(ListR * l, int num);
int endListLR(ListR * l);
void showActualElement(ListR * l);
void showElements(ListR * l);
NodeR * extractActualR(ListR * l);
void getPreviousLR(ListR * l);
int isEmptyLR(ListR * l);
void showElement(NodeR * n);
void clearListR(ListR * l);
NodeR * passNode(ListR *o);
NodeR * getNewNode1(ListR *o,long long int val1);
NodeR * getNewNode2(ListR *o,long long int val1,long long int val2);
NodeR * getNewNode3(ListR *o,long long int val1,long long int val2,long long int val3);
NodeR * getNewNode4(ListR *o,long long int val1,long long int val2,long long int val3,long long int val4);
NodeR * getNewNode5(ListR *o,long long int val1,long long int val2,long long int val3,long long int val4,long long int val5);
void unloadNodes(ListR *o,ListR *d);
