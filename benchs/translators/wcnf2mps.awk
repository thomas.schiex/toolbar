
# warning! restricted wcnf format such that each clause is entirely defined within a single line of text

# note: tautologies are automatically removed
# hard clauses are translated into one linear constraint only (use optional top value in p header line)
# translation of soft clauses adds a boolean variable specifying its satisfiability status
# unary soft clauses are directly encoded into the objective function

function format(s1,s2,s3,s4) 
{
  printf(" %-2s %-8s  %-8s  %12s\n", s1,s2,s3,s4);
  return;
}

BEGIN {
    verbose = 0;
    top = 1e100;
    ok = 0;
    objcste = 0;
}

ok {
  nb++;

  if ($NF != 0) {
    if (verbose) print "*** ERROR: current line does not end with a zero";
  }

  arity[nb] = NF - 2;
  rhs[nb] = 1;
  cost[nb] = $1;
  for (i=2; i<NF; i++) {
    if ($i < 0) {
	matrix[-$i,nb]--; 
	if (matrix[-$i,nb] == 0) {
	    tautology[nb] = 1;
	    if (verbose) print "*** WARNING: " nb " is a tautology due to " $i "!";
	}
	if (matrix[-$i,nb]==-1) rhs[nb]--; # same variable repeated in the same clause collapse to one
	if (matrix[-$i,nb]<-1) arity[nb]--;
	matrix[-$i,nb] = -1; 
    } else {
	matrix[$i,nb]++;
	if (matrix[$i,nb] == 0) {
	    tautology[nb] = 1;
	    if (verbose) print "*** WARNING: " nb " is a tautology due to " $i "!";
	}
	if (matrix[$i,nb]>1) arity[nb]--;
	matrix[$i,nb] = 1; 
    }
  }
  if (arity[nb]==1 && cost[nb] < top) {
      unarycost[$2] += cost[nb];
      if (unarycost[$2] >= top) {
	  cost[nb] = top; # hard unary clause because of previous soft unary clauses on the same variable
      } else {
	  softunary[nb] = 1; # soft unary clause
      }
  }
  if (!(nb in tautology)) {
      for (i=2; i<NF; i++) {
	  if ($i < 0) defined[-$i] = 1;
	  else  defined[$i] = 1;
      }
  }
}

/^c PSEUDOBOOLEAN / {
    top = $3;
}

/^p / {
  var = $3;
  nb = $4;
  ok = 1;
  if (NF >= 5) top = $5;

  print "NAME          " FILENAME;
  
  nbcopy = nb;
  nb = 0;
}

END {
  if (nb != nbcopy) {
    if (verbose) print "*** ERROR: read not enough clauses!!!",nb,nbcopy;
  }

  print "ROWS";
  print " N  obj";
  for (c=1; c<=nb; c++) {
    if (!(c in softunary) && !(c in tautology)) print " G  c" c;
  }

  print "COLUMNS";
  for (i=1; i<=var; i++) {
      for (c=1; c<=nb; c++) {
	  if (!(c in softunary) && !(c in tautology) && (matrix[i,c] != 0)) {
	      format("", "y" i, "c" c, matrix[i,c]);
	  }
      }
      if (i in unarycost && unarycost[i] < top) {
	  format("", "y" i, "obj", -unarycost[i]);
	  objcste += unarycost[i];
      } else if (-i in unarycost && unarycost[-i] < top) {
	  format("", "y" i, "obj", unarycost[-i]);
      }
  }
  for (c=1; c<=nb; c++) {
    if (cost[c] < top && !(c in softunary) && !(c in tautology)) {
	format("", "z" c, "c" c, 1);
	format("", "z" c, "obj", cost[c]);
    }
  }   
  if (objcste > 0) {
      format("", "one", "obj", objcste);
  }

  print "RHS";
  for (c=1; c<=nb; c++) {
    if (!(c in softunary) && !(c in tautology)) format("", "RHS", "c" c, rhs[c]);
  }

  print "BOUNDS";
  for (i=1; i<=var; i++) {
      if (i in defined) {
	  format("BV", "BND", "y" i, "");
      } else {
	  if (verbose) print "*** WARNING! Variable " i " not used!";
      }
  }
  for (c=1; c<=nb; c++) {
    if (cost[c] < top && !(c in softunary) && !(c in tautology)) format("UP", "BND", "z" c, 1);
  }
  if (objcste > 0) {
      format("FX", "BND", "one", 1);
  }

  print "ENDATA";
}
