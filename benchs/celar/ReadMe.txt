---
--- Original README file from  ftp.cert.fr/pub/lemaitre/FullRLFAP.tgz
---

The    three   directories   FullRLFAP/CELAR,    FullRLFAP/GRAPH   and
FullRLFAP/SUBCELAR6 contain the definition of the radio link frequency
assignment problems described in (the forthcoming):

Radio Link Frequency Assignment Benchmarks. B. Cabon, S. de Givry,
L. Lobjois, T. Schiex and J.P. Warners. CONSTRAINTS journal. 1998.

The  first directory  holds  the CELAR  instances,   provided by the  Centre
d'Electronique de l'Armement (France).

The second directory  holds the GRAPH instances,  generated using  the GRAPH
generator at Delft University (the Netherlands).

The third directory holds the  SUBCELAR6 instances which have been extracted
from  the  CELAR6 instance  by  the   combinatorial  optimization group   of
ONERA/CERT (France) to prove optimality of the best solution known.

If  you find any  mistake  or ambiguity in  this  data, please report  it to
Thomas.Schiex@toulouse.inra.fr.

---

The  fourth directory holds  the SUBCELAR7  instances which  have been
extracted from  the CELAR7 instance by  the combinatorial optimization
group of ONERA/CERT (France) to compute a problem lower bound.  

A safe domain reduction, which  preserves the fact that the optimum is
a lower bound  of the original problem, has  been applied on SUBCELAR6
and SUBCELAR7 instances, producing the following instances:

CELAR6-SUB1-24 (maximum of 24 values per domain)
CELAR6-SUB4-20 (maximum of 20 values per domain)
CELAR7-SUB1-20 (maximum of 20 values per domain)
CELAR7-SUB4-22 (maximum of 22 values per domain)

For more details, see: 

Simon de Givry. Minorants de probl�mes de minimisation de violation de
contraintes  : recherche de  bonnes relaxations  � l'aide  de m�thodes
incompl�tes. In Proc. of JNPC-99, Lyon, France, 1999 (in French)

---

Known lower and upper bounds are available in the file CelarResults.txt.

---

All these RLFAP instances have been converted into .cp and .wcsp formats,
using scripts in the directory translators.

---

scen06-XX[reduc].wcsp instances are derived from CELAR6 by removing a number of values (at most, XX values removed per domain) and changing the constraint tuples accordingly such that their optimum is a lowerbound for the original CELAR6 problem. The "reduc" extension means a preprocessing step has been applied to the instance (singleton EDAC with an initial upper bound of 3389, Koster's dominance rule, variables with a degree inferior than or equal to 2 removed).
