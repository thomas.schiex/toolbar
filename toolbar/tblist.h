/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tblist.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef LIST_H
#define LIST_H

union intorcost
{
  int i;
  cost_t c;
};

struct define_node_LIST
{
 struct define_node_LIST *ant;
 struct define_node_LIST *sig;
 int val1;
 int val2;
 int val3;
 union intorcost val4;
};
typedef struct define_node_LIST node_LIST;

struct define_List
{
 node_LIST *prim;
 node_LIST *ult;
 node_LIST *finger;
 int n_elem;
};
typedef struct define_List List;

extern void List_allocate_dynamic_memory();
extern void List_get_more_dynamic_memory();
extern void List_deallocate_dynamic_memory();
extern node_LIST *pido();
extern void devuelvo(node_LIST *p);
extern void List_create(List *l);
extern void List_insert_first (List *l, int elem1);
extern void List_insert1 (List *l, int elem1);
extern void List_insert2 (List *l, int elem1, int elem2);
extern void List_insert4 (List *l, int elem1, int elem2, int elem3, int elem4);
extern void List_insert3pc (List *l, int elem1, int elem2, int elem3, cost_t elem4);
extern void List_delete_node (List *l);
extern int List_empty(List *l);
extern int List_n_of_elem(List *l);
extern void List_reset_traversal(List *l);
extern int List_end_traversal(List *l);
extern void List_next(List *l);
extern int List_consult1(List *l);
extern void List_consult2(List *l, int *elem1, int *elem2);
extern void List_consult4(List *l, int *elem1, int *elem2, int *elem3, int *elem4);
extern void List_consult3pc(List *l, int *elem1, int *elem2, int *elem3, cost_t *elem4);
extern void List_dispose_memory(List *l);
extern void List_delete_this_elem(List *l, int elem);

#endif
