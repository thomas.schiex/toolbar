
# Convert a wcsp problem in .wcsp file format into a Ilog reified problem (SoftAsHard) in C++ 

# Warning! initial .wcsp file must have all default constraint cost fields set to zero.
# Warning bis! this conversion creates one C++ line per non-zero tuple. The resulting C++ file may be very large.

# Usage:
#
# # to ensure all default constraint cost fields set to zero:
# toolbar problem.wcsp -l0 -qproblem_extended.wcsp
#
# awk -f wcsp2ilog.awk problem_extended.wcsp > problem.cc
#
# g++ problem.cc -DILOGLUE -DIL_STD -I/usr/local/Ilog/solver60/include -I/usr/local/Ilog/concert20/include -L/usr/local/Ilog/solver60/lib/i86_linux2_glibc2.3_gcc3.2/static_pic -L/usr/local/Ilog/concert20/lib/i86_linux2_glibc2.3_gcc3.2/static_pic -lsolverfloat -lsolver -lconcert

FNR==1 {
  name = $1;
  n = $2;
  d = $3;
  e = $4;
  ub = $5;
  print "/*",name,n,d,e,ub,"*/";
  print "#include <ilsolver/ilosolverint.h>";
  print "ILOSTLBEGIN";
  print "int main(){";
  print "IloEnv env;";
  print "  try {";
  print "IloModel model(env);";
}

FNR==2 {
  for (i=1; i<=n; i++) {
    print "IloIntVar x" i-1 "(env,0," $i - 1 ");";
  }
  print "IloIntVarArray vars(env," n ");";
  for (i=0; i<n; i++) {
    print "vars[" i "]=x" i ";";
  }
  for (c=0; c<e; c++) {
    print "IloIntVar c" c "(env,0," ub ");";
  }
  print "IloIntVarArray costs(env," e ");";
  for (c=0; c<e; c++) {
    print "costs[" c "]=c" c ";";
  }
  c = 0;
  tuple = 0;
}

FNR >= 3 {
  if (tuple==0) {
    if ($(NF-1) != 0) {
      printf("convert wcsp into simple format using toolbar -q option\n");
      exit(0);
    }
    arity = $1;
    printf("IloIntVarArray scope%d(env,%d);\n", c, arity+1);
    for (a=2; a<=arity+1; a++) {
      print "scope" c "[" a-2 "]=x" $a ";";
    }
    print "scope" c "[" arity "]=c" c ";";
    print "IloIntTupleSet set" c "(env," arity+1 ");";
    tuple = $NF;
  } else {
    printf("set%d.add(IloIntArray(env,%d",c,NF);
    for (i=1;i<=NF;i++) {
      printf(",%d", $i);
    }
    print "));";
    tuple--;
    if (tuple == 0) {
      print "model.add(IloTableConstraint(env, scope" c ",set" c ",IlcTrue));";
      c++;
    }
  }
}

END {
  print "IloIntVar sum(env,0," ub ");";
  print "model.add(sum == IloSum(costs));";
  print "IloObjective obj = IloMinimize(env, sum);";
  print "model.add(obj);";
  print "IloSolver solver(model);";
#    print "solver.out() << \"Objective.lb = \" << solver.getMin(sum) << endl;"; # solver.getMin(obj)
  print "if (solver.solve()) {";
  print "solver.out() << solver.getStatus() << \" Solution\" << endl;";
  print "solver.out() << \"Objective = \" << solver.getValue(obj) << endl;";
  for (i=0; i<n; i++) {
    print "cout << \"x" i " = \" <<  solver.getValue(x" i ") << endl;";
  }
  print "}";
  print "solver.printInformation();";
  print "} catch (IloException& ex) {";
  print "cout << \"Error: \" << ex << endl;";
  print "}";

  print "env.end();";
  print "return 0;";
  print "}";
}
