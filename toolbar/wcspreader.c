/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wcspreader.c
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"

/* warning! limit on stacksize */
#define MAXNVAR 100000
#define MAXARITY 1000

extern void sort_val_lex(int var);

void readWCSP(FILE* file) 
{
  int c,i,j,a,b,k,l,arity,nbc,e;
  int ntuples,ntuplesdiff;
  cost_t defval, val;
  int nbconstr; /* total number of problem constraints */
  cost_t top;
  char pbname[256];
  char dummy[256];
  int completeAssignment[MAXNVAR];
  int nbinary = 0;
  int shared;
  int nbshared = 0;

  /* read problem name and sizes */
  myfscanf4(file, "%s %d %d %d", pbname, &NVar, &NValues, &nbconstr);
  SCANCOST(file,&top);
  /* printf("Read problem: %s\n", pbname); */
  Top = MIN(Top, top);

  /* read variable domain sizes */
  DomainSize = mycalloc(NVar, sizeof(int));
  for (i=0; i<NVar; i++) {
    myfscanf1(file, "%d", &DOMSIZE(i));
    if (DOMSIZE(i) > NValues) NValues = DOMSIZE(i);
  }

  /* read each constraint */
  Graph = mycalloc(NVar * NVar, sizeof(cost_t *));
  UnaryConstraints = mycalloc(NVar * NValues, sizeof(cost_t));
  NaryConstraints = mycalloc(nbconstr, sizeof(int **));
  NaryCosts = mycalloc(nbconstr, sizeof(cost_t **));
  SharedNaryCosts = mycalloc(nbconstr, sizeof(int *));
  MeanCosts = mycalloc(nbconstr, sizeof(double));
  Variables = mycalloc(NVar, sizeof(int **));

  for (c=0; c<nbconstr; c++) {
    myfscanf1(file, "%d", &arity);
    /* special constraint which also defines a shared n-ary constraint */
    shared = FALSE;
    if (arity < 0) {
      if (-arity <= 2) {
	fprintf(stderr,"Error: unary or binary shared constraints are forbidden!\n");
	abort();
      }
      shared = TRUE;
      arity = -arity;
    }
    if (arity == 2) {
      /* printf("read binary c_%d...\n", c); */
      myfscanf2(file, "%d %d",&i, &j);
      if (i==j) {
	fprintf(stderr,"Error: binary constraint with only one variable in its scope!\n");
	abort();
      }
      SCANCOST(file,&defval);
      myfscanf1(file,"%d", &ntuples);

      if (!ISBINARY(i,j)) {
	ISBINARY(i,j) = mycalloc(DOMSIZE(i) * DOMSIZE(j), sizeof(cost_t));
	ISBINARY(j,i) = ISBINARY(i,j);
      }
      for (a=0; a<DOMSIZE(i); a++) {
	for (b=0; b<DOMSIZE(j); b++) {
	  setBinaryCost(i,j,a,b,binaryCost(i,j,a,b) + defval);
	}
      }
      for (k=0; k<ntuples; k++) {
	myfscanf2(file, "%d %d", &a, &b);
	SCANCOST(file,&val);
	setBinaryCost(i,j,a,b,binaryCost(i,j,a,b) + val - defval);
      }
      nbinary++;
    } else if (arity == 1) {
      /* printf("read unary c_%d...\n", c); */
      myfscanf1(file, "%d", &i);
      SCANCOST(file, &defval);
      myfscanf1(file,"%d",&ntuples);
      for (a=0; a<DOMSIZE(i); a++) {
	UNARYCOST(i,a) += defval;
      }
      for (k=0; k<ntuples; k++) {
	myfscanf1(file, "%d", &a);
	SCANCOST(file, &val);
	UNARYCOST(i,a) += val - defval;
      }
    } else if (arity == 0) {
      /* printf("read global lower bound contribution c_%d...\n", c); */
      SCANCOST(file, &defval);
      myfscanf1(file, "%d", &ntuples);
      if (ntuples != 0) {
	fprintf(stderr,"Error: global lower bound contribution with several tuples!\n");
	abort();
      }
      Bottom += defval;
    } else if (arity >= 3) {
      /* printf("read nary c_%d...\n", c); */
      if (arity > MaxArity) MaxArity = arity;
      NaryConstraints[NConstr] = mycalloc((1 + arity), sizeof(int));
      ARITY(NConstr) = arity;
      ntuples = 1;
      for (k=0; k<arity; k++) {
	  SCOPEONE(NConstr,k) = -1;
      }
      for (k=0; k<arity; k++) {
	myfscanf1(file, "%d", &e);
	if (inScope(e,NConstr)) {
	  fprintf(stderr,"Error: nary constraint with two identical variables in its scope!\n");
	  abort();
	} else {
	  SCOPEONE(NConstr,k) = e;
	}
	ntuples *= DOMSIZE(SCOPEONE(NConstr,k));
      }
      SCANCOST(file, &defval);
      myfscanf1(file, "%d", &ntuplesdiff);
      if (ntuplesdiff < 0) {
	/* this constraint is shared with a previous n-ary constraint number */
	if (-ntuplesdiff-1 >= nbshared) {
	  fprintf(stderr,"Error: shared nary constraint number %d not yet defined!\n", -ntuplesdiff);
	  abort();
	}
	if (arity != ARITY(SharedNaryCosts[-ntuplesdiff-1])) {
	  fprintf(stderr,"Error: shared nary constraint number %d has not the same arity as n-ary constraint %d (%d <> %d)!\n", -ntuplesdiff, NConstr+1, arity, ARITY(SharedNaryCosts[-ntuplesdiff-1]));
	  abort();
	}
	COSTS(NConstr) = COSTS(SharedNaryCosts[-ntuplesdiff-1]);
	WEIGHTCONSTRAINT(NConstr) = WEIGHTCONSTRAINT(SharedNaryCosts[-ntuplesdiff-1]);
      } else {
	COSTS(NConstr) = mycalloc(ntuples, sizeof(cost_t));
	if (shared) {
	  SharedNaryCosts[nbshared] = NConstr;
	  nbshared++;
	  if (defval > BOTTOM && defval < Top && preProject >= arity) {
	    fprintf(stderr,"Error: shared constraint incompatible with preproject!\n");
	    abort();
	  }
	}
	for (k=0; k<ntuples; k++) {
	  COSTS(NConstr)[k] = defval;
	}
	for (k=0; k<ntuplesdiff; k++) {
	  for (l=0; l<arity; l++) {
	    myfscanf1(file, "%d", &completeAssignment[SCOPEONE(NConstr,l)]);
	  }
	  SCANCOST(file, &val);
	  if (shared && val > BOTTOM && val < Top && preProject >= arity) {
	    fprintf(stderr,"Error: shared constraint incompatible with preproject!\n");
	    abort();
	  }
	  setCost(NConstr, completeAssignment, val);
	  WEIGHTCONSTRAINT(NConstr) += val - defval;
	}
	WEIGHTCONSTRAINT(NConstr) /= cartesianProduct(NConstr);
	WEIGHTCONSTRAINT(NConstr) += defval;
      }
      NConstr++;
    } else {
      fprintf(stderr,"Error: negative constraint arity!\n");
      abort();
    }
  }
  if (fscanf(file, "%s",dummy) != EOF) {
    fprintf(stderr,"Error: too much information in problem file!\n");
    abort();
  }

  /* TO BE MOVED IN wcsp.c */
  /* create links between variables and nary constraints */
  for (i=0; i<NVar; i++) {
    nbc = 0;
    for (c=0; c<NConstr; c++) {
      for (k=0; k<ARITY(c); k++) {
	if (SCOPEONE(c,k) == i) {
	  nbc++;
	}
      }
    }
    Variables[i] = mycalloc(nbc + 1, sizeof(int));
    VARNCONSTR(i) = nbc;
    nbc = 0;
    for (c=0; c<NConstr; c++) {
      for (k=0; k<ARITY(c); k++) {
	if (SCOPEONE(c,k) == i) {
	  VARCONSTRONE(i,nbc) = c;
	  nbc++;
	}
      }
    }
  }
  /* printf("... problem %s is loaded.\n", pbname); */
  if (Verbose) {
    printf("Read %d variables, with %d values at most, and %d constraints.\n", NVar, NValues, nbconstr);
  }
}

int truecartesianProduct(int i) 
{
  int res = 1;
  int *var_p =  SCOPE(i);
  int *end = var_p + ARITY(i);
  while (var_p < end) {
    res *= List_n_of_elem(&feasible_values[*var_p]);
    var_p++;
  }
  return res;
}

void recursiveNaryPrint(FILE *file, int c, int i, int *complassignment, int *naryidxval)
{
  int a,idxa;

  if (i < ARITY(c)) {
    idxa = 0;
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      naryidxval[i] = idxa;
      complassignment[SCOPEONE(c,i)] = a;
      recursiveNaryPrint(file, c, i+1, complassignment, naryidxval);
      idxa++;
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else {
    for (i=0; i<ARITY(c); i++) {
      fprintf(file, "%d ", naryidxval[i]);
    }
    FPRINTCOST(file, cost(c, complassignment));
    fprintf(file, "\n");
  }
}

void saveWCSP(FILE* file) 
{
  int i,j,a,b,c,idxa,idxb,nbvar = 0, nbval = 0, nbconstr = 0;
  int first = NVar;
  int num[NVar];
  int unassigned[NConstr];
  int recursiveNaryAssignment[MAXNVAR];
  int recursiveNaryPrintIdxVal[MAXARITY];

  /* eliminate the variables with a singleton domain and no nary constraints on it */
  if (LcLevel >= LC_AC) {
    for (i=0; i<NVar; i++) {
      if (assignments[i]==-1 && List_n_of_elem(&feasible_values[i])==1 && VARNCONSTR(i)==0) {
	assignments[i]=NValues+1;
      }
    }
  }

  /* count the number of constraints */
  if (Bottom > BOTTOM) nbconstr++;  
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      if (first==NVar) first = i;
      num[i] = nbvar;
      nbvar++;
      nbval = MAX(nbval, List_n_of_elem(&feasible_values[i]));
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 &&  ISBINARY(i,j)) {
	  nbconstr++;
	}
      }
    }
  }
  nbconstr += nbvar; /* unary costs */
  for (c=0; c<NConstr; c++) { /* n-ary constraints */
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (assignments[SCOPEONE(c,i)]!=-1) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbconstr++;
    else if (ARITY(c) > 3) {
      /* ternary should have been transformed into binary by projectB */
      printf("Warning! partially assigned n-ary constraint %d ignored!\n", c);
    }
  }

  /* header */
  fprintf(file, "%s %d %d %d ", Filename, nbvar, nbval, nbconstr);
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  /* domain sizes */
  if (first < NVar) {
    fprintf(file, "%d", List_n_of_elem(&feasible_values[first]));
    for (i=first+1; i<NVar; i++) {
      if (assignments[i]==-1) {
	fprintf(file, " %d", List_n_of_elem(&feasible_values[i]));
      }
    }
    fprintf(file, "\n");
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    if (unassigned[c]) {
      fprintf(file, "%d", ARITY(c));
      for (i=0; i<ARITY(c); i++) {
	fprintf(file, " %d", num[SCOPEONE(c,i)]);
      }
      fprintf(file, " ");
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", truecartesianProduct(c));
      recursiveNaryPrint(file, c, 0, recursiveNaryAssignment, recursiveNaryPrintIdxVal); 
    }
  }
  /* binary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j)) {
	  fprintf(file, "2 %d %d ", num[i], num[j]);
	  FPRINTCOST(file, BOTTOM);
	  fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i])*List_n_of_elem(&feasible_values[j]));
	  idxa=0;
	  List_create(&feasible_values2[i]);
	  sort_val_lex(i);
	  List_reset_traversal(&feasible_values2[i]);
	  while(!List_end_traversal(&feasible_values2[i])) {
	    a = List_consult1(&feasible_values2[i]);
	    List_next(&feasible_values2[i]);
	    idxb=0;
	    List_create(&feasible_values2[j]);
	    sort_val_lex(j);
	    List_reset_traversal(&feasible_values2[j]);
	    while(!List_end_traversal(&feasible_values2[j])) {
	      b = List_consult1(&feasible_values2[j]);
	      List_next(&feasible_values2[j]);
	      fprintf(file, "%d %d ", idxa, idxb);
	      FPRINTCOST(file, CostB(i, a, j, b));
	      fprintf(file, "\n");
	      idxb++;
	    }
	    List_dispose_memory(&feasible_values2[j]);
	    idxa++;
	  }
	  List_dispose_memory(&feasible_values2[i]);  
	}
      }
    }
  }
  /* unary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      fprintf(file, "1 %d ", num[i]);
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i]));
      idxa=0;
      List_create(&feasible_values2[i]);
      sort_val_lex(i);
      List_reset_traversal(&feasible_values2[i]);
      while(!List_end_traversal(&feasible_values2[i])) {
	a = List_consult1(&feasible_values2[i]);
	List_next(&feasible_values2[i]);
	fprintf(file, "%d ", idxa);
	FPRINTCOST(file, CostU(i, a));
	fprintf(file, "\n");
	idxa++;
      }
      List_dispose_memory(&feasible_values2[i]);
    }
  }
  if (Bottom > BOTTOM) {
    fprintf(file, "0 ");
    FPRINTCOST(file, Bottom);
    fprintf(file, " 0\n");
  }

  fclose(file);
}

/* DIRECT ENCODING */

int recursiveNaryClauseCountDir(int c, int i, int *complassignment)
{
  int a;
  int sum = 0;

  if (i < ARITY(c)) {
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      complassignment[SCOPEONE(c,i)] = a;
      sum += recursiveNaryClauseCountDir(c, i+1, complassignment);
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else if (cost(c, complassignment) > BOTTOM) {
    sum = 1;
  }
  return sum;
}

void recursiveNaryClausePrintDir(FILE *file, int c, int i, int *complassignment, int *naryidxval, int *num)
{
  int a;

  if (i < ARITY(c)) {
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      naryidxval[i] = -num[SCOPEONE(c,i) * NValues + a];
      complassignment[SCOPEONE(c,i)] = a;
      recursiveNaryClausePrintDir(file, c, i+1, complassignment, naryidxval, num);
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else {
    if (cost(c, complassignment) > BOTTOM) {
      FPRINTCOST(file, cost(c, complassignment));
      for (i=0; i<ARITY(c); i++) {
	fprintf(file, " %d", naryidxval[i]);
      }
      fprintf(file, " 0\n");
    }
  }
}

/* file "convertsol" can be used to convert wcnf solution to wcsp solution
   each line contains at least three fields:
   field1: log-encoding-bit-number or -1 if direct encoding
   field2: boolean variable index (starting from 1)
   field3: wcsp variable index (starting from 0)
   field4: (if field1=0:) domain values represented by the log-encoding
           (if field1=-1) domain value associated to the boolean variable
	   (if field1>=1) nothing
*/
void saveWCNFDir(FILE* file) 
{
  int i,j,a,b,c;
  int nbvar = 0, nbclause = 0;
  int first;
  int num[NVar * NValues];
  int unassigned[NConstr];
  int recursiveNaryAssignment[MAXNVAR];
  int recursiveNaryPrintIdxVal[MAXARITY];
  FILE *translate;

  translate = fopen("convertsol","w");

  /* count the number of propositional variables */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      if (List_n_of_elem(&feasible_values[i]) == 2) {
	nbvar++;  /* count only one propositional variable for each boolean WCSP variable */
	if (translate) fprintf(translate, "0 %d %d", nbvar,i);
	first = TRUE;
	for (a=0; a<DOMSIZE(i); a++) {
	  if (current_domains[i][a]==-1) {
	    if (first) {
	      num[i * NValues + a] = nbvar;
	      first = FALSE;
	      if (translate) fprintf(translate, " %d", a);
	    } else {
	      num[i * NValues + a] = -nbvar;
	      if (translate) fprintf(translate, " %d", a);
	    }
	  }
	}
	if (translate) fprintf(translate, "\n");
	assert(first == FALSE);
      } else {
	for (a=0; a<DOMSIZE(i); a++) 
	{
	  if (current_domains[i][a]==-1) {
	    nbvar++;  /* count one propositional variable per value of a non boolean WCSP variable */
	    num[i * NValues + a] = nbvar;
	    if (translate) fprintf(translate, "-1 %d %d %d\n", nbvar,i,a);
	  }
	}
	nbclause++; /* add a positive hard clause to impose atleast one value selection per variable */
	nbclause+=DOMSIZE(i)*(DOMSIZE(i)-1);
      }
    }
  }

  if (translate) fclose(translate);

  /* count the number of clauses */
/*    if (Bottom > BOTTOM) nbclause += 2; // zero-arity cost */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      /* unary costs */
      for (a=0; a<DOMSIZE(i); a++) {
	if (current_domains[i][a]==-1) {
	  if (CostU(i,a) > BOTTOM) nbclause++; /* one unary clause per unary cost */
	}
      }
      /* binary costs */
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 &&  ISBINARY(i,j)) {
	  for (a=0; a<DOMSIZE(i); a++) {
	    if (current_domains[i][a]==-1) {
	      for (b=0; b<DOMSIZE(j); b++) {
		if (current_domains[j][b]==-1) {
		  if (CostB(i,a,j,b) > BOTTOM) nbclause++;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (assignments[SCOPEONE(c,i)]!=-1) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbclause += recursiveNaryClauseCountDir(c, 0, recursiveNaryAssignment);
    else if (ARITY(c) > 3) {
      /* ternary should have been transformed into binary by projectB */
      printf("Warning! partially assigned n-ary constraint %d ignored!\n", c);
    }
  }

  /* header */
  fprintf(file, "c %s\n", Filename);
  fprintf(file, "c PSEUDOBOOLEAN ");
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  if (Bottom > BOTTOM) 
  {
  	fprintf(file, "c LOWER BOUND ");
	FPRINTCOST(file, Bottom);
	fprintf(file, "\n");
  }

  fprintf(file, "p wcnf %d %d \n", nbvar, nbclause);

  /* domains of arity > 2 or == 1 => one positive hard clause  */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      if (List_n_of_elem(&feasible_values[i]) != 2) {
	FPRINTCOST(file, Top);
	for (a=0; a<DOMSIZE(i); a++) {
	  if (current_domains[i][a]==-1) {
	    fprintf(file, " %d", num[i * NValues + a]);
	  }
	}
	fprintf(file, " 0\n");
	
	for (a=0; a<DOMSIZE(i); a++) 
	{
	if (current_domains[i][a]==-1) 
	{
	
	for (b=0; b<DOMSIZE(i); b++) 
	{
	if (current_domains[i][b]==-1 && a!=b) 
	{
		FPRINTCOST(file, Top);
		fprintf(file, " -%d -%d 0\n", num[i * NValues + a],num[i * NValues + b]);
		
	}
	}
	
	
	}
	}
      }
    }
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    if (unassigned[c]) {
      recursiveNaryClausePrintDir(file, c, 0, recursiveNaryAssignment, recursiveNaryPrintIdxVal, num); 
    }
  }
  /* binary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j)) {
	  List_create(&feasible_values2[i]);
	  sort_val_lex(i);
	  List_reset_traversal(&feasible_values2[i]);
	  while(!List_end_traversal(&feasible_values2[i])) {
	    a = List_consult1(&feasible_values2[i]);
	    List_next(&feasible_values2[i]);
	    List_create(&feasible_values2[j]);
	    sort_val_lex(j);
	    List_reset_traversal(&feasible_values2[j]);
	    while(!List_end_traversal(&feasible_values2[j])) {
	      b = List_consult1(&feasible_values2[j]);
	      List_next(&feasible_values2[j]);
	      if (CostB(i, a, j, b) > BOTTOM) {
		FPRINTCOST(file, CostB(i, a, j, b));
		fprintf(file, " %d %d 0\n", -num[i * NValues + a], -num[j * NValues + b]);
	      }
	    }
	    List_dispose_memory(&feasible_values2[j]);
	  }
	  List_dispose_memory(&feasible_values2[i]);  
	}
      }
    }
  }
  /* unary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      List_create(&feasible_values2[i]);
      sort_val_lex(i);
      List_reset_traversal(&feasible_values2[i]);
      while(!List_end_traversal(&feasible_values2[i])) {
	a = List_consult1(&feasible_values2[i]);
	List_next(&feasible_values2[i]);
	if (CostU(i,a) > BOTTOM) {
	  FPRINTCOST(file, CostU(i, a));
	  fprintf(file, " %d 0\n", -num[i * NValues + a]);
	}
      }
      List_dispose_memory(&feasible_values2[i]);
    }
  }
  /* zero-arity cost */
  /*
  if (Bottom > BOTTOM) {
    FPRINTCOST(file, Bottom);
    fprintf(file, " 1 0\n");
    FPRINTCOST(file, Bottom);
    fprintf(file, " -1 0\n");
  }
  */

  fclose(file);
}

/* LOG ENCODING */

int recursiveNaryClauseCountLog(int c, int i, int *complassignment)
{
  int a;
  int sum = 0;

  if (i < ARITY(c)) {
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      complassignment[SCOPEONE(c,i)] = a;
      sum += recursiveNaryClauseCountLog(c, i+1, complassignment);
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else if (cost(c, complassignment) > BOTTOM) {
    sum = 1;
  }
  return sum;
}

void recursiveNaryClausePrintLog(FILE *file, int c, int i, int pos, int *complassignment, int *naryidxval,int **t,int **vars)
{
  int a,l;
  int dec;

  if (i < ARITY(c)) {
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) 
    {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      dec = 0;
      if (List_n_of_elem(&feasible_values[SCOPEONE(c,i)]) >=1)
      {
		// LLAMAR FUNCION i
		for(l=0;l<vars[SCOPEONE(c,i)][0];l++)
		{
			if(t[a][l]==0) naryidxval[pos+dec]=-(vars[SCOPEONE(c,i)][1]+l+1);
			else naryidxval[pos+dec]=vars[SCOPEONE(c,i)][1]+l+1;
			dec++;
		}
		//printf("0 \n");
		// END LLAMAR FUNCION i
      }
	
      complassignment[SCOPEONE(c,i)] = a;
      recursiveNaryClausePrintLog(file, c, i+1, pos + dec, complassignment, naryidxval, t,vars);
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else {
    if (cost(c, complassignment) > BOTTOM) {
      FPRINTCOST(file, cost(c, complassignment));
      for (i=0; i<pos; i++) {
	fprintf(file, " %d", naryidxval[i]);
      }
      fprintf(file, " 0\n");
    }
  }
}

void createTable(int **t)
{

t[0][7]=0;t[0][6]=0;t[0][5]=0;t[0][4]=0;t[0][3]=0;t[0][2]=0;t[0][1]=0;t[0][0]=0;
t[1][7]=0;t[1][6]=0;t[1][5]=0;t[1][4]=0;t[1][3]=0;t[1][2]=0;t[1][1]=0;t[1][0]=1;
t[2][7]=0;t[2][6]=0;t[2][5]=0;t[2][4]=0;t[2][3]=0;t[2][2]=0;t[2][1]=1;t[2][0]=0;
t[3][7]=0;t[3][6]=0;t[3][5]=0;t[3][4]=0;t[3][3]=0;t[3][2]=0;t[3][1]=1;t[3][0]=1;
t[4][7]=0;t[4][6]=0;t[4][5]=0;t[4][4]=0;t[4][3]=0;t[4][2]=1;t[4][1]=0;t[4][0]=0;
t[5][7]=0;t[5][6]=0;t[5][5]=0;t[5][4]=0;t[5][3]=0;t[5][2]=1;t[5][1]=0;t[5][0]=1;
t[6][7]=0;t[6][6]=0;t[6][5]=0;t[6][4]=0;t[6][3]=0;t[6][2]=1;t[6][1]=1;t[6][0]=0;
t[7][7]=0;t[7][6]=0;t[7][5]=0;t[7][4]=0;t[7][3]=0;t[7][2]=1;t[7][1]=1;t[7][0]=1;
t[8][7]=0;t[8][6]=0;t[8][5]=0;t[8][4]=0;t[8][3]=1;t[8][2]=0;t[8][1]=0;t[8][0]=0;
t[9][7]=0;t[9][6]=0;t[9][5]=0;t[9][4]=0;t[9][3]=1;t[9][2]=0;t[9][1]=0;t[9][0]=1;
t[10][7]=0;t[10][6]=0;t[10][5]=0;t[10][4]=0;t[10][3]=1;t[10][2]=0;t[10][1]=1;t[10][0]=0;
t[11][7]=0;t[11][6]=0;t[11][5]=0;t[11][4]=0;t[11][3]=1;t[11][2]=0;t[11][1]=1;t[11][0]=1;
t[12][7]=0;t[12][6]=0;t[12][5]=0;t[12][4]=0;t[12][3]=1;t[12][2]=1;t[12][1]=0;t[12][0]=0;
t[13][7]=0;t[13][6]=0;t[13][5]=0;t[13][4]=0;t[13][3]=1;t[13][2]=1;t[13][1]=0;t[13][0]=1;
t[14][7]=0;t[14][6]=0;t[14][5]=0;t[14][4]=0;t[14][3]=1;t[14][2]=1;t[14][1]=1;t[14][0]=0;
t[15][7]=0;t[15][6]=0;t[15][5]=0;t[15][4]=0;t[15][3]=1;t[15][2]=1;t[15][1]=1;t[15][0]=1;
t[16][7]=0;t[16][6]=0;t[16][5]=0;t[16][4]=1;t[16][3]=0;t[16][2]=0;t[16][1]=0;t[16][0]=0;
t[17][7]=0;t[17][6]=0;t[17][5]=0;t[17][4]=1;t[17][3]=0;t[17][2]=0;t[17][1]=0;t[17][0]=1;
t[18][7]=0;t[18][6]=0;t[18][5]=0;t[18][4]=1;t[18][3]=0;t[18][2]=0;t[18][1]=1;t[18][0]=0;
t[19][7]=0;t[19][6]=0;t[19][5]=0;t[19][4]=1;t[19][3]=0;t[19][2]=0;t[19][1]=1;t[19][0]=1;
t[20][7]=0;t[20][6]=0;t[20][5]=0;t[20][4]=1;t[20][3]=0;t[20][2]=1;t[20][1]=0;t[20][0]=0;
t[21][7]=0;t[21][6]=0;t[21][5]=0;t[21][4]=1;t[21][3]=0;t[21][2]=1;t[21][1]=0;t[21][0]=1;
t[22][7]=0;t[22][6]=0;t[22][5]=0;t[22][4]=1;t[22][3]=0;t[22][2]=1;t[22][1]=1;t[22][0]=0;
t[23][7]=0;t[23][6]=0;t[23][5]=0;t[23][4]=1;t[23][3]=0;t[23][2]=1;t[23][1]=1;t[23][0]=1;
t[24][7]=0;t[24][6]=0;t[24][5]=0;t[24][4]=1;t[24][3]=1;t[24][2]=0;t[24][1]=0;t[24][0]=0;
t[25][7]=0;t[25][6]=0;t[25][5]=0;t[25][4]=1;t[25][3]=1;t[25][2]=0;t[25][1]=0;t[25][0]=1;
t[26][7]=0;t[26][6]=0;t[26][5]=0;t[26][4]=1;t[26][3]=1;t[26][2]=0;t[26][1]=1;t[26][0]=0;
t[27][7]=0;t[27][6]=0;t[27][5]=0;t[27][4]=1;t[27][3]=1;t[27][2]=0;t[27][1]=1;t[27][0]=1;
t[28][7]=0;t[28][6]=0;t[28][5]=0;t[28][4]=1;t[28][3]=1;t[28][2]=1;t[28][1]=0;t[28][0]=0;
t[29][7]=0;t[29][6]=0;t[29][5]=0;t[29][4]=1;t[29][3]=1;t[29][2]=1;t[29][1]=0;t[29][0]=1;
t[30][7]=0;t[30][6]=0;t[30][5]=0;t[30][4]=1;t[30][3]=1;t[30][2]=1;t[30][1]=1;t[30][0]=0;
t[31][7]=0;t[31][6]=0;t[31][5]=0;t[31][4]=1;t[31][3]=1;t[31][2]=1;t[31][1]=1;t[31][0]=1;


t[32][7]=0;t[32][6]=0;t[32][5]=1;t[32][4]=0;t[32][3]=0;t[32][2]=0;t[32][1]=0;t[32][0]=0;
t[33][7]=0;t[33][6]=0;t[33][5]=1;t[33][4]=0;t[33][3]=0;t[33][2]=0;t[33][1]=0;t[33][0]=1;
t[34][7]=0;t[34][6]=0;t[34][5]=1;t[34][4]=0;t[34][3]=0;t[34][2]=0;t[34][1]=1;t[34][0]=0;
t[35][7]=0;t[35][6]=0;t[35][5]=1;t[35][4]=0;t[35][3]=0;t[35][2]=0;t[35][1]=1;t[35][0]=1;
t[36][7]=0;t[36][6]=0;t[36][5]=1;t[36][4]=0;t[36][3]=0;t[36][2]=1;t[36][1]=0;t[36][0]=0;
t[37][7]=0;t[37][6]=0;t[37][5]=1;t[37][4]=0;t[37][3]=0;t[37][2]=1;t[37][1]=0;t[37][0]=1;
t[38][7]=0;t[38][6]=0;t[38][5]=1;t[38][4]=0;t[38][3]=0;t[38][2]=1;t[38][1]=1;t[38][0]=0;
t[39][7]=0;t[39][6]=0;t[39][5]=1;t[39][4]=0;t[39][3]=0;t[39][2]=1;t[39][1]=1;t[39][0]=1;
t[40][7]=0;t[40][6]=0;t[40][5]=1;t[40][4]=0;t[40][3]=1;t[40][2]=0;t[40][1]=0;t[40][0]=0;
t[41][7]=0;t[41][6]=0;t[41][5]=1;t[41][4]=0;t[41][3]=1;t[41][2]=0;t[41][1]=0;t[41][0]=1;
t[42][7]=0;t[42][6]=0;t[42][5]=1;t[42][4]=0;t[42][3]=1;t[42][2]=0;t[42][1]=1;t[42][0]=0;
t[43][7]=0;t[43][6]=0;t[43][5]=1;t[43][4]=0;t[43][3]=1;t[43][2]=0;t[43][1]=1;t[43][0]=1;
t[44][7]=0;t[44][6]=0;t[44][5]=1;t[44][4]=0;t[44][3]=1;t[44][2]=1;t[44][1]=0;t[44][0]=0;
t[45][7]=0;t[45][6]=0;t[45][5]=1;t[45][4]=0;t[45][3]=1;t[45][2]=1;t[45][1]=0;t[45][0]=1;
t[46][7]=0;t[46][6]=0;t[46][5]=1;t[46][4]=0;t[46][3]=1;t[46][2]=1;t[46][1]=1;t[46][0]=0;
t[47][7]=0;t[47][6]=0;t[47][5]=1;t[47][4]=0;t[47][3]=1;t[47][2]=1;t[47][1]=1;t[47][0]=1;
t[48][7]=0;t[48][6]=0;t[48][5]=1;t[48][4]=1;t[48][3]=0;t[48][2]=0;t[48][1]=0;t[48][0]=0;
t[49][7]=0;t[49][6]=0;t[49][5]=1;t[49][4]=1;t[49][3]=0;t[49][2]=0;t[49][1]=0;t[49][0]=1;
t[50][7]=0;t[50][6]=0;t[50][5]=1;t[50][4]=1;t[50][3]=0;t[50][2]=0;t[50][1]=1;t[50][0]=0;
t[51][7]=0;t[51][6]=0;t[51][5]=1;t[51][4]=1;t[51][3]=0;t[51][2]=0;t[51][1]=1;t[51][0]=1;
t[52][7]=0;t[52][6]=0;t[52][5]=1;t[52][4]=1;t[52][3]=0;t[52][2]=1;t[52][1]=0;t[52][0]=0;
t[53][7]=0;t[53][6]=0;t[53][5]=1;t[53][4]=1;t[53][3]=0;t[53][2]=1;t[53][1]=0;t[53][0]=1;
t[54][7]=0;t[54][6]=0;t[54][5]=1;t[54][4]=1;t[54][3]=0;t[54][2]=1;t[54][1]=1;t[54][0]=0;
t[55][7]=0;t[55][6]=0;t[55][5]=1;t[55][4]=1;t[55][3]=0;t[55][2]=1;t[55][1]=1;t[55][0]=1;
t[56][7]=0;t[56][6]=0;t[56][5]=1;t[56][4]=1;t[56][3]=1;t[56][2]=0;t[56][1]=0;t[56][0]=0;
t[57][7]=0;t[57][6]=0;t[57][5]=1;t[57][4]=1;t[57][3]=1;t[57][2]=0;t[57][1]=0;t[57][0]=1;
t[58][7]=0;t[58][6]=0;t[58][5]=1;t[58][4]=1;t[58][3]=1;t[58][2]=0;t[58][1]=1;t[58][0]=0;
t[59][7]=0;t[59][6]=0;t[59][5]=1;t[59][4]=1;t[59][3]=1;t[59][2]=0;t[59][1]=1;t[59][0]=1;
t[60][7]=0;t[60][6]=0;t[60][5]=1;t[60][4]=1;t[60][3]=1;t[60][2]=1;t[60][1]=0;t[60][0]=0;
t[61][7]=0;t[61][6]=0;t[61][5]=1;t[61][4]=1;t[61][3]=1;t[61][2]=1;t[61][1]=0;t[61][0]=1;
t[62][7]=0;t[62][6]=0;t[62][5]=1;t[62][4]=1;t[62][3]=1;t[62][2]=1;t[62][1]=1;t[62][0]=0;
t[63][7]=0;t[63][6]=0;t[63][5]=1;t[63][4]=1;t[63][3]=1;t[63][2]=1;t[63][1]=1;t[63][0]=1;



}

void saveWCNFLog(FILE* file) 
{
  int i,j,a,b,c,cu;
  int newV;
  int nbvar = 0, nbclause = 0;
  int **vars;
  int **t;
  int l;
  int recursiveNaryAssignment[MAXNVAR];
  int recursiveNaryPrintIdxVal[MAXARITY*2];
  int unassigned[NConstr];
  FILE *translate;

  translate = fopen("convertsol","w");


t=(int **)malloc(sizeof(int)*256);
for(i=0;i<256;i++)
{
	t[i]=(int *)malloc(sizeof(int)*NVar*10);
}

vars=(int **)malloc(sizeof(int)*NVar);
for(i=0;i<NVar;i++)
{
	vars[i]=(int *)malloc(sizeof(int)*3);
}


createTable(t);

  cu=0;
  nbvar=0;
  for (i=0; i<NVar; i++) 
  {
    if (assignments[i]==-1) 
    {
      if (List_n_of_elem(&feasible_values[i]) >= 1) 
      {
      		newV=(int) sqrt(DOMSIZE(i));
		if(DOMSIZE(i)==2)
		{
			vars[i][0]=1;
			vars[i][1]=nbvar;
			vars[i][2]=nbvar;
			
		}
		else
		{
			
			//if((float)sqrt(DOMSIZE(i)) > (float)newV) newV++;
			if(DOMSIZE(i)>2 && DOMSIZE(i)<=4) newV=2;
			else if (DOMSIZE(i)>4 && DOMSIZE(i)<=7) newV=3;
			else if (DOMSIZE(i)>7 && DOMSIZE(i)<=15) newV=4;
			else if (DOMSIZE(i)>15 && DOMSIZE(i)<=31) newV=5;
			else if (DOMSIZE(i)>31 && DOMSIZE(i)<=63) newV=6;
			else if (DOMSIZE(i)>63 && DOMSIZE(i)<=127) newV=7;
			
			vars[i][0]=newV;
			vars[i][1]=nbvar;
			vars[i][2]=nbvar+newV;
			nbclause+=pow(2,newV)-DOMSIZE(i); // Hard unary constraints
			cu+=pow(2,newV)-DOMSIZE(i);
			//printf("\n D:%d %f %d\n",DOMSIZE(i),newV);
			//getchar();
		}
		
		//printf("\n VAR %d : %d - %d",i,vars[i][1],vars[i][2]);
		if (translate) {
		  fprintf(translate, "0 %d %d", nbvar+1,i);
		  for (a=0; a<DOMSIZE(i); a++) {
		    fprintf(translate, " %d", a);
		  }
		  fprintf(translate, "\n");
		  for (a=1; a<newV; a++) {
		    fprintf(translate, "%d %d %d\n", a, nbvar+1+a,i);
		  }
		}
		nbvar+=newV;
      }
     }
   }
   //printf("\n CU %d \n",cu);

  if (translate) fclose(translate);
     
  /* count the number of clauses */
/*    if (Bottom > BOTTOM) nbclause += 2; // zero-arity cost */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      /* unary costs */
      for (a=0; a<DOMSIZE(i); a++) {
	if (current_domains[i][a]==-1) {
	  if (CostU(i,a) > BOTTOM) nbclause++; /* one unary clause per unary cost */
	}
      }
      /* binary costs */
      
      
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 &&  ISBINARY(i,j)) {
	  for (a=0; a<DOMSIZE(i); a++) {
	    if (current_domains[i][a]==-1) {
	      for (b=0; b<DOMSIZE(j); b++) {
		if (current_domains[j][b]==-1) {
		  if (CostB(i,a,j,b) > BOTTOM) nbclause++;
		}
	      }
	    }
	  }
	}
      }
      
      
      
    }
  }
  
  /* n-ary constraints */

  
  for (c=0; c<NConstr; c++) {
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (assignments[SCOPEONE(c,i)]!=-1) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbclause += recursiveNaryClauseCountLog(c, 0, recursiveNaryAssignment);
    else if (ARITY(c) > 3) {
      // ternary should have been transformed into binary by projectB 
      printf("Warning! partially assigned n-ary constraint %d ignored!\n", c);
    }
  }
  


  /* header */
  fprintf(file, "c %s\n", Filename);
  fprintf(file, "c PSEUDOBOOLEAN ");
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  if (Bottom > BOTTOM) 
  {
  	fprintf(file, "c LOWER BOUND ");
	FPRINTCOST(file, Bottom);
	fprintf(file, "\n");
  }
  fprintf(file, "p wcnf %d %d \n", nbvar, nbclause);
  
  /* unary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      
      List_create(&feasible_values2[i]);
      sort_val_lex(i);
      List_reset_traversal(&feasible_values2[i]);
      while(!List_end_traversal(&feasible_values2[i])) {
	a = List_consult1(&feasible_values2[i]);
	List_next(&feasible_values2[i]);
	if (CostU(i,a) > BOTTOM) 
	{
	  FPRINTCOST(file, CostU(i, a));
	  // LLAMAR FUNCION i
	  for(l=0;l<vars[i][0];l++)
	  {
	  	if(t[a][l]==0) fprintf(file," -%d ",vars[i][1]+l+1);
		else fprintf(file," %d ",vars[i][1]+l+1);
	  }
	  // END LLAMAR FUNCION i
	  fprintf(file, " 0\n");
	}
      }
      List_dispose_memory(&feasible_values2[i]);
    }
  }
  
 
  
  /* binary constraints */
  
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j)) {
	  List_create(&feasible_values2[i]);
	  sort_val_lex(i);
	  List_reset_traversal(&feasible_values2[i]);
	  while(!List_end_traversal(&feasible_values2[i])) {
	    a = List_consult1(&feasible_values2[i]);
	    List_next(&feasible_values2[i]);
	    List_create(&feasible_values2[j]);
	    sort_val_lex(j);
	    List_reset_traversal(&feasible_values2[j]);
	    while(!List_end_traversal(&feasible_values2[j])) {
	      b = List_consult1(&feasible_values2[j]);
	      List_next(&feasible_values2[j]);
	      if (CostB(i, a, j, b) > BOTTOM) 
	      {
	        if(CostB(i, a, j, b)>Top) FPRINTCOST(file,Top);
		else FPRINTCOST(file, CostB(i, a, j, b));
		// LLAMAR FUNCION i
		for(l=0;l<vars[i][0];l++)
		{
			if(t[a][l]==0) fprintf(file," -%d ",vars[i][1]+l+1);
			else fprintf(file," %d ",vars[i][1]+l+1);
		}
		// END LLAMAR FUNCION i
		// LLAMAR FUNCION j
		for(l=0;l<vars[j][0];l++)
		{
			if(t[b][l]==0) fprintf(file," -%d ",vars[j][1]+l+1);
			else fprintf(file," %d ",vars[j][1]+l+1);
		}
		// END LLAMAR FUNCION j
		fprintf(file, " 0\n");
	      }
	    }
	    List_dispose_memory(&feasible_values2[j]);
	  }
	  List_dispose_memory(&feasible_values2[i]);
	}
      }
    }
  }
  

  /* n-ary constraints */
  
  
  for (c=0; c<NConstr; c++) {
    if (unassigned[c])
    {
    	 recursiveNaryClausePrintLog(file, c, 0, 0, recursiveNaryAssignment, recursiveNaryPrintIdxVal,t,vars); 
    }
  } 
  
  // Add "Hard" Unary constraints
  
  //fprintf(file,"\n");
  cu=0;
  for (i=0; i<NVar; i++) 
  {
    if (assignments[i]==-1 && DOMSIZE(i)>2) 
    {
      if (List_n_of_elem(&feasible_values[i]) >= 1) 
      {
      		for(a=DOMSIZE(i);a<pow(2,vars[i][0]);a++)
		{
			cu++;
			//printf("\n %d %d \n",DOMSIZE(i),a);
			FPRINTCOST(file,Top);
			// LLAMAR FUNCION i
			for(l=0;l<vars[i][0];l++)
			{
				if(t[a][l]==0) fprintf(file," -%d ",vars[i][1]+l+1);
				else fprintf(file," %d ",vars[i][1]+l+1);
			}
			// END LLAMAR FUNCION i
			fprintf(file, " 0\n");
		}
		
      }
     }
   }
   //printf("\n CU %d \n",cu);
  
  /* zero-arity cost */
  /*
  if (Bottom > BOTTOM) 
  {
    FPRINTCOST(file, Bottom);
    fprintf(file, " 1 0\n");
    FPRINTCOST(file, Bottom);
    fprintf(file, " -1 0\n");
  }
  */
  
for(i=0;i<256;i++)
{
	free(t[i]);
}
free(t);


for(i=0;i<NVar;i++)
{
	free(vars[i]);
}
free(vars);


  fclose(file);
}


 


// SAVE THE WCSP PROBLEM IN WCNF FORMAT

void saveWCNF(FILE* file)
{
	if(wcnfType==WT_DIR) saveWCNFDir(file);
	else saveWCNFLog(file);
}

