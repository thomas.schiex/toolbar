#! /bin/tcsh

set repeat = 10
set n = 40
set c = 50

foreach k (2 3)
  @ c = 50
  while ($c <= 3000)
    mkcnf${k}.sh $n $c 1 $repeat
    @ c += 50
  end
end
