This directory is a repository for weighted CSP and weighted Max-SAT problems (with or without hard constraints).
See the readme file in each directory for more information.

academics		Some academic problems (warehouse location problems,
			4 Queens, Zebra, crypto-arithmetic puzzles, etc.)

auction			Combinatorial auctions problems generated using CATS

bep				Mission management problems for agile satellites.	

celar			CELAR benchmark (radio link frequency assignment problems)

coloring		Graph coloring instances turned into minimum coloring problems.

dimacs			Second  DIMACS Challenge (unsatisfiable SAT instances)

ergo			Bayesian network problems in ergo format (see ERGO.ps)

generators		Binary random (structured -kbtree- or not) Weighted CSP generator, 
				random k-SAT generator, and weighted queen problem generator

jnh			JNH benchmark (weighted Max-SAT with varying clause length)

kbtree			highly structured random WCSP problem.

maxclique		DIMACS maximum clique problems and bioinformatics protein prediction
			instances.

maxcsp			Random binary Max-CSP instances
			More instances are located in toolbar/web directory.

maxcut			Random and spinglass maxcut instances.

maxone			Random and DIMACS derived instances

pedigree		Real and randomly-generated animals pedigree diagnosis instances

planning		Optimal cost planning problems

randomksat		Random Max-SAT instances with constant clause length

spot5			SPOT5 benchmark (earth observation daily management problems, CNES)

translators		Translators between cp, wcsp, ds, and cnf formats
			(translators for specific Max-SAT solvers can be found in dimacs, jnh, 
			 and randomksat directories)

warehouses		 Warehouse location problems 
