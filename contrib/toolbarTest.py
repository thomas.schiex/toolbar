#!/usr/bin/python

#-----------------------------------------------------------------------------
# Purpose:     Run several tests on toolbar, error checking BEDA.
#
# Author:      Paul Maier
#
# Created:     27.03.2008
# RCS-ID:      $Id$
# SVN-ID:      $Id$
# Copyright:   
# Licence:     <your licence>
#-----------------------------------------------------------------------------

"""Run several tests on toolbar, error checking BEDA.
"""

from result import *
import unittest, os

TOOLBAR_EXE = "../toolbar-mulcyber/toolbar"
TEST_FILES = ["../benchs/academics/8wqueens.wcsp", 
              "../benchs/academics/16wqueens.wcsp", 
              "../benchs/academics/zebra.wcsp"]

# reference solutions computed with toulbar2, option s for computing all solutions
# or computed with toolbar -C -v 
CORRECTNESS_TEST_FILES = [
      ("../benchs/academics/4wqueens.wcsp", [[2, 0, 3, 1]]),
      ("../benchs/academics/8wqueens.wcsp", [[1, 4, 6, 3, 0, 7, 5, 2]]),
      ("../benchs/academics/zebra.wcsp", [[0, 2, 4, 3, 1, 0, 4, 2, 1, 3, 0, 2, 1, 3, 4, 4, 
                                1, 0, 3, 2, 3, 2, 4, 0, 1]]),
      ("../benchs/academics/16wqueens.wcsp", [[3, 12, 8, 4, 2, 15, 5, 14, 10, 0, 6, 1, 7, 13, 11, 9],
                          [3, 12, 8, 4, 2, 15, 5, 11, 13, 0, 6, 1, 7, 10, 14, 9]]),
      ("../benchs/academics/langford_2_4.wcsp", [[4, 0, 1, 2, 6, 3, 5, 7],
                                       [1, 4, 2, 0, 3, 7, 6, 5]]),
      ("../benchs/academics/donald.wcsp", [[4, 2, 6, 4, 8, 0, 9, 6, 3, 0, 1, 1, 0, 1, 1]])

] 
TMP_OUTPUT = "tmpOut.txt"

class ToolbarTest(unittest.TestCase):
    
    def setUp(self):
        unittest.TestCase.setUp(self)
    
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        
    def testDomainSizeReduction(self):
        
        for testFile in TEST_FILES:
            os.system(TOOLBAR_EXE + " -C -v -j -M256 " + testFile + " > "+
                      TMP_OUTPUT)
            parser = OutputParser(TMP_OUTPUT, [parseElementToolbarBEDA])
            result = parser.parse()
            
            for msgSize, abstractMsgSize in zip(result.BEmsgSizes, 
                                                result.BEDAmsgSizes):
                self.assert_(msgSize >= abstractMsgSize)
    
    def testCoreToolbarTests(self):
        """
        Test cases embedded in toolbar.
        """
        print
        print "----------------------------------------------------------------"
        print "toolbar integrated tests: >>>"
        os.system(TOOLBAR_EXE + " --test")
        print "toolbar integrated tests end <<<"
        print "----------------------------------------------------------------"
            
    
    def testBEDACorrectness(self):
        for testFile, optima in CORRECTNESS_TEST_FILES:
            print "testing problem " + testFile + " ... "

            os.system(TOOLBAR_EXE + " -C -v -j -M256 " + testFile + " > " +
                      TMP_OUTPUT)
            parser = OutputParserToolbarSolution(TMP_OUTPUT)
            result = parser.parse()
            self.assert_(result.solution in optima)
    
        
if __name__ == '__main__':
    unittest.main()