/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbmain.c
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "tbsystem.h"
#include "wcsp.h"
#include "wmaxsat.h"
#include "solver.h"
#include "preproject.h"
#include "ergoreader.h"
#include "treedec.h"
#include "be.h"
#ifdef BEDA
#include "domainAbstractionBE.h"
#include "abstractionFunction.h"
#include "allTests.h"
#endif

static double SolvingTime;
char * CertificateName;

/* function called when time limit is out */
void timeOut() {
  int i;

  SolvingTime = cpuTime() - SolvingTime;
  if (BEMode) {
	  printf("BE time out after %g seconds.\n",SolvingTime);
	  return;
  }
  if(FileFormat==FORMAT_CNF || FileFormat==FORMAT_WCNF) saveResultFileX(SolvingTime);
  if (BestSol[0] != -1) {
    printf("Best bound: ");
    PRINTCOST(Top);
    printf(" in %lu nodes and %g seconds.\n",  NNodes, SolvingTime);
  } else {
    if (AllSolutions) {
      printf("%d solution(s) found after %lu nodes and %g seconds.\n", NumberOfSolutions, NNodes, SolvingTime);
    } else {
      printf("No solution found after %lu nodes and %g seconds.\n", NNodes, SolvingTime);
    }
  }
  if (FileFormat == FORMAT_BAYESNET)
    printf("Loglikelihood %lf (p = %le)\n", Cost2LogLike(Top),Cost2Proba(Top));
  if (Verbose && BestSol[0] != -1) {
    printf("Best solution:");
    for (i=0; i<NVar; i++) {
      printf(" %d", BestSol[i]);
    }
    printf("\n");
  }
} /* no need to call the exit functions, it is done in tbsystem.c */

/* function called when node limit is out */
void nodeOut()
{
  if(Verbose)
	printf("\nNode limit expired... Aborting...\n");
  timeOut();
  exit(0);
}

int localSearchWCSP(cost_t *upperbound)
{
	int i;
	int varIndex;
	int value;
	char line[1024];
	char keyword[1024];
	char *fich = "resincop";
	cost_t tmpUB=-1;
	FILE *file;
	FILE *sol;
	cost_t bestcost=MAXCOST;
	cost_t solcost=MAXCOST;
	int *bestsol=NULL;
	int *solution=NULL;
	bestsol = mycalloc(NVar, sizeof(int));
	if (bestsol==NULL) return FALSE;
	solution = mycalloc(NVar, sizeof(int));
	if (solution==NULL) return FALSE;

	sprintf(line,"./narycsp %s %s 0 1 5 idwa 100000 cv v 0 200 1 0 0", fich, Filename);
	system(line);
	
	file = fopen(fich, "r");
	if(file == NULL)
	{	
		return FALSE;
	}
	
	while (!feof(file)) {
	  fscanf(file, "%s", keyword);
	  if (strcmp(keyword, "Meilleur")==0) {
            fscanf(file, "%s", keyword);
            if (strcmp(keyword, "essai")==0) {
	      fscanf(file, "%s", keyword); // skip ":"
	      SCANCOST(file, &tmpUB);
	      if (tmpUB>=0 && tmpUB < *upperbound) *upperbound = tmpUB;
	      if (WriteSolution && *upperbound == bestcost) {
		sol = fopen("sol", "w");
		if(sol == NULL)
		  {	
		    return FALSE;
		  }
		for (i=0; i<NVar; i++) {
		  fprintf(sol," %d", bestsol[i]);
		}
		fprintf(sol,"\n");
		fclose(sol);
	      }
	      break;
            }
	  }
	  if (strcmp(keyword, "variable")==0) {
            fscanf(file, "%d", &varIndex);
            fscanf(file, "%s", keyword); // skip ":"
            fscanf(file, "%d", &value);
            solution[varIndex] = value;
	  }
	  if (strcmp(keyword, "verification")==0) {
            SCANCOST(file, &solcost);
            if (solcost < bestcost) {
	      bestcost = solcost;
	      for (i=0; i<NVar; i++) {
		  bestsol[i] = solution[i];
	      }
            }
	  }
	}
	fclose(file);
	sprintf(line,"rm -f %s",fich);
	system(line);
	return TRUE;
}

int main(int argc,char ** argv) {

  int i;

  FILE* fileHandle;

  Process_Options(argc,argv);
  SeedInit(Seed);
  
#ifdef BEDA  
  if(TestMode) { //run tests upon provided option --test
	  RunAllTests();
	  return (0);
  }
#endif

  if (Filename) {
    fileHandle = fopen(Filename, "r");
    if(fileHandle == NULL) {
      perror("Error: bad filename!"); 
      abort();
    }
  } else fileHandle = stdin;

  switch (FileFormat) {
  case FORMAT_WCSP : readWCSP(fileHandle); if (!BEMode) allocateWCSP(); break;
  case FORMAT_CNF : readMAXSATandWMAXSATProblems(fileHandle, TRUE); break;
  case FORMAT_WCNF : readMAXSATandWMAXSATProblems(fileHandle, FALSE); break;
  case FORMAT_BAYESNET : ErgoRead(fileHandle); if (!BEMode) allocateWCSP(); break;
  }
  fclose(fileHandle);

  /* test WEIGHTCONSTRAINT 
     for (i=0; i < NConstr; i++) {
     printf("%d %g\n", i, WEIGHTCONSTRAINT(i));
     } */

  /************************************************/
  /* If we choose to compute a tree decomposition */
  /************************************************/

  if (TreedecMode)
    {
      computeTreedec ();
#ifdef BEDA
      if (!BEMode && !DomainAbstractionBEMode) return (0);
#else
      if (!BEMode) return (0);
#endif
    }

  if (BEMode) {
	solveBE();
	return (0);
  }
#ifdef BEDA  
  if (DomainAbstractionBEMode) {
      if (UserTau >= NUM_DIFFERENT_TAUS) {
          printf("Unknown abstraction function: %d \n", UserTau);
          abort();
      }
      if (Verbose) {
          printf("chosen abstraction function is %s\n", UserTauName[UserTau]);
      }
      
	  solveDomainAbstractionBE();
	  return (0);
  }
#endif
  allocateSol();

  if (LocalSearchPreprocessing) localSearchWCSP(&Top);

  if (Verbose) {
    printf("Top = ");
    PRINTCOST(Top);
    printf("\n");
  }

  if (TimeLimit) {
    timer(TimeLimit);
  }

  SolvingTime = cpuTime();
  
  if (preProject >2) Project2Binary(preProject, 0);

  /* checking certificate */
  if (CertificateName && !AllSolutions) {
    fileHandle = fopen(CertificateName, "r");
    if(fileHandle == NULL) {
      perror("Error: bad certificate filename!"); 
      abort();
    }
    for (i=0; i<NVar; i++) {fscanf(fileHandle,"%d",&(BestSol[i]));}
    printf("Certificate: ");
    for (i=0; i<NVar; i++) { printf(" %d", BestSol[i]);}
    printf("\n");
    certificateCheck(BestSol);
    return 0;
  }

  #ifdef UNIX
	signal(SIGINT,  timeout);
  #endif

  switch (FileFormat) {
  case FORMAT_WCSP : solve(); break;
  case FORMAT_CNF : solveMAXSATandWMAXSATProblems(); break;
  case FORMAT_WCNF : solveMAXSATandWMAXSATProblems(); break;
  case FORMAT_BAYESNET : solve(); break;
  }

  SolvingTime = cpuTime() - SolvingTime;

  if (TimeLimit) {
    timerStop();
  }

  if (AllSolutions) {
    printf("%d solution(s)", NumberOfSolutions);
  } else {
    if (BestSol[0] != -1) {
      printf("Optimum: ");
    } else {
      printf("Lower bound: ");
    }
    PRINTCOST(Top);
  }
  printf(" in %lu nodes and %g seconds", NNodes, SolvingTime);
  if ((Options & OPTIONS_SINGLETON_PRE) || (Options & OPTIONS_SINGLETON_BB)) 
    printf(" (singleton: %lu nodes)", SingletonNNodes);
  printf(".\n");
    
  if (FileFormat == FORMAT_BAYESNET)
    printf("Loglikelihood %lf (p = %le)\n", Cost2LogLike(Top),Cost2Proba(Top));

  if (Verbose && BestSol[0] != -1) {
    printf("Optimal solution:");
    for (i=0; i<NVar; i++) {
      printf(" %d", BestSol[i]);
    }
    printf("\n");
  }

  switch (FileFormat) {
  case FORMAT_WCSP : freeWCSP(); break;
  case FORMAT_CNF : break; /* already done in wmaxsat.c */
  case FORMAT_WCNF : break;
  case FORMAT_BAYESNET : freeWCSP(); break;
  }
  freeSol();

  return 0;
}
