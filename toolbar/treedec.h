/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tblist.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef TREEDEC_H
#define TREEDEC_H

#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"

#define EDGE 1
#define FILL_IN 2
#define PSEUDO 4

#define LATEX 1
#define EPS 2



extern int *Ordering; /* This is the array for the ordering */
extern int *InverseOrdering; /* The inverse of the ordering */
extern int **ElimGraph; /* This if the incidence matrix for the elimination graph :
			   0 means that there is no edge between i and j,
			   1 means that there is an edge,
			   3 means that this is a fill-in edge (EDGE+FILL_IN)
			   5 means that this is a pseudo edge (EDGE+PSEUDO) (i.e. where
			   at least one endpoint is already ordered),
			   7 means that this is a pseudo fill-in edge
			   (EDGE+PSEUDO+FILL_IN)
			   4, 6 or >7 don't exist
			   (mean that the program is in trouble). */
extern int *HeuristicArray; /* This is the array of values of the heuristic
			       for each vertex */
typedef struct JG
{
  int **cliques; /* Each row is a clique, with the number of vertices at position 1
		    and the vertices after. */
  int **edges; /* The incidence matrix,with for each edge the weight = nb of vertices
		  shared by the two cliques */
  int root; /* The root of the graph */
  int nCliques; /* The number of cliques in the junction graph */
} JunctionGraph;

extern JunctionGraph Junction;

extern int graphicFormat;


/*====================================================*/
/*                                                    */
/*       Main procedures (compute the treedec)        */
/*                                                    */
/*====================================================*/

extern void computeTreedec ();
extern void computeOrder (int heuristic);
extern void KosterTreedec ();

/*====================================================*/
/*                                                    */
/*            Initialization procedures               */
/*                                                    */
/*====================================================*/


/************************************************/
/* The following procedure initializes the      */
/* incidence matrix.                            */
/************************************************/

extern void initAdj ();

/************************************************/
/* The following procedure initializes the      */
/* heuristic array with the values given by the */
/* heuristic choosen.                           */
/************************************************/

extern void initHeuristicArray ();


/*====================================================*/
/*                                                    */
/*  Subfunctions for the computation of an ordering   */
/*                                                    */
/*====================================================*/

/************************************************/
/* The following procedure "removes" a vertex   */
/* from the graph by correctly updating the     */
/* array of heuristic and the incidence matrix. */
/************************************************/

extern void removeVertex (int vertex);

/************************************************/
/* The following procedure add the fill-in of   */
/* a vertex to the elimgraph by updating the    */
/* incidence matrix with the fill-in edges.     */
/************************************************/

extern void addFillIn (int vertex);

/*************************************************/
/* The following procedures return a vertex      */
/* choosen by a particular heuristic...          */
/*************************************************/

extern int getMaxHeuristic ();
extern int getMinHeuristic ();

/*************************************************/
/* The following procedure returns the fillIn    */
/* produced by the elimination of the vertex.    */
/*************************************************/

extern int getFillIn (int vertex);

/*************************************************/
/* The following procedure swaps two vertices in */
/* the matrix of incidence.                      */
/*************************************************/

extern void swapVertices (int v1, int v2, int *tmpOrdering);

/*************************************************/
/* The following procedure reorders the matrix   */
/* of incidence thanks to the ordering found.    */
/*************************************************/

extern void reorderMatrix ();

/*************************************************/
/* The following procedure computes the junction */
/* graph corresponding to the ordering.          */
/*************************************************/

extern void buildJunctionGraph ();

/*====================================================*/
/*                                                    */
/*    Subfunctions for the computation of a tree      */
/*    decomposition with the algorithm of Koster      */
/*                                                    */
/*====================================================*/


/* The sub-procedure for the tree decomposition. */
extern void splitCluster (int cluster);
extern int getSeparatorSize (int clique1, int clique2);
extern int getConnectedComponents (int **incidence, int *minCut, int **connected, int nvertices);
extern void getSeparator (int *separator, int clique1, int clique2);
/* For further improvements (not yet so well studied !) */
extern int preprocess (int *simplicialArray, int *vertices, int **incidence);


/*====================================================*/
/*                                                    */
/* Subfunctions for the computation of the parameters */
/*                                                    */
/*====================================================*/


/*************************************************/
/* The following procedure computes the treewidth*/
/* (induced width) of the elim-graph             */
/*************************************************/

extern int getTreeWidth ();

/*************************************************/
/* The following procedure computes the lowest   */
/* height of the tree by trying to root the tree */
/* with each node.                               */
/*************************************************/

extern int setRoot ();
extern int getTreeHeight ();
extern int recursiveTreeHeight (int *labnodes, int node);


/*************************************************/
/* The following procedure computes the maximum  */
/* separator size, just by finding the max in    */
/* the incidence matrix of the junction tree.    */
/*************************************************/

extern int getMaximumSeparatorSize ();

/*************************************************/
/* The following procedure computes the total    */
/* number of vertices in the separators, just by */
/* reading the incidence matrix of the junc tree.*/
/*************************************************/

extern int getTotalSeparatorSize ();

/*************************************************/
/* The following procedure computes the parameter*/
/*  ====      =====      |                       */
/*  \         || ||    __|                       */
/*  /         || ||   /  |                       */
/*  ====      || ||   \__|                       */
/* cliques  variables                            */
/*************************************************/

extern double getDomCliqueSize ();

/*************************************************/
/* The following procedure computes the parameter*/
/*  ====      =====      |                       */
/*  \         || ||    __|                       */
/*  /         || ||   /  |                       */
/*  ====      || ||   \__|                       */
/*  seps    variables                            */
/*************************************************/

extern double getDomSepSize ();

/*====================================================*/
/*                                                    */
/* Some test procedures to check the algorithm (to be */
/*  removed as soon as we're sure that the algorithm  */
/*                 works as expected...               */
/*                                                    */
/*====================================================*/

extern void testIfTreedec (JunctionGraph j, int **incidence, int nvertices, int *ordering);
extern int testIfTree (int **incidence, int *vertices, int nvertices, int vertex, int pa);


/*====================================================*/
/*                                                    */
/*  Some graphical utility to draw constraint graphs  */
/*   with a kind of clustering to LaTeX with eepic.   */
/*                                                    */
/*====================================================*/

extern void recursiveClustGraphic (int s, int **clustCoordonnees, int level);
extern void exportTreedecToGraphic ();
extern void writeGraphicFile (char *fileName, int **clustCoordonnees, float **coordonnees, int figure);
extern int PPGraphic (int vertex, int **clustCoordonnees, int *colors, int *pi, int depth, int breadth, int *m);
extern void drawLine (FILE *ptrfile, int x1, int y1, int x2, int y2);
extern void dottedLine (FILE *ptrfile, int x1, int y1, int x2, int y2);
extern void drawCircle (FILE *ptrfile, int x1, int y1, float r);
extern void drawText (FILE *ptrfile, int x1, int y1, char *s);
extern void printHeader (FILE *ptrfile);
extern void printEndDocument (FILE *ptrfile);


/*====================================================*/
/*                                                    */
/* Some graph tools such as flow algorithms that are  */
/*              used by the program.                  */
/*                                                    */
/*====================================================*/

/* The Prim's algorithm to find a spanning tree of minimal weight */
extern void prim (int **edges, int s);
/* The Edmond-Karp's algorithm to find a maximal flow (similar
   to Ford-Fulkerson but with breadth first search) */
extern int EK (int **incidence, int s, int t, int nvertices, int ub);
/* The breadth first search that is used by the EK algorithm */
extern int BFS (int **incidence, int s, int t, int nvertices, int *cut, int *colors, int *d, int *pi);
/* At each step of the EK algorithm, updates the flow */
extern int updateFlow (int **incidence, int *pi, int s, int t);
/* Finds the minimum separating vertex cut */
extern int findMinCut (int **incidence, int *minCut, int nvertices);
/* Finds the minimum cut between s and t */
extern int findMinCutst (int **FFmatrix, int **incidence, int **minCut, int s, int t, int nvertices, int ub);
/* -- No more used (the new version uses the EK's algorithm -- */
extern void FordFulkersonize (int **incidence, int o, int t, int nvertices);
extern int findAugmentingPath (int **incidence, int *path, int *vertices, int v, int t, int capacity, int nvertices);

/*====================================================*/
/*                                                    */
/*                Some usefull macros                 */
/*                                                    */
/*====================================================*/

#define IS_EDGE_BUT_NOT_PSEUDO(i, j) ElimGraph[i][j]&!(ElimGraph[i][j]&PSEUDO)
#define IS_EDGE_BUT_NOT_FILL_IN(i, j) ElimGraph[i][j]&!(ElimGraph[i][j]&FILL_IN)
#define IS_EDGE(i, j) ElimGraph[i][j]
#define IS_FILL_IN(i, j) (ElimGraph[i][j]&FILL_IN)
#define IS_PSEUDO(i, j) (ElimGraph[i][j]&PSEUDO)

#endif
