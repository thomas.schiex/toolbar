# Probleme du poisson soumis par A. Einstein
# nom du problem et coutmaximum=1 (CSP)
Einstein 1

# Position des maisons
# position de la maison bleue
P_B 1 2 3 4 5
# position de la maison blanc
P_W 1 2 3 4 5
# position de la maison jaune
P_J 1 2 3 4 5
# position de la maison rouge
P_R 1 2 3 4 5
# position de la maison vert
P_V 1 2 3 4 5
hard(alldiff(P_B,P_W,P_J,P_R,P_V))

# Animal du Allemand
#1:chat, 2:cheval, 3:chien, 4:oiseaux, 5:poisson
A_L 1 2 3 4 5
# Cigarette du Allemand
#1:Blend, 2:Blue Master, 3:Dunhill, 4:Pall Mall, 5:Prince
C_L 1 2 3 4 5
# Boisson du Allemand
#1:Biere, 2:Cafe, 3:Eau, 4:Lait, 5:The
B_L 1 2 3 4 5
# Position de la maison du Allemand
P_L 1 2 3 4 5
# Couleur de la maison du Allemand
#1:Blanc, 2:Bleue, 3:Jaune, 4:Rouge, 5:Vert 
M_L 1 2 3 4 5
hard(equiv(M_L==1, P_L==P_W))
hard(equiv(M_L==2, P_L==P_B))
hard(equiv(M_L==3, P_L==P_J))
hard(equiv(M_L==4, P_L==P_R))
hard(equiv(M_L==5, P_L==P_V))

# Animal du Anglais
#1:chat, 2:cheval, 3:chien, 4:oiseaux, 5:poisson
A_A 1 2 3 4 5
# Cigarette du Anglais
#1:Blend, 2:Blue Master, 3:Dunhill, 4:Pall Mall, 5:Prince
C_A 1 2 3 4 5
# Boisson du Anglais
#1:Biere, 2:Cafe, 3:Eau, 4:Lait, 5:The
B_A 1 2 3 4 5
# Position de la maison du Anglais
P_A 1 2 3 4 5
# Couleur de la maison du Anglais
#1:Blanc, 2:Bleue, 3:Jaune, 4:Rouge, 5:Vert 
M_A 1 2 3 4 5
hard(equiv(M_A==1, P_A==P_W))
hard(equiv(M_A==2, P_A==P_B))
hard(equiv(M_A==3, P_A==P_J))
hard(equiv(M_A==4, P_A==P_R))
hard(equiv(M_A==5, P_A==P_V))

# Animal du Danois
#1:chat, 2:cheval, 3:chien, 4:oiseaux, 5:poisson
A_D 1 2 3 4 5
# Cigarette du Danois
#1:Blend, 2:Blue Master, 3:Dunhill, 4:Pall Mall, 5:Prince
C_D 1 2 3 4 5
# Boisson du Danois
#1:Biere, 2:Cafe, 3:Eau, 4:Lait, 5:The
B_D 1 2 3 4 5
# Position de la maison du Danois
P_D 1 2 3 4 5
# Couleur de la maison du Danois
#1:Blanc, 2:Bleue, 3:Jaune, 4:Rouge, 5:Vert 
M_D 1 2 3 4 5
hard(equiv(M_D==1, P_D==P_W))
hard(equiv(M_D==2, P_D==P_B))
hard(equiv(M_D==3, P_D==P_J))
hard(equiv(M_D==4, P_D==P_R))
hard(equiv(M_D==5, P_D==P_V))

# Animal du Norvegien
#1:chat, 2:cheval, 3:chien, 4:oiseaux, 5:poisson
A_N 1 2 3 4 5
# Cigarette du Norvegien
#1:Blend, 2:Blue Master, 3:Dunhill, 4:Pall Mall, 5:Prince
C_N 1 2 3 4 5
# Boisson du Norvegien
#1:Biere, 2:Cafe, 3:Eau, 4:Lait, 5:The
B_N 1 2 3 4 5
# Position de la maison du Norvegien
P_N 1 2 3 4 5
# Couleur de la maison du Norvegien
#1:Blanc, 2:Bleue, 3:Jaune, 4:Rouge, 5:Vert 
M_N 1 2 3 4 5
hard(equiv(M_N==1, P_N==P_W))
hard(equiv(M_N==2, P_N==P_B))
hard(equiv(M_N==3, P_N==P_J))
hard(equiv(M_N==4, P_N==P_R))
hard(equiv(M_N==5, P_N==P_V))

# Animal du Suedois
#1:chat, 2:cheval, 3:chien, 4:oiseaux, 5:poisson
A_S 1 2 3 4 5
# Cigarette du Suedois
#1:Blend, 2:Blue Master, 3:Dunhill, 4:Pall Mall, 5:Prince
C_S 1 2 3 4 5
# Boisson du Suedois
#1:Biere, 2:Cafe, 3:Eau, 4:Lait, 5:The
B_S 1 2 3 4 5
# Position de la maison du Suedois
P_S 1 2 3 4 5
# Couleur de la maison du Suedois
#1:Blanc, 2:Bleue, 3:Jaune, 4:Rouge, 5:Vert 
M_S 1 2 3 4 5
hard(equiv(M_S==1, P_S==P_W))
hard(equiv(M_S==2, P_S==P_B))
hard(equiv(M_S==3, P_S==P_J))
hard(equiv(M_S==4, P_S==P_R))
hard(equiv(M_S==5, P_S==P_V))

hard(alldiff(A_S, A_N, A_D, A_A, A_L))
hard(alldiff(B_S, B_N, B_D, B_A, B_L))
hard(alldiff(C_S, C_N, C_D, C_A, C_L))
hard(alldiff(P_S, P_N, P_D, P_A, P_L))
hard(alldiff(M_S, M_N, M_D, M_A, M_L))

#1
hard(M_A==4)
#2
hard(A_S==3)
#3
hard(B_D==5)
#4
hard(P_V == P_W - 1)
#5
hard(equiv(M_S==5, B_S==2))
hard(equiv(M_L==5, B_L==2))
hard(equiv(M_A==5, B_A==2))
hard(equiv(M_D==5, B_D==2))
hard(equiv(M_N==5, B_N==2))
#6
hard(equiv(C_S==4, A_S==4))
hard(equiv(C_L==4, A_L==4))
hard(equiv(C_A==4, A_A==4))
hard(equiv(C_D==4, A_D==4))
hard(equiv(C_N==4, A_N==4))
#7
hard(equiv(M_S==3, C_S==3))
hard(equiv(M_L==3, C_L==3))
hard(equiv(M_A==3, C_A==3))
hard(equiv(M_D==3, C_D==3))
hard(equiv(M_N==3, C_N==3))
#8
hard(equiv(P_S==3, B_S==4))
hard(equiv(P_L==3, B_L==4))
hard(equiv(P_A==3, B_A==4))
hard(equiv(P_D==3, B_D==4))
hard(equiv(P_N==3, B_N==4))
#9
hard(P_N==1)
#10
hard(implies(C_S==1 && A_N==1, abs(P_S-P_N)==1))
hard(implies(C_S==1 && A_L==1, abs(P_S-P_L)==1))
hard(implies(C_S==1 && A_A==1, abs(P_S-P_A)==1))
hard(implies(C_S==1 && A_D==1, abs(P_S-P_D)==1))

hard(implies(C_L==1 && A_N==1, abs(P_L-P_N)==1))
hard(implies(C_L==1 && A_S==1, abs(P_L-P_S)==1))
hard(implies(C_L==1 && A_A==1, abs(P_L-P_A)==1))
hard(implies(C_L==1 && A_D==1, abs(P_L-P_D)==1))

hard(implies(C_A==1 && A_N==1, abs(P_A-P_N)==1))
hard(implies(C_A==1 && A_L==1, abs(P_A-P_L)==1))
hard(implies(C_A==1 && A_S==1, abs(P_A-P_S)==1))
hard(implies(C_A==1 && A_D==1, abs(P_A-P_D)==1))

hard(implies(C_D==1 && A_N==1, abs(P_D-P_N)==1))
hard(implies(C_D==1 && A_L==1, abs(P_D-P_L)==1))
hard(implies(C_D==1 && A_A==1, abs(P_D-P_A)==1))
hard(implies(C_D==1 && A_S==1, abs(P_D-P_S)==1))

hard(implies(C_N==1 && A_S==1, abs(P_N-P_S)==1))
hard(implies(C_N==1 && A_L==1, abs(P_N-P_L)==1))
hard(implies(C_N==1 && A_A==1, abs(P_N-P_A)==1))
hard(implies(C_N==1 && A_D==1, abs(P_N-P_D)==1))

hard(!(C_S==1 && A_S==1))
hard(!(C_N==1 && A_N==1))
hard(!(C_D==1 && A_D==1))
hard(!(C_A==1 && A_A==1))
hard(!(C_L==1 && A_L==1))
#11
hard(implies(C_S==3 && A_N==2, abs(P_S-P_N)==1))
hard(implies(C_S==3 && A_L==2, abs(P_S-P_L)==1))
hard(implies(C_S==3 && A_A==2, abs(P_S-P_A)==1))
hard(implies(C_S==3 && A_D==2, abs(P_S-P_D)==1))

hard(implies(C_L==3 && A_N==2, abs(P_L-P_N)==1))
hard(implies(C_L==3 && A_S==2, abs(P_L-P_S)==1))
hard(implies(C_L==3 && A_A==2, abs(P_L-P_A)==1))
hard(implies(C_L==3 && A_D==2, abs(P_L-P_D)==1))

hard(implies(C_A==3 && A_N==2, abs(P_A-P_N)==1))
hard(implies(C_A==3 && A_L==2, abs(P_A-P_L)==1))
hard(implies(C_A==3 && A_S==2, abs(P_A-P_S)==1))
hard(implies(C_A==3 && A_D==2, abs(P_A-P_D)==1))

hard(implies(C_D==3 && A_N==2, abs(P_D-P_N)==1))
hard(implies(C_D==3 && A_L==2, abs(P_D-P_L)==1))
hard(implies(C_D==3 && A_A==2, abs(P_D-P_A)==1))
hard(implies(C_D==3 && A_S==2, abs(P_D-P_S)==1))

hard(implies(C_N==3 && A_S==2, abs(P_N-P_S)==1))
hard(implies(C_N==3 && A_L==2, abs(P_N-P_L)==1))
hard(implies(C_N==3 && A_A==2, abs(P_N-P_A)==1))
hard(implies(C_N==3 && A_D==2, abs(P_N-P_D)==1))

hard(!(C_S==3 && A_S==2))
hard(!(C_N==3 && A_N==2))
hard(!(C_D==3 && A_D==2))
hard(!(C_A==3 && A_A==2))
hard(!(C_L==3 && A_L==2))
#12
hard(equiv(C_S==2, B_S==1))
hard(equiv(C_N==2, B_N==1))
hard(equiv(C_D==2, B_D==1))
hard(equiv(C_A==2, B_A==1))
hard(equiv(C_L==2, B_L==1))
#13
hard(C_L==5)
#14
hard(M_N != 2)
hard(abs(P_N-P_B)==1)
#15
hard(implies(C_S==1 && B_N==3, abs(P_S-P_N)==1))
hard(implies(C_S==1 && B_L==3, abs(P_S-P_L)==1))
hard(implies(C_S==1 && B_A==3, abs(P_S-P_A)==1))
hard(implies(C_S==1 && B_D==3, abs(P_S-P_D)==1))

hard(implies(C_L==1 && B_N==3, abs(P_L-P_N)==1))
hard(implies(C_L==1 && B_S==3, abs(P_L-P_S)==1))
hard(implies(C_L==1 && B_A==3, abs(P_L-P_A)==1))
hard(implies(C_L==1 && B_D==3, abs(P_L-P_D)==1))

hard(implies(C_A==1 && B_N==3, abs(P_A-P_N)==1))
hard(implies(C_A==1 && B_L==3, abs(P_A-P_L)==1))
hard(implies(C_A==1 && B_S==3, abs(P_A-P_S)==1))
hard(implies(C_A==1 && B_D==3, abs(P_A-P_D)==1))

hard(implies(C_D==1 && B_N==3, abs(P_D-P_N)==1))
hard(implies(C_D==1 && B_L==3, abs(P_D-P_L)==1))
hard(implies(C_D==1 && B_A==3, abs(P_D-P_A)==1))
hard(implies(C_D==1 && B_S==3, abs(P_D-P_S)==1))

hard(implies(C_N==1 && B_S==3, abs(P_N-P_S)==1))
hard(implies(C_N==1 && B_L==3, abs(P_N-P_L)==1))
hard(implies(C_N==1 && B_A==3, abs(P_N-P_A)==1))
hard(implies(C_N==1 && B_D==3, abs(P_N-P_D)==1))

hard(!(C_S==1 && B_S==3))
hard(!(C_N==1 && B_N==3))
hard(!(C_D==1 && B_D==3))
hard(!(C_A==1 && B_A==3))
hard(!(C_L==1 && B_L==3))

