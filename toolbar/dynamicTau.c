/*
 * Purpose: This module provides functions to generate abstraction functions
 * dynamically, or rather, choosing the neccessary value abstraction functions.
 * 
 * CVS-ID: $ID: $
 * SVN-ID: $ID $
 * 
 * author maierpa
 *
 */
#include <math.h>
#include <limits.h>
#include "abstractionFunction.h"
#include "dynamicTau.h"
#include "moduloN_vtau.h"
#include "measurementsBE.h"

int bucketScope(bool_t *bScope, bucket b);
void emptyScope(bool_t *bScope);

/**
 * Generate the value abstraction functions for a simple abstraction function.
 * The idea here is to abstract the domains in such a way that for each bucket
 * 
 * cGenerated->nTuples < maxTuples
 * 
 * holds. cGenerated is the constraint that will be computed from the other 
 * constraints in the bucket via abstractVarProjection(). This can be expressed 
 * as an optimization problem:
 * 
 * For each bucket  
 * 
 * maximize dax1 * ... * daxv
 * 			dax1 * ... * daxv <= maxTuples
 * 
 * 		with dax1, ..., daxv being the domain sizes of variables x1, ..., xv in 
 * 		scope(bucketMessage)
 * 
 * The algorithm implemented here is rather dumb, though.
 * 
 * PRE:
 * 	dtauData->vtaus has NVar elements
 *  dtauData->domsizes has NVar elements
 */
void generateSimpleAbstractionData(dynamic_tau_data_t * dtauData, 
								   bucket *buckets, int maxTuples) {
	int maxDoms[NVar];
	int i = 0;
	int j = 0;
	bool_t msgScope[NVar];
	bool_t bucketScopes[NVar][NVar];

	for (i = 0; i < NVar; ++i) {
		maxDoms[i] = DomainSize[i];
		msgScope[i] = FALSE;
		
		for (j = 0; j < NVar; ++j) {
			bucketScopes[j][i] = FALSE;
		}
	}
	
	for (i = NVar-1; i >= 0; i--) {
		
		int nBucketVars = bucketScope(msgScope, buckets[i]);
		// add variables from prior computed messages
		int nMsgVars = nBucketVars-1; //one less because the bucket's var. is projected out
		for (j = 0; j < NVar; j++) {
			if (bucketScopes[i][j]) {
				msgScope[j] = TRUE;
				nMsgVars++;
			}
		}
		msgScope[i] = FALSE; //the message scope is the bucket scope without the
							 //bucket variable

		bool_t scopeIsEmpty = TRUE;
		for (j = 0; j < NVar; j++) {
			scopeIsEmpty &= !msgScope[j];
		}
		if (scopeIsEmpty) continue;
		
		// now add the msg scope to the bucket the msg would be send to
		for (j = i; j >= 0; j--) {
			if (msgScope[j]) {
				/*
				 * Traversing the variable ordering backwards, the 
				 * first one in the msg's scope is the variable of the target 
				 * bucket. 
				 */
				int var = 0;
				for (; var < NVar; var++) {
					if (msgScope[var]) bucketScopes[j][var] = TRUE;
				}
				break;
			}
		}
		
		
		/*
		 * first compute an estimate of the no. of tuples of the constraint that 
		 * would be generated in this bucket. Why? Because the number of tuples
		 * could actually grow so huge that the variable nTuples overflows 
		 * (and since this is compiled with -ftrapv the program aborts). Of 
		 * course one could use a bigger variable (like int64_t or something), but
		 * if nTuples overflows we know that it is bigger than maxTuples (you 
		 * don't want so many tuples). So here we simply compute log2(nTuples)
		 * and check whether it's larger than 32. If so, we set nTuples to maxInt
		 * and leave it at that.
	     */
		int nTuples = 1;
		float lognTuples = 0;
		for (j = i-1; j >= 0; j--) {
			if (msgScope[j]) lognTuples += log2i(maxDoms[j]);
		}
		
		if (lognTuples >= log2i(INT_MAX)) {
			nTuples = INT_MAX;
		} else {
			// compute the no. of tuples exactly
			for (j = i-1; j >= 0; j--) {
				if (msgScope[j])
					nTuples *= maxDoms[j];
			}
		}
		
		/*
		 *  if the no. of tuples is too big, we need to restrain it
		 * a new max. domain size is computed for all variables in the current
		 * bucket:
		 * 
		 * 		maxdom = nMsgVars_root(maxTuples)
		 * 
		 * so that
		 * 		
		 * 		maxdom^nMsgVars <= maxTuples
		 * 
		 * holds.
		 */
		if (nTuples > maxTuples) {
			double resArity = 1.0/(double)nMsgVars;
			int maxdom = (int)pow(maxTuples, resArity);

			int abstractNTuples = 1;

			for (j = 0; j < NVar; ++j) {
				if (msgScope[j]) {
					maxDoms[j] = MIN(maxDoms[j], maxdom);
					abstractNTuples *= maxDoms[j]; 
				}
			}
		}
		
		emptyScope(msgScope);
	}
	
	if (Verbose) {
		printf("maxDoms = [");
		for (i = 0; i < NVar; ++i) {
			printf("%d,", maxDoms[i]);
		}
		printf("]\n");
	}
	
	/*
	 * Now we've got the max. domain sizes for all variables of the WCSP. In the
	 * next step abstraction functions for single variable values are chosen in
	 * such a way that the abstract domain size is less or equal to the max. 
	 * domain size of the according variable. In this case this is achieved by
	 * chosing a function
	 * 
	 * f: v_x -> v_x%maxDoms[x]
	 * 
	 * for each variable x in X, the set of variables of the problem.
	 */
	
	for (i = 0; i < NVar; ++i) {
		dtauData->domsizes[i] = maxDoms[i];
		
		if (maxDoms[i] < DomainSize[i]) {
			if (maxDoms[i] > VTAU_MAX_MODULO) {
				printf("Computed an abstract domain bigger than "
						"VTAU_MAX_MODULO\n");
				abort();
			}
			dtauData->vtaus[i] = ModuloN_vtaus[maxDoms[i]];
		} else {
			dtauData->vtaus[i] = vtauID;
		}
	}
}

/**
 * Compute union of all scopes of all constraints in buckets[i] and store it
 * in bScope. The function assumes that no constraints have been generated yet,
 * i.e. b->primera_func_anidada == NULL.
 * 
 * @return the number of variables in this bucket
 */
int bucketScope(bool_t *bScope, bucket b) {
	nodofunc *c = NULL;
	int varInd = 0;
	int nBucketVars = 0;
	
	c = b.primera_func_original;
	
	while (c != NULL) {
		for (varInd = 0; varInd < c->aridad; ++varInd) {
			if (bScope[c->vars[varInd]] == FALSE) {
				nBucketVars++;
				bScope[c->vars[varInd]] = TRUE;
			}
		}
		c = c->sig_func;
	}
	return nBucketVars;
}

/**
 * Empty the passed scope.
 */
void emptyScope(bool_t *bScope) {
	int i = 0;
	for (i = 0; i < NVar; ++i) bScope[i] = FALSE;
}
