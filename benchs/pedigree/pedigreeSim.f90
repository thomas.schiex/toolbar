      program Pedigree
!---- Simulation of populations (05/04/05)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      implicit none
      character(30) :: pedfile1  
      character(30) :: input
      integer :: l,i,j,m,nk,jk,i9,mk,nag,n1,n2,ni,nc,gbon
      integer :: ngen,n,male,na,nfe,female,la 
      integer,allocatable :: sex(:),gen1(:),gen2(:),is(:),id(:)
      integer,allocatable :: iv1(:),iv2(:),iv3(:),iv4(:)
      integer,allocatable :: iv51(:),iv52(:),iv6(:),iv7(:),iv8(:)
      integer,allocatable :: vc(:),ge1(:),ge2(:)      
      real  (kind=8):: temp,b1,b2,b3,b4,b5,lp
!-------------------------------------------------------------------

  open(unit=1,file='ped.pre',status='replace')
  open(unit=2,file='ped.errors',status='replace')
  
  call getarg(1, input)
  read(input,*) ngen 
  write(*,*) 'ngen=', ngen

  call getarg(2, input)
  read(input,*) n 
  write(*,*) 'n=', n

  call getarg(3, input)
  read(input,*) male 
  write(*,*) 'male=', male

  lp = 0.1
  call random_seed

!#######################################################################
  nfe=n-male
  na=(ngen*nfe)+n
  female=nfe/male 
  
  allocate (sex(n),gen1(n),gen2(n))
  allocate (is(male),id(nfe))
  allocate (iv1(na),iv2(na),iv3(na),iv4(na),iv51(na),iv52(na))
  allocate (iv6(na),iv7(na),iv8(na)) 
  allocate (vc(na),ge1(na),ge2(na))    

  iv1(:) = 0;  iv2(:) = 0
  iv3(:) = 0;  iv4(:) = 0
  iv51(:) = 0; iv52(:) = 0;
  iv6(:) = 0; iv7(:) = 0;  iv8(:) = 0

  b1=(1.0/9.0);b2=(3.0/9.0);b3=(5.0/9.0);b4=(6.0/9.0);b5=(8.0/9.0)
!----- First generation -----------------------------
      do i=1,n                  
       call random_number(temp)   
       if (temp <= b1)then
       gen1(i)=1; gen2(i)=1
       elseif (temp > b1.and.temp <= b2) then
       gen1(i)=1; gen2(i)=2
       elseif (temp > b2.and.temp <= b3) then
       gen1(i)=1; gen2(i)=3       
       elseif (temp > b3.and.temp <= b4) then
       gen1(i)=2; gen2(i)=2
       elseif (temp > b4.and.temp <= b5) then
       gen1(i)=2; gen2(i)=3              
       elseif (temp > b5) then
       gen1(i)=3; gen2(i)=3
       endif

       if(i <= male)then         !individuals' sex
       sex(i)=1
       else
       sex(i)=2
       endif
        
       iv1(i) = i
       iv4(i) = sex(i)
       iv51(i) = gen1(i) 
       iv52(i) = gen2(i)                         
      enddo
      
      do i=1,male
      is(i)=iv1(i)
      enddo

      do i=1,nfe   !n-male
      id(i)=iv1(i+male)
      enddo

!-----------------------
!     Random mates
!----------------------- 
      l = n
      ni=nfe
      do m = 1, ngen
      write(*,*)'generation ',m
       jk=0
       do i=1,male
	nk=0
        do 200 j=1,female
        jk=jk+1
!------ No mates between father-daughter, mother-son
        if(iv2(id(jk))==is(i).or.iv3(is(i))==id(jk))then
         i9=i+1
         if (i9 > male) i9=1
	else  
         i9=i
        endif
        l=l+1
        if(l > na) goto 5678

!----- Progeny's genotypes
       if(iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
          iv51(id(jk))==1.and.iv52(id(jk))==1)then
          iv51(l)=1; iv52(l)=1
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
               iv51(id(jk))==1.and.iv52(id(jk))==2).or.&
              (iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==1))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=1
              elseif (temp > 0.50) then
              iv51(l)=1; iv52(l)=2
	      endif
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
               iv51(id(jk))==1.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==1))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=1
              elseif (temp > 0.50) then
              iv51(l)=1; iv52(l)=3
	      endif	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==2).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==1))then
              iv51(l)=1; iv52(l)=2
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==1))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.50) then
              iv51(l)=1; iv52(l)=3
	      endif	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==1.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==1))then
              iv51(l)=1; iv52(l)=3
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==1.and.iv52(id(jk))==2).or.&
              (iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=1; iv52(l)=1
              elseif (temp > 0.25 .and. temp <= 0.75) then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.75) then
              iv51(l)=2; iv52(l)=2	      
	      endif	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==1.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=1; iv52(l)=1
              elseif (temp > 0.25 .and. temp <= 0.50) then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.50 .and. temp <= 0.75) then	      
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.75) then
              iv51(l)=2; iv52(l)=3	      
	      endif	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==2).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.50) then
              iv51(l)=2; iv52(l)=2
	      endif	 	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.25 .and. temp <= 0.50) then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.50 .and. temp <= 0.75) then	      
              iv51(l)=2; iv52(l)=2
              elseif (temp > 0.75) then
              iv51(l)=2; iv52(l)=3	      
	      endif		      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.50) then
              iv51(l)=2; iv52(l)=3
	      endif	      
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==1.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==3))then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=1; iv52(l)=1
              elseif (temp > 0.25 .and. temp <= 0.75) then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.75) then
              iv51(l)=3; iv52(l)=3	      
	      endif	
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==2).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==3))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.50) then
              iv51(l)=2; iv52(l)=3
	      endif 
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==3))then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=1; iv52(l)=2
              elseif (temp > 0.25 .and. temp <= 0.50) then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.50 .and. temp <= 0.75) then	      
              iv51(l)=2; iv52(l)=3
              elseif (temp > 0.75) then
              iv51(l)=3; iv52(l)=3	      
	      endif
       elseif((iv51(is(i9))==1.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==1.and.iv52(id(jk))==3))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=1; iv52(l)=3
              elseif (temp > 0.50) then
              iv51(l)=3; iv52(l)=3
	      endif 	      
       elseif(iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
              iv51(id(jk))==2.and.iv52(id(jk))==2)then
              iv51(l)=2; iv52(l)=2
       elseif((iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==2.and.iv52(id(jk))==2))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=2; iv52(l)=2
              elseif (temp > 0.50) then
              iv51(l)=2; iv52(l)=3
	      endif 	      
       elseif((iv51(is(i9))==2.and.iv52(is(i9))==2.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==2.and.iv52(id(jk))==2))then
              iv51(l)=2; iv52(l)=3
       elseif(iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==2.and.iv52(id(jk))==3)then
              call random_number(temp)
              if (temp <= 0.25)then
              iv51(l)=2; iv52(l)=2
              elseif (temp > 0.25 .and. temp <= 0.75) then
              iv51(l)=2; iv52(l)=3
              elseif (temp > 0.75) then
              iv51(l)=3; iv52(l)=3	      
	      endif
       elseif((iv51(is(i9))==2.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3).or.&
              (iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
	       iv51(id(jk))==2.and.iv52(id(jk))==3))then
              call random_number(temp)
              if (temp <= 0.50)then
              iv51(l)=2; iv52(l)=3
              elseif (temp > 0.50) then
              iv51(l)=3; iv52(l)=3
	      endif 	      
       elseif(iv51(is(i9))==3.and.iv52(is(i9))==3.and.&
               iv51(id(jk))==3.and.iv52(id(jk))==3)then
              iv51(l)=3; iv52(l)=3
      endif		 
       
!----- Progeny's sex 

 call random_number(temp)
 if(temp <= 0.50)then
 iv4(l)=1
 else
 iv4(l)=2
 endif

!------Progeny's age
       
       iv1(l) = l
       iv2(l) = is (i9)
       iv3(l) = id (jk)
       iv6(l) = 0
       iv7(l) = m
       mk=l       !mk, # of indiv generated until this generation
200    continue
      enddo
!------------------------------------------------------------------
      
      nag=mk-nfe+1    !individuals of the last year

5678  continue

!-----The end of the generation--------------------

 call parents

      if(m==1)then
       do i=n+1,mk
       iv8(i)=i-n   !iv8=family
       enddo
      else
       do i=n+1,nag  !all indiv except first and last generation 
       do j=nag,mk   !last generation
        if(iv2(i)==iv2(j).and.iv3(i)==iv3(j)) iv8(j)=iv8(i)
        if(iv8(iv2(i))==0) iv8(iv2(i))=iv8(i)
        if(iv8(iv3(i))==0) iv8(iv3(i))=iv8(i)
       enddo
       enddo
       do i=n+1,nag
       do j=nag,mk
	if(iv8(j).eq.0) then
	ni=ni+1
	iv8(j)=ni
	endif
       enddo
       enddo
      endif
      	
      enddo
!--------------------------------------------------------------------------
1234  continue

      call randomloss  !unknown genotypes

!---- Prints--------------------------------------------------------------
15    format (8(3x,i5))
      write(*,*) 'n� of founders=', n
      write(*,*) 'n� of animals=', na
!--      write(1,*) 
      do i=1,na
!--  write(1,15) iv1(i),iv4(i),iv2(i),iv3(i),iv51(i),iv52(i),ge1(i),ge2(i)
     if(ge1(i).ne.0 .and. iv2(i).ne.0 .and.iv3(i).ne.0 .and. ge1(iv2(i)).eq.0 .and. ge1(iv3(i)).eq.0)then
     call random_number(temp)
     if(temp <= 0.5)then
      gbon=ge1(i)  
      call random_number(temp)
      if(temp <= 0.250) ge1(i)=1
      if(temp > 0.250 .and. temp <= 0.5 ) ge1(i)=2
      if(temp > 0.50 .and. temp < 0.75 ) ge1(i)=3
      if(temp >= 0.750) ge1(i)=4
      if(gbon.ne.ge1(i))then
       print*,iv1(i)
       write(2,15) iv1(i),gbon
      endif
     endif
     endif
     write(1,15) 1,iv1(i),iv2(i),iv3(i),iv4(i),ge1(i),ge2(i)
      enddo   
      close(1)
      close(2)


 contains
 
!------------------------
  Subroutine Parents
!------------------------
implicit none
integer :: i,n1,n2,ik_ma,ik_fe
integer, allocatable, dimension (:) :: iw_ma,iw_fe,vg_ma,iwksp_ma
integer :: sele_ma(male),sele_fe(nfe)

ik_ma=0
ik_fe=0
do i=1,mk    !all the individuals 
 if(iv4(i)==1)  ik_ma=ik_ma+1
 if(iv4(i)==2)  ik_fe=ik_fe+1
enddo
allocate(iw_fe(ik_fe),iw_ma(ik_ma))
allocate(vg_ma(ik_ma),iwksp_ma(ik_ma))

n1=0
do i=1,mk   
if(iv4(i)==1)then 
 n1=n1+1
 iw_ma(n1)=iv1(i)
! vg_ma(n1)=iv5(i) 
endif
enddo 
n2=0     
do i=1,mk 
if(iv4(i)==2)then
 n2=n2+1
 iw_fe(n2)=iv1(i)
endif
enddo

 call random_permu (iw_ma)   
 sele_ma=iw_ma(1:male)
 do i=1,male
 is(i)=sele_ma(i)
 enddo
 
 call random_permu (iw_fe) 
 sele_fe=iw_fe(1:nfe)
 do i=1,nfe
 id(i)=sele_fe(i)
 enddo
!----------------------------- 
 end subroutine parents
!-----------------------------

!----------------------------
 subroutine Random_permu(iw)
!----------------------------
implicit none
integer :: iw(:)
real (kind=8) :: w(size(iw),2)
 call random_number(w(:,1))
w(:,2)=real(iw)
 call sortmat2(w,1)
iw=nint(w(:,2))
!----------------------------
end subroutine random_permu
!----------------------------

!-----------------------------------
 subroutine sortmat2 (matrix,ipos)
!-----------------------------------
!brute force sorting routine
 implicit none
 integer       :: i, ipos, jmax, m
 real(kind=8) :: matrix(:,:), temp(size(matrix,dim=2)), vmin
 m=size(matrix,dim=1)
 do i=1,m-1
   vmin=minval((matrix(i+1:m,ipos)))
   do jmax=i+1,m
    if(matrix(jmax,ipos)== vmin ) exit
   enddo
   if(matrix(i,ipos) > matrix(jmax,ipos)) then
    temp=matrix(i,:)
    matrix(i,:)=matrix(jmax,:)
    matrix(jmax,:)=temp
   endif
 enddo
!-------------------------
 end subroutine sortmat2
!-------------------------

!---------------------------      
 Subroutine Randomloss
!---------------------------
      la=na*lp
      nc=0
      ge1(:)=iv51(:)
      ge2(:)=iv52(:)      
35    continue
      do i=1,na
       call random_number(temp) 
       if(temp<=lp*(ngen-iv7(i)+1))then 
       if(vc(i)==0) then
!-       if(nc>la) exit    !it leaves the loop
        ge1(i)=0
        ge2(i)=0	
        vc(i)=1
        nc=nc+1
       endif
       endif
       enddo
!-       if(nc<la) goto 35
           
!-------------------------------      
 end subroutine randomloss
!-------------------------------  

!##################################################################
end Program Pedigree


