#! /usr/bin/tcsh

# Translate the CELAR benchmark in wcsp format from the original FullRLFAP directory
# Remove variables which are not decision variables
# (see http://www.inra.fr/bia/T/schiex/Doc/CELARE.html#hints)

# Warning! gawk needs a very large amount of memory (more than 2 Giga bytes) for scen08 and graph13

# where is libcp.awk
setenv AWKPATH .

foreach f (../celar/FullRLFAP/*/*)
 if (${f:t} != CVS) then 
    if (-d $f) then
	echo "$f"
	awk -f celar2cp.awk $f/cst.txt $f/dom.txt $f/var.txt $f/ctr.txt >! ../celar/${f:t}.cp
	awk -f cp2wcsp.awk ../celar/${f:t}.cp >! ../celar/${f:t}.wcsp
    endif
 endif
end
