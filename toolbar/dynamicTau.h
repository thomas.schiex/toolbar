#ifndef DYNAMICTAU_H_
#define DYNAMICTAU_H_

#include "be.h"
#include "domainAbstractionBE.h"


void generateSimpleAbstractionData(dynamic_tau_data_t * dtauData, 
								   bucket *buckets, int maxTuples);

#endif /*DYNAMICTAU_H_*/
