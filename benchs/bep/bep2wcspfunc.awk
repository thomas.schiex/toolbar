
# convert a BEP instance into a weighted CSP enhanced with arithmetic cost functions

# usage: awk -f bep2wcsp.awk instances/bEpInstance_A_15_1 > problem_func.wcsp

NR == 3 {
	N = $1;
	maxd = 0;
	top = 1;
}

NR >= 4 && NR < N + 4 {
	duration[$1] = $4;
	if ($5 > 0) earliest[$1] = $5;
	else earliest[$1] = 0;
	latest[$1] = $6 - $4;
	d = latest[$1]+1;
	if (d > maxd) maxd = d;
	revenue[$1] = $7;
	top += revenue[$1] * (N-1);
}

NR >= N+4 {
	for (i=1;i<=N;i++) {
		delay[NR-N-3,i] = $i;
	}
}

END {
	print "bep",N+1,maxd,N+N*(N-1)/2,top;
	printf("-%d",latest[1]+2);
	for (i=2;i<=N;i++) {
		printf(" -%d",latest[i]+2);
	}
	print " -1";
	for (i=1;i<=N;i++) {
		print 2,i-1,N,-1,">=",earliest[i],0;
	}
	for (i=1;i<=N;i++) {
		for (j=i+1;j<=N;j++) {
			print 2,i-1,j-1,-1,"sdisj",duration[i]+delay[i,j],duration[j]+delay[j,i],latest[i]+1,latest[j]+1,revenue[i],revenue[j];
		}
	}
}
