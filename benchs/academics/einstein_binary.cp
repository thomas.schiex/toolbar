Einstein 1

#0
Suedois 1 2 3 4 5
Danois 1 2 3 4 5
Norvegien 1 2 3 4 5
Allemand 1 2 3 4 5
Anglais 1 2 3 4 5
hard(alldiff(Suedois,Danois,Norvegien,Allemand,Anglais))
#equivalent to: Suedois Danois Norvegien Allemand Anglais -1 salldiff var 1

#5
Blanche 1 2 3 4 5
Bleue 1 2 3 4 5
Verte 1 2 3 4 5
Rouge 1 2 3 4 5
Jaune 1 2 3 4 5
hard(alldiff(Blanche,Bleue,Verte,Rouge,Jaune))
#Blanche Bleue Verte Rouge Jaune -1 salldiff var 1

#10
Chien 1 2 3 4 5
Chat 1 2 3 4 5
Oiseaux 1 2 3 4 5
Poisson 1 2 3 4 5
Cheval 1 2 3 4 5
hard(alldiff(Chien,Chat,Oiseaux,Poisson,Cheval))
#Chien Chat Oiseaux Poisson Cheval -1 salldiff var 1

#15
Cafe 1 2 3 4 5
The 1 2 3 4 5
Eau 1 2 3 4 5
Lait 1 2 3 4 5
Biere 1 2 3 4 5
hard(alldiff(Cafe,The,Eau,Lait,Biere))
#Cafe The Eau Lait Biere -1 salldiff var 1

#20
PallMall 1 2 3 4 5
Dunhill 1 2 3 4 5
BlueMaster 1 2 3 4 5
Prince 1 2 3 4 5
Blend 1 2 3 4 5
hard(alldiff(PallMall,Dunhill,BlueMaster,Prince,Blend))
#PallMall Dunhill BlueMaster Prince Blend -1 salldiff var 1

hard(Anglais == Rouge)
hard(Suedois == Chien)
hard(Danois == The)
hard(Verte == Blanche - 1)
hard(Verte == Cafe)
hard(PallMall == Oiseaux)
hard(Jaune == Dunhill)
hard(Lait == 3)
hard(Norvegien == 1)
hard(abs(Blend-Chat) == 1)
hard(abs(Dunhill-Cheval) == 1)
hard(BlueMaster == Biere)
hard(Allemand == Prince)
hard(abs(Norvegien-Bleue)== 1)
hard(abs(Blend-Eau) == 1)

