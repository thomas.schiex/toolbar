BEGIN {
  nb = 0;
  sum = 0;
}

FNR == NR && FNR > 1 {
  nb++;
  w[nb] = $2;
  nbcf += ($1 + 1);
}

FNR != NR && FNR == 1 {
  nb = 0;
  nbvar = $1;
  nbclause = $2;
  printf("p cnf %d %d\n",nbvar+nbclause,nbcf);
}

FNR != NR && FNR > 1 {
  nb++;
  for (i=3; i<=NF; i++) {
    printf("%d ", $i);
  }
  printf("-%d 0\n", nb+nbvar);
  for (i=3; i<=NF; i++) {
    j = 0 - $i;
    printf("%d %d 0\n", j,nb+nbvar);
  }
}
