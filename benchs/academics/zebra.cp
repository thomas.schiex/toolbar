ZEBRA

Norvegian 1 2 3 4 5
Englishman 1 2 3 4 5
Jap 1 2 3 4 5
Spaniard 1 2 3 4 5
Ukranian 1 2 3 4 5

Kool 1 2 3 4 5
Parliament 1 2 3 4 5
Winston 1 2 3 4 5
Camel 1 2 3 4 5
Lucky 1 2 3 4 5

Fox 1 2 3 4 5
Snail 1 2 3 4 5
Horse 1 2 3 4 5
Dog 1 2 3 4 5
Zebra 1 2 3 4 5

Green 1 2 3 4 5
Blue 1 2 3 4 5
Yellow 1 2 3 4 5
Ivory 1 2 3 4 5
Red 1 2 3 4 5

OJ 1 2 3 4 5
Milk 1 2 3 4 5
Coffee 1 2 3 4 5
Water 1 2 3 4 5
Tea 1 2 3 4 5

hard( alldiff(Norvegian, Englishman, Jap, Spaniard, Ukranian) )
hard( alldiff(Kool, Parliament, Winston, Camel, Lucky) )
hard( alldiff(Fox, Snail, Horse, Dog, Zebra) )
hard( alldiff(Green, Blue, Yellow, Ivory, Red) )
hard( alldiff(OJ, Milk, Coffee, Water, Tea) )

hard( Englishman == Red )
hard( Spaniard == Dog )
hard( Coffee == Green )
hard( Ukranian == Tea )
hard( Winston == Snail )
hard( Kool == Yellow )

hard( Green == Ivory + 1 )
hard( Milk == 3 )
hard( Norvegian == 1 )
hard( abs(Camel - Fox) == 1 )
hard( Lucky == OJ )
hard( Jap == Parliament )
hard( abs(Kool - Horse) == 1 )
hard( abs(Norvegian - Blue) == 1 )

# solution:
#left[Yellow]: Norvegian owns a Fox, smokes Kool and drinks Water
#midleft[Blue]: Ukranian owns a Horse, smokes Camel and drinks Tea
#middle[Red]: Englishman owns a Snail, smokes Winston and drinks Milk
#midright[Ivory]: Spaniard owns a Dog, smokes Lucky and drinks OJ
#right[Green]: Jap owns a Zebra, smokes Parliament and drinks Coffee
