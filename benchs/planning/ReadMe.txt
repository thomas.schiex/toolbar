
Problems from planning in temporal and metric domains.

Informal  description of WCSP  encoding: each  fluent of  the planning
graph  is represented by  a variable  with domain  values representing
possible  actions  to produce  this  fluent.  Hard binary  constraints
represent mutual exclusions between  fluents and actions, and activity
edges (fluent-action-fluent link) in the graph. Soft unary constraints
represent  action  costs. The  goal  is to  find  a  valid plan  which
minimizes the sum of the action costs.

Logistics, Depots, Zenotravel, Driverlog, others come from
http://planning.cis.strath.ac.uk/competition/
and http://ipc.icaps-conference.org/

Available as WCSP instances from the courtesy of Sylvain Cussat-Blanc 
and Pierre R�gnier (IRIT, Toulouse, France).
