
# Translate in wcnf format from a JNH problem in sat format (with non-unit clause weights)

# Usage: awk -f jnh2wcnf.awk file.sat > file.wcnf

BEGIN {
  RS = "@";
}

{
  nbvar = $1;
  nbctr = $2;
  print "p wcnf",nbvar,nbctr;
  pos = 3;
  for (c=0; c<nbctr; c++) {
    arity = $pos;
    printf("%d", $(pos+1));
    for (i=1; i<=arity; i++) {
	printf(" %d", $(pos+i+1));
    }
    print " 0";
    pos += arity + 2;
  } 
}
