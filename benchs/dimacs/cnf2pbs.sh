
for e in *.cnf ; do 
  f=`basename $e .cnf`
  echo "$f.pbs ..."
  nawk -f cnf2pbs.awk $e > $f.pbs
  nawk -f cnf2pbseq.awk $e $e > $f.pbseq
  nawk -f cnf2pbspb.awk $e $f.lb > $f.pbs.pb
  ln -fs $f.pbs.pb $f.pbseq.pb
done
