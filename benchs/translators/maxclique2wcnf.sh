#! /bin/sh
awk '/^p /{print "c PSEUDOBOOLEAN",$3 + 1; print "p wcnf",$3,$3 + $3 * ($3 - 1) / 2 - $4; n=$3; for(i=1;i<=n;i++)print 1,i,0} /^e /{graph[0+$2 "_" 0+$3]=1;graph[0+$3 "_" 0+$2]=1} END{for(i=1;i<=n;i++){for(j=i+1;j<=n;j++){if (!((i "_" j) in graph)) {print n+1,-i,-j,0}}}}' $1 > $1.wcnf
