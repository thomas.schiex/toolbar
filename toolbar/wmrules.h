/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmrules.h

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

int NRES_AnB1(problem *p,restoreStruct *r);
void ternaryEAC(problem *p,restoreStruct *r);
