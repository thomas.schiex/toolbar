/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: ergoreader.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef ERGOREADER_H
#define ERGOREADER_H

double Cost2LogLike(cost_t c);
double Cost2Proba(cost_t c);

#endif
