# Langford problem generator in cp format

# (uncomment for soft version)

BEGIN {
  n = 3;
  m = 11;
#  top =  1;
  top =  n*m*n*m; # soft langford
  print "LANGFORD|" n "_" m "| " top;
  for (i=1;i<=m*n;i++) {
    printf("x%d", i);
    for (a=1;a<=m*n;a++) {
      printf(" %d", a);
    }
    print "";
  }
  for (i=1;i<=m*n;i++) {
    for (j=i+1;j<=m*n;j++) {
      print "hard(x" i " != x" j ")";
    }
  }
  for (i=1;i<=m;i++) {
    for (j=2;j<=n;j++) {
# classic langford problem with hard distance constraints
#       print "hard( x" i+(j-1)*m " - x" i+(j-2)*m " == " i+1 ")";
# soft langford problem with unit costs
#       print "hard( x" i+(j-2)*m " < x" i+(j-1)*m ")";
#       print "soft(1, x" i+(j-1)*m " - x" i+(j-2)*m " == " i+1 ")";
# soft langford problem with random costs
      print "hard( x" i+(j-2)*m " < x" i+(j-1)*m ")";
      print "soft(" int(rand()*(top-1))+1 ", x" i+(j-1)*m " - x" i+(j-2)*m " == " i+1 ")";
    }
  }
}
