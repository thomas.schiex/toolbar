FNR == 1 {
  nbvar = $1;
  nbclause = $2;
  cont = 0;
  clause = "";
  nb = 0;
  print $0;
}

FNR > 1 && cont > 0 {
  cont = cont - NF;
  clause = clause " " $0;
  if  (cont < 0) {
    print "#### ERROR cont = " cont;
  }
}

FNR > 1 && cont == 0 {
  if (clause == "") {
    if (NF - 2 == $1) {
      print $0;
      nb++;
    } else {
      cont = $1 - NF + 2;
      clause = "" $0;
    }
  } else {
    print clause;
    nb++;
    clause = "";
  }
}

END {
  if (nbclause != nb) print "#### ERROR nbclause(" nbclause ") != nb(" nb ")";
}
