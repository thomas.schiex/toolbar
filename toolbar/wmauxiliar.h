/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmauxiliar.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#define NO_VALUE -1
#define TRUE 1
#define FALSE 0
#define PREELIM 2
#define PREELIM2 3
#define MAX_CARS 1000

// CNF2WCNF
#define MAX_NAME 20
#define MAX_CAR_LINE 256


#define INS_MEM 0
#define LIST_ERROR 1
#define FILE_NOT_EXISTS 2
#define ERROR_READING_PROBLEM 3
#define FILE_CREATING_ERROR 4

#define MIN(X,Y) ((X) > (Y) ? (Y) : (X))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

int minV(long long int a,long long int b);
int maxV(long long int a,long long int b);
long long int myabs(long long int val);
void error(int v);
void start_timers();
unsigned long elapsed_time();
int negVal(int val);
