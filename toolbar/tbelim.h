#ifndef TBELIM_H
#define TBELIM_H

extern int small_degree(int *d,int *var, int *var2, int *var3);
extern void elim(int l, int i, int i2, int i3);
extern int elimAtRoot();

#endif
