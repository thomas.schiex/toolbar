/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbsystem.h
  $Id$

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
/* tbsystem.h
   contains codes which depends on the operating system */

//#define WINDOWS //uncomment if using windows
#define UNIX // uncomment if using Unix/Linux
//#define NO_OS // uncomment if only want to compile standard c, in this case
                // some options won't work, like time limit.

void timeout(int sig);
