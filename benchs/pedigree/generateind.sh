#!/bin/sh

ninst=50
num=0
aux=0

ngen=3
n=1
male=4



while (( $n < 60 )) ; do
    aux=`expr $n - $male`
    aux=`expr $aux % $male`
    num=0
    if [[ $aux == 0  ]] ; then
        zeros=""
	len=`expr length $n`
	if [[ $len == 1  ]] ; then
	    zeros="0"
	fi    
	base=ind${zeros}${n}
	echo $base
	\rm -rf $base
	\mkdir ${base}
	while (( $num < $ninst )) ; do
	    ./pedigreeSim $ngen $n $male &> /dev/null
	    if [[ $num > 0 ]] ; then
		res0=`diff ped.pre  ${base}/ped${num}.pre`
		res1=`cat ped.errors | wc -l`
		if [[ $res1 < 3 ]] ; then 
			res0=""
		fi 
	    else 
		res0="OK"
		res1="OK"
	    fi
	    if [[ $res0 != "" ]] ; then
		num=`expr $num + 1`
		\mv ped.pre ${base}/ped${num}.pre
		\mv ped.errors ${base}/ped${num}.errors	
	    fi
	done
    fi	
    n=`expr $n + 1`
done
