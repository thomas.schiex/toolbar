
# Translate in wcsp format from an original RLFAP instance
# Remove variables which are not decision variables
# (see http://www.inra.fr/bia/T/schiex/Doc/CELARE.html#hints)

# Usage: gawk -f celar2cp.awk ../celar/FullRLFAP/SUBCELAR6/CELAR6-SUB0/cst.txt ../celar/FullRLFAP/SUBCELAR6/CELAR6-SUB0/dom.txt ../celar/FullRLFAP/SUBCELAR6/CELAR6-SUB0/var.txt ../celar/FullRLFAP/SUBCELAR6/CELAR6-SUB0/ctr.txt

function perror(i,message) {
  print message;
  print "line " NR ": " $0;
  error = i;
  exit(i);
}

BEGIN {
    print "celar";
    first = 0;
}
    
match(FILENAME,"cst") && /a.* = / {
    sub("a","");
    cost[$1] = $3;
}
    
match(FILENAME,"cst") && /b.* = / {
    sub("b","");
    mobility[$1] = $3;
}

match(FILENAME,"dom") && NF == $2 + 2 {
    num = $1;
    $1 = "";
    $2 = "";
    dom[num] = $0;
}

match(FILENAME,"var") && NF >= 2 && NF <= 4 {
  if ((($1 % 2) == 0) && ($1 - 1 in vars)) {
    twin[$1] = $1 - 1;
    if ((NF == 4 && $4 == 0) || (NF == 3)) {
      print "hard( x" twin[$1] " == celar(" $3 ") )";
    } else {
      if (NF == 4) {
	print "soft(" mobility[$4] ", x" twin[$1] " == celar(" $3 ") )";
      }
    }
  } else {
    vars[$1] = 1;
    if ((NF == 4 && $4 == 0) || (NF == 3)) {
      print "x" $1,$3;
    } else {
      print "x" $1,dom[$2];
      if (NF == 4) {
	print "soft(" mobility[$4] ", x" $1 " == " $3 ")";
      }
    }
  }
}

match(FILENAME,"ctr") && (NF == 5 || NF == 6) && ($4 == "=") {
  if (($1 != twin[$2]) && ($2 != twin[$1])) {
    perror(1, "Error: celar trick does not work!");
  }
}

match(FILENAME,"ctr") && (NF == 5 || NF == 6) {
  if ($4 != "=") {
    if ($6 == 0) {
      print "hard( abs(" (($1 in twin)?("celar(x" twin[$1] ")"):"x" $1) " - " (($2 in twin)?("celar(x" twin[$2] ")"):"x" $2) ") " $4 " " $5 ")";
    } else {
      print "soft( " cost[$6] ", abs(" (($1 in twin)?("celar(x" twin[$1] ")"):"x" $1) " - " (($2 in twin)?("celar(x" twin[$2] ")"):"x" $2) ") " $4 " " $5 ")";
    }
  }
}
