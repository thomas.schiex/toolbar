/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmheurs.h
  $Id$

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
// PRECOMPUTED JEROSLOWS

#define MAX_TRIES 5

int emptyFunction1(problem *p,int i,int j);
int emptyFunction2(problem *p,int i);
int nopassBinaryJeroslowExecute(problem *p,int i,int j);



int passBinaryJeroslowExecute(problem *p,int i,int j);
int nopassJeroslowDeactivateExecute(problem *p,int i);
int passJeroslowDeactivateExecute(problem *p,int i);



cost_WM iniVarJeroslow(int i, problem *p);
void initializeJeroslow(problem *p);



extern func_t nopassBinaryJeroslow[7];
extern func_t passBinaryJeroslow[7];
extern func_t nopassJeroslowDeactivate[7];
extern func_t passJeroslowDeactivate[7];



// SELECTION VARIABLES HERISTICS

int WM_Jeroslow_like(problem *p);
int WM_sort_var_lex(problem *p);
extern func_t HeurVarFuncWM[7];


// SELECTION VALUES HEURISTICS

int WM_sort_val_lex(problem *p,restoreStruct *r,int var);
int WM_min_unary_cost(problem *p,restoreStruct *r,int var);
extern func_t HeurValFuncWM[2];


// LOCAL CONSISTENCY ENFORCE

void initRestoreStruct(problem * p, restoreStruct * r);
void initStack(problem * p, restoreStruct * r, stack *s);
void initTStack(problem *p,restoreStruct *r);
int pruneVar(problem *p, restoreStruct *r,int var);
int pruneVariables(problem *p, restoreStruct *r);
int checkNodeConsistency(problem *p);
int projectUnary(problem *p, restoreStruct * r,int i);
int executeNodeConsistency(problem *p, restoreStruct * r);
int binaryToUnaryAndNodeConsistency(problem *p,restoreStruct *r,int var,int val);
void restoreTernary(problem *p, restoreStruct * r);
int restoreBinaryToUnaryLimited(problem *p,restoreStruct *r,int var,int val);
void restoreNodeConsistency(problem *p, restoreStruct * r);
int existSupport(problem * p,int i,int j);
int existFullSupport(problem * p,int i,int j);
int executeAC3(problem * p, restoreStruct * r);
void restoreAC3(problem *p, restoreStruct * r);
int executeDAC(problem *p,restoreStruct * r);
int executeFDAC(problem *p,restoreStruct *r);
int findFullSupports(problem *p,restoreStruct * r,int i,int j);

extern func_t LcFuncWM[7];

int orP(int a,int b);
int findSupportsAC3(problem *p,restoreStruct * r, int i, int j);
int existentialSupport(problem *p,restoreStruct *r,int j,int *flag);
int execEAC(problem *p);
int considerVar(problem *p,int i);
int calculateEpsilon(problem *p,int j,int k,int val);
void autoSetCondAdd(problem *p,int i);


int checkAC3(problem * p, restoreStruct * r);
void showDAC(problem * p, int i, int j);
int checkDAC(problem *p);
int checkEAC(problem *p,restoreStruct *r);
int hyperBinaryResolution(problem *p,restoreStruct *r);

