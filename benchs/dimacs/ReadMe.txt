These  SAT instances  are  from the  Second  DIMACS Challenge  (1993).
Original files in DIMACS cnf format with a description can be found in
the SATLIB benchmark:

http://www.intellektik.informatik.tu-darmstadt.de/SATLIB/benchm.html

Most of  them are  unsatisfiable instances, and  so can be  tackled as
Max-SAT  instances. File names  with a  .lb extension  contain problem
lower bounds  found by  using Walksat (version  41) [Selman &  Kautz &
Cohen,  1996].   In  minimization,  upper  bounds can  be  deduced  by
substracting   the  number   of  clauses/constraints   by   the  lower
bound. Script  files cnf2opb.sh and  cnf2pbs.sh convert cnf  format to
OPBDP [Barth, 1995]  and PBS [Aloul et al,  2002] solver file formats.
Script file cnf2mps.sh convert cnf format to the common integer linear
programming format MPS.
