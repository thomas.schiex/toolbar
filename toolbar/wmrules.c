/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmrules.c

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "wmauxiliar.h"
#include "wmtypes.h"
#include "wmheurs.h"
#include "wmrules.h"


int existsTernary(problem *p,int i,int j,int k,int v1,int v2,int v3)
{
	int lit,cont;
	clause * c;

	ReferenceToClause * rc;

	ListD * lCla;

	
	if (v1==TRUE)
		{ lCla = p->variables[i].listLiterals; }
	else
		{ lCla = p->variables[i].listNoLiterals; }

	setFirstLD(lCla);

	while (!endListLD(lCla))
	{
		rc=getActualReference(lCla);

		c=&(p->clauses[rc->indexToClause]);

		cont=0;
		if (c->elim==FALSE && c->literalsWithoutAssign==3 && c->weight>BOTTOM)
		{
			for(lit=0;lit<c->literalsTotal;lit++)
			{
				if(p->Result[c->TLiterals[lit].idVar]==NO_VALUE && c->TLiterals[lit].idVar!=i && ( (c->TLiterals[lit].idVar==j && c->TLiterals[lit].boolValue==v2)|| (c->TLiterals[lit].idVar==k && c->TLiterals[lit].boolValue==v3) ))
				{
					cont++;
				}
			}
		}
		
		if(cont==2)
		{
			return rc->indexToClause;
		}
		
		getNextLD(lCla);
	}
	
	return -1;
}

int translateEAC1(problem *p,restoreStruct *r,int i,int j,int k,int v1,int v2,int v3,int ter)
{
	cost_WM minCost;
	int c;
	ReferenceToClause * rc;

	minCost=p->clauses[ter].weight;
	minCost=minV(minCost,p->UnaryCosts[i][negVal(v1)]);
	minCost=minV(minCost,p->UnaryCosts[k][negVal(v3)]);
	minCost=minV(minCost, p->BinaryCosts[i][j][v1][negVal(v2)]);

	if(p->clauses[ter].weight>minCost)
	{
		insertElement(&r->ternaryList,getNewNode2(&p->restoreList,ter,minCost));
		nopassJeroslow(p,ter);
		p->clauses[ter].weight-=minCost;
		passJeroslow(p,ter);
		
	}
	else
	{
		p->clauses[ter].elim = TRUE;
		insertElement(&r->satList,getNewNode1(&p->restoreList,ter));	
		// actualize incremental jeroslow
		nopassJeroslow(p,ter);

	}

	p->UnaryCosts[i][negVal(v1)]-=minCost;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,i,negVal(v1),minCost));
	
	p->UnaryCosts[k][negVal(v3)]-=minCost;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,k,negVal(v3),minCost));


	(*nopassBinaryJeroslow[HeurVar])(p,i,j);
	p->BinaryCosts[i][j][v1][negVal(v2)]=p->BinaryCosts[i][j][v1][negVal(v2)]-minCost;
	p->BinaryCosts[j][i][negVal(v2)][v1]=p->BinaryCosts[j][i][negVal(v2)][v1]-minCost;
	setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,v1,negVal(v2),minCost));
	(*passBinaryJeroslow[HeurVar])(p,i,j);
	

	(*nopassBinaryJeroslow[HeurVar])(p,i,k);
	p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]=p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]+minCost;
	p->BinaryCosts[k][i][negVal(v3)][negVal(v1)]=p->BinaryCosts[k][i][negVal(v3)][negVal(v1)]+minCost;
	setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,k,negVal(v1),negVal(v3),-minCost));
	(*passBinaryJeroslow[HeurVar])(p,i,k);
		
	
	// (A v �B v �C,minCost)
	p->totalClauses++;
	c=p->totalClauses-1;	
	p->clauses[c].weight=minCost;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=3;
	p->clauses[c].literalsTotal=3;
	
	p->clauses[c].TLiterals[0].idVar=i;
	p->clauses[c].TLiterals[0].boolValue=negVal(v1);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[0].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[1].idVar=j;
	p->clauses[c].TLiterals[1].boolValue=v2;
	rc=createReference(c);
	if (p->clauses[c].TLiterals[1].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[2].idVar=k;
	p->clauses[c].TLiterals[2].boolValue=v3;
	rc=createReference(c);
	if (p->clauses[c].TLiterals[2].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listLiterals,rc);
	passJeroslow(p,c);

	p->LB+=minCost;
	r->new3SAT++;
	return TRUE;
	
}

int translateEAC2(problem *p,restoreStruct *r,int i,int j,int k,int v1,int v2,int v3,int ter)
{
	cost_WM minCost;
	int c;
	ReferenceToClause * rc;

	minCost=p->clauses[ter].weight;
	minCost=minV(minCost,p->BinaryCosts[i][j][v1][v2]);
	minCost=minV(minCost, p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]);
	minCost=minV(minCost, p->UnaryCosts[k][v3]);
	
	if(p->clauses[ter].weight>minCost)
	{
		insertElement(&r->ternaryList,getNewNode2(&p->restoreList,ter,minCost));
		nopassJeroslow(p,ter);
		p->clauses[ter].weight-=minCost;
		passJeroslow(p,ter);
		
	}
	else
	{
		p->clauses[ter].elim = TRUE;
		insertElement(&r->satList,getNewNode1(&p->restoreList,ter));	
		// actualize incremental jeroslow
		nopassJeroslow(p,ter);

	}

	p->UnaryCosts[k][v3]-=minCost;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,k,v3,minCost));

	(*nopassBinaryJeroslow[HeurVar])(p,i,j);
	p->BinaryCosts[i][j][v1][v2]=p->BinaryCosts[i][j][v1][v2]-minCost;
	p->BinaryCosts[j][i][v2][v1]=p->BinaryCosts[j][i][v2][v1]-minCost;
	setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,v1,v2,minCost));
	(*passBinaryJeroslow[HeurVar])(p,i,j);
	

	(*nopassBinaryJeroslow[HeurVar])(p,i,k);
	p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]=p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]-minCost;
	p->BinaryCosts[k][i][negVal(v3)][negVal(v1)]=p->BinaryCosts[k][i][negVal(v3)][negVal(v1)]-minCost;
	setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,k,negVal(v1),negVal(v3),minCost));
	(*passBinaryJeroslow[HeurVar])(p,i,k);
			
	// (A v B v C,minCost)
	p->totalClauses++;
	c=p->totalClauses-1;
	p->clauses[c].weight=minCost;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=3;
	p->clauses[c].literalsTotal=3;
	
	p->clauses[c].TLiterals[0].idVar=i;
	p->clauses[c].TLiterals[0].boolValue=negVal(v1);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[0].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[1].idVar=j;
	p->clauses[c].TLiterals[1].boolValue=negVal(v2);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[1].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[2].idVar=k;
	p->clauses[c].TLiterals[2].boolValue=negVal(v3);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[2].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listLiterals,rc);
	passJeroslow(p,c);
	
	p->LB+=minCost;
	r->new3SAT++;
	return TRUE;
	
}

void ternaryEAC(problem *p,restoreStruct *r)
{
	int i,j,k,v1,v2,v3,cont,ter;

	cont=0;
	
	for(i=0;i<p->totalVariables;i++)
	{
	if(p->Result[i]==-1 && p->ArrayOfDecision[i].not_considered_values==2)
	{
	for(v1=0;v1<2;v1++)
	{

	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==-1 && p->ArrayOfDecision[j].not_considered_values==2 && j!=i)
	{
	for(v2=0;v2<2;v2++)
	{

	for(k=0;k<p->totalVariables;k++)
	{
	if(p->Result[k]==-1 && p->ArrayOfDecision[k].not_considered_values==2 && k!=i && k!=j)
	{
	for(v3=0;v3<2;v3++)
	{
		if(p->UnaryCosts[i][negVal(v1)]>BOTTOM && p->UnaryCosts[k][negVal(v3)]>BOTTOM && p->BinaryCosts[i][j][v1][negVal(v2)]>BOTTOM)
		{
			ter=existsTernary(p,i,j,k,negVal(v1),negVal(v2),negVal(v3));
			//if(ter!=-1 && p->UnaryCosts[i][negVal(v1)]>=p->clauses[ter].weight && p->UnaryCosts[k][negVal(v3)]>=p->clauses[ter].weight && p->BinaryCosts[i][j][v1][negVal(v2)]>=p->clauses[ter].weight)
			if(ter!=-1)
			{
				translateEAC1(p,r,i,j,k,v1,v2,v3,ter);
				cont++;
			}
		}

		if(p->BinaryCosts[i][j][v1][v2]>BOTTOM && p->UnaryCosts[k][v3]>BOTTOM && p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]>BOTTOM)
		{
			ter=existsTernary(p,i,j,k,negVal(v1),v2,v3);
			//if(ter!=-1 && p->BinaryCosts[i][j][v1][v2]>=p->clauses[ter].weight && p->UnaryCosts[k][v3]>=p->clauses[ter].weight && p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]>=p->clauses[ter].weight)
			if(ter!=-1)
			{
				translateEAC2(p,r,i,j,k,v1,v2,v3,ter);
				cont++;
			}

		}
		
	}
	}
	}
	
	
	
	}
	}
	}
	
	
	}
	}
	}
	
	//if(cont>0) printf("\n EAC TERNARY RULE %d TIMES APPLIED D:%d N:%d\n",cont,depth,NNodes);

}

void showClause(problem *p,restoreStruct *r,int c1)
{
	int lit1;
	printf("\n ");
	for(lit1=0;lit1<p->clauses[c1].literalsTotal;lit1++)
	{
		if(p->Result[p->clauses[c1].TLiterals[lit1].idVar]==NO_VALUE)
		{
		
			if(p->clauses[c1].TLiterals[lit1].boolValue==FALSE)
			{
				printf("-");
			}
			printf("%d ",p->clauses[c1].TLiterals[lit1].idVar);
		}
	}
	printf(" ,%d \n",p->clauses[c1].weight);

}


int canApplyNRES(problem *p,restoreStruct *r,int c1,int c2,int *clashing)
{
	int lit1,lit2,equals=0,equals2=0;
	*clashing=NO_VALUE;
	if(p->clauses[c1].literalsWithoutAssign==p->clauses[c2].literalsWithoutAssign)
	{
	for(lit1=0;lit1<p->clauses[c1].literalsTotal;lit1++)
	{
		if(p->Result[p->clauses[c1].TLiterals[lit1].idVar]==NO_VALUE)
		{
		
		for(lit2=0;lit2<p->clauses[c2].literalsTotal;lit2++)
		{
			if(p->Result[p->clauses[c2].TLiterals[lit2].idVar]==NO_VALUE)
			{
			
				if(p->clauses[c1].TLiterals[lit1].idVar==p->clauses[c2].TLiterals[lit2].idVar)
				{
				equals2++;
				}
				if(p->clauses[c1].TLiterals[lit1].idVar==p->clauses[c2].TLiterals[lit2].idVar&& *clashing==NO_VALUE && p->clauses[c1].TLiterals[lit1].boolValue!=p->clauses[c2].TLiterals[lit2].boolValue)
				{
					*clashing=p->clauses[c1].TLiterals[lit1].idVar;
					equals++;
				}
				else if(p->clauses[c1].TLiterals[lit1].idVar==p->clauses[c2].TLiterals[lit2].idVar&& p->clauses[c1].TLiterals[lit1].boolValue==p->clauses[c2].TLiterals[lit2].boolValue)
				{
					equals++;
				}
			}
		}

		
		}
	}
	
	
	if(p->clauses[c2].literalsWithoutAssign==equals && *clashing!=NO_VALUE) return TRUE;
	}
	return FALSE;
}


void createNewClauseA(problem *p,restoreStruct *r,int c1,cost_WM min)
{
	int c,lits,lit1;
	ReferenceToClause * rc;
	p->totalClauses++;
	if(p->totalClauses>p->maxClauses) printf("\n La Cagaste %d %d \n",p->totalClauses,p->maxClauses);
	c=p->totalClauses-1;
	p->clauses[c].weight=p->clauses[c1].weight-min;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=p->clauses[c1].literalsWithoutAssign;
	p->clauses[c].literalsTotal=p->clauses[c1].literalsWithoutAssign;

	lits=0;
	for(lit1=0;lit1<p->clauses[c1].literalsTotal;lit1++)
	{
		if(p->Result[p->clauses[c1].TLiterals[lit1].idVar]==NO_VALUE)
		{
			p->clauses[c].TLiterals[lits].idVar=p->clauses[c1].TLiterals[lit1].idVar;
			p->clauses[c].TLiterals[lits].boolValue=p->clauses[c1].TLiterals[lit1].boolValue;
			rc=createReference(c);
			if (p->clauses[c].TLiterals[lits].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[lits].idVar].listNoLiterals,rc);
			else insertReference(p->variables[p->clauses[c].TLiterals[lits].idVar].listLiterals,rc);
			
			lits++;
		}
	}
	
	passJeroslow(p,c);
	r->new3SAT+=1;
	//showClause(p,r,c);
}

void createNewClauseB(problem *p,restoreStruct *r,int c1,int c2,cost_WM min,int clashing)
{
	int c,lits,lit1;
	ReferenceToClause * rc;
	literal l1,l2;
	
	if(p->clauses[c1].literalsWithoutAssign>3)
	{
	p->totalClauses++;
	if(p->totalClauses>p->maxClauses) printf("\n La Cagaste %d %d \n",p->totalClauses,p->maxClauses);
	c=p->totalClauses-1;
	p->clauses[c].weight=min;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=p->clauses[c1].literalsWithoutAssign-1;
	p->clauses[c].literalsTotal=p->clauses[c1].literalsWithoutAssign-1;

	lits=0;
	for(lit1=0;lit1<p->clauses[c1].literalsTotal;lit1++)
	{
		if(p->Result[p->clauses[c1].TLiterals[lit1].idVar]==NO_VALUE && clashing!=p->clauses[c1].TLiterals[lit1].idVar)
		{
			p->clauses[c].TLiterals[lits].idVar=p->clauses[c1].TLiterals[lit1].idVar;
			p->clauses[c].TLiterals[lits].boolValue=p->clauses[c1].TLiterals[lit1].boolValue;
			rc=createReference(c);
			if (p->clauses[c].TLiterals[lits].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[lits].idVar].listNoLiterals,rc);
			else insertReference(p->variables[p->clauses[c].TLiterals[lits].idVar].listLiterals,rc);
			
			lits++;
		}
	}
	
	passJeroslow(p,c);
	r->new3SAT+=1;
	//showClause(p,r,c);
	
	}
	else
	{

		compClauses(p,c1,c2,clashing,&l1,&l2);
	
		if(l1.boolValue==FALSE) l1.boolValue=TRUE;
		else l1.boolValue=FALSE;
	
		if(l2.boolValue==FALSE) l2.boolValue=TRUE;
		else l2.boolValue=FALSE;
	
		(*nopassBinaryJeroslow[HeurVar])(p,l1.idVar,l2.idVar);
		
		if(p->BinaryCosts[l1.idVar][l2.idVar][l1.boolValue][l2.boolValue]==BOTTOM)
		{
			pushStack(p->R,l1.idVar);
			pushStack(p->R,l2.idVar);
			pushStack(p->Q,l1.idVar);
			pushStack(p->Q,l2.idVar);
		}
		
	
		p->BinaryCosts[l1.idVar][l2.idVar][l1.boolValue][l2.boolValue]+=min;
	
		p->BinaryCosts[l2.idVar][l1.idVar][l2.boolValue][l1.boolValue]+=min;
		
		insertElement(&r->ACList,getNewNode5(&p->restoreList,l1.idVar,l2.idVar,l1.boolValue,l2.boolValue,-min));
	
		(*passBinaryJeroslow[HeurVar])(p,l1.idVar,l2.idVar);
	}

}


int checkAllClauses(problem *p,restoreStruct *r,ListD * cla,int c1,int v)
{
	cost_WM min;
	int clashing=NO_VALUE,repCla;
	ReferenceToClause * rc;
	
	if(p->clauses[c1].elim==FALSE)
	{
	setFirstLD(cla);

	while(!endListLD(cla))
	{
		rc=getActualReference(cla);
		if(p->clauses[rc->indexToClause].elim==FALSE && rc->indexToClause!=c1)
		{
			
			if(canApplyNRES(p,r,c1,rc->indexToClause,&clashing)==TRUE)
			{
				min=minV(p->clauses[c1].weight,p->clauses[rc->indexToClause].weight);

				
				p->clauses[c1].elim = TRUE;
				insertElement(&r->satList,getNewNode1(&p->restoreList,c1));
				nopassJeroslow(p,c1);
				
				p->clauses[rc->indexToClause].elim = TRUE;
				insertElement(&r->satList,getNewNode1(&p->restoreList,rc->indexToClause));
				nopassJeroslow(p,rc->indexToClause);
				
				if(p->clauses[c1].weight>p->clauses[rc->indexToClause].weight) repCla=c1;
				else repCla=rc->indexToClause;
				
				
				if(p->clauses[c1].weight!=p->clauses[rc->indexToClause].weight)
				{
					createNewClauseA(p,r,repCla,min);
				}
				
				createNewClauseB(p,r,c1,rc->indexToClause,min,clashing);
				
				return TRUE;
			}
		}
		getNextLD(cla);
	}
	}
	return FALSE;
}

int NRES_AnB1(problem *p,restoreStruct *r)
{
	int c,coinc=0,lit1,lit2,stamp;
	
	do{
	stamp=coinc;
	for(c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE && p->clauses[c].literalsWithoutAssign>=3)
		{

			lit2=NO_VALUE;
			for(lit1=0;lit1<p->clauses[c].literalsTotal;lit1++)
			{
				if(p->Result[p->clauses[c].TLiterals[lit1].idVar]==NO_VALUE && p->ArrayOfDecision[p->clauses[c].TLiterals[lit1].idVar].not_considered_values==2)
				{
					lit2=p->clauses[c].TLiterals[lit1].idVar;
					lit1=p->clauses[c].literalsTotal;
				}
			}
			
			if(lit2!=NO_VALUE)
			{
				if(checkAllClauses(p,r,p->variables[lit2].listLiterals,c,lit2)==TRUE) coinc++;
				if(checkAllClauses(p,r,p->variables[lit2].listNoLiterals,c,lit2)==TRUE) coinc++;
			}
		}
	}
	}
	while(stamp!=coinc);
	
	
	if(coinc>0 && depth==0)
	{
		printf("c NRES_An_B1 applied %d times.\n",coinc);

		return TRUE;
	}
	else return FALSE;
}

