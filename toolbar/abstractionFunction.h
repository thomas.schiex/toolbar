#ifndef ABSTRACTIONFUNCTION_H_
#define ABSTRACTIONFUNCTION_H_

#include "domainAbstractionBE.h"

#define NUM_DIFFERENT_TAUS 4

typedef enum TauFunctionType_{
	tauTypeID,
	tauTypeModulo2,
	tauTypeModulo3,
	tauTypeDynamic
} TauFunctionType;


abstracted_constraint_t *tauID(nodofunc *c, dynamic_tau_data_t *dtauData);
abstracted_constraint_t *tauModulo2(nodofunc *c, dynamic_tau_data_t *dtauData);
abstracted_constraint_t *tauModulo3(nodofunc *c, dynamic_tau_data_t *dtauData);
abstracted_constraint_t *tauDynamic(nodofunc *c, dynamic_tau_data_t *dtauData);
abstracted_constraint_t * newAbstractedConstraint(short arity,
        short *variables, int * domsizes, value_tau_func_t *vtaus);

int vtauID(int v);

extern char *UserTauName[];
extern tau_func_t TauFunctions[];

#endif /*ABSTRACTIONFUNCTION_H_*/
