/*
 * Purpose: This module provides abstraction functions for the bucket 
 * elimination with domain abstraction algorithm.
 * 
 * author Paul Maier
 *
 */
#include <stdio.h>
#include <string.h>
#include "be.h"
#include "domainAbstractionBE.h"
#include "abstractionFunction.h"
#include "CuTest.h"

// the available abstraction functions
char *UserTauName[NUM_DIFFERENT_TAUS] = {"tauID", "tauModulo2", "tauModulo3", "tauDynamic"};
tau_func_t TauFunctions[NUM_DIFFERENT_TAUS] = {tauID, tauModulo2, tauModulo3, tauDynamic};

// from be.c: this function iterates over all possible assignments
extern bool_t siguiente_asignacion(short num_vars, short *variables,
        short *tupla);

void generateAbstractedCostMatrix(abstracted_constraint_t *ac, nodofunc *c);

int vtauID(int v) {
    return v;
}

int vtauModulo2(int v) {
    return v%2;
}

int vtauModulo3(int v) {
    return v%3;
}


value_tau_func_t *newVtauArray(value_tau_func_t vtau, int size) {
    value_tau_func_t * vtaus = mycalloc(size, sizeof(value_tau_func_t));
    int i = 0;
    for (i = 0; i < size; ++i) {
        vtaus[i] = vtau;
    }
    
    return vtaus;
}

/**
 * Recieves an array of value tau functions which are than applied to the passed
 * constraint c.
 */
abstracted_constraint_t *tauDynamic(nodofunc *c, dynamic_tau_data_t *dtauData) {
	abstracted_constraint_t *ac = newAbstractedConstraint(c->aridad, c->vars,
			dtauData->domsizes, dtauData->vtaus);

	generateAbstractedCostMatrix(ac, c);
	return ac;
}

/**
 * This abstraction function simply creates a new constraint function which is
 * the same as the passed one.
 */
abstracted_constraint_t *tauID(nodofunc *c, dynamic_tau_data_t *dtauData) {
    static int *domsizes = NULL;
    static value_tau_func_t *vtaus = NULL;
   
    if (domsizes == NULL && vtaus == NULL) {

        domsizes = mycalloc(NVar, sizeof(int));
        int i = 0;
        for (i = 0; i < NVar; ++i) {
            // for tauID the abstract domain sizes are the same as the 
            // concrete domain sizes
            domsizes[i] = DOMSIZE(i);  
        }

          vtaus = newVtauArray(vtauID, NVar);
    }
    
    abstracted_constraint_t *ac = newAbstractedConstraint(c->aridad, c->vars,
            domsizes, vtaus);
//    memcpy(ac->costs, c->costs, ac->ntuples*sizeof(cost_t));

    // the following doesn't make much sense in the identity function, but 
    // this way the overhead of abstraction can be measured
    generateAbstractedCostMatrix(ac, c);
    return ac;
}

abstracted_constraint_t *tauModulo2(nodofunc *c, dynamic_tau_data_t *dtauData) {
    static int *domsizes = NULL;
    static value_tau_func_t *vtaus = NULL;
    if (domsizes == NULL && vtaus == NULL) {

        domsizes = mycalloc(NVar, sizeof(int));
        int i = 0;
        for (i = 0; i < NVar; ++i) {
            // for tauID the abstract domain sizes are the same as the 
            // concrete domain sizes
            domsizes[i] = DOMSIZE(i) > 2 ? 2 : DOMSIZE(i);  
        }

          vtaus = newVtauArray(vtauModulo2, NVar);
    }
    
    abstracted_constraint_t *ac = newAbstractedConstraint(c->aridad, c->vars,
            domsizes, vtaus);
    
    generateAbstractedCostMatrix(ac, c);
    return ac;
}

abstracted_constraint_t *tauModulo3(nodofunc *c, dynamic_tau_data_t *dtauData) {
    static int *domsizes = NULL;
    static value_tau_func_t *vtaus = NULL;
    if (domsizes == NULL && vtaus == NULL) {

        domsizes = mycalloc(NVar, sizeof(int));
        int i = 0;
        for (i = 0; i < NVar; ++i) {
            // for tauID the abstract domain sizes are the same as the 
            // concrete domain sizes
            domsizes[i] = DOMSIZE(i) > 3 ? 3 : DOMSIZE(i);
        }

        vtaus = newVtauArray(vtauModulo3, NVar);
    }
    
    abstracted_constraint_t *ac = newAbstractedConstraint(c->aridad, c->vars,
            domsizes, vtaus);
    
    generateAbstractedCostMatrix(ac, c);
    return ac;
}

/**
 * Generate an abstracted cost matrix ac->costs from the given matrix c->costs.
 * 
 * PRE: ac->costs was initialized with Top.
 */
void generateAbstractedCostMatrix(abstracted_constraint_t *ac, nodofunc *c) {
    bool_t overflow = FALSE;
    full_assignment_t assignment = mycalloc(ac->aridad, sizeof(short));
    int pos = 0, abstractPos = 0;
    int i = 0;
    int val = 0;
    int av = 0;
    while (!overflow) {
        
        assignment[0] = 0;
        abstractPos = 0;
        // compute the start position within the abstracted cost matrix
        // leaving out the first variable
        for (i=1; i<ac->aridad; i++) {
            av = ac->vtaus[ac->vars[i]](assignment[ i ]);
            abstractPos=abstractPos+av*(ac->despl_var[i]);
        }
        
        for (val = 0; val < DOMSIZE(c->vars[0]); ++val){
            av = ac->vtaus[ac->vars[0]](val);
            // the cost array of ac was initialised with Top
            ac->costs[abstractPos + av] = 
                MIN(ac->costs[abstractPos + av], c->costs[pos]);
            pos++;
        }
        // set the first variable to the max. value, otherwise the advancing 
        // of the assignment won't work
        assignment[0] = DOMSIZE(ac->vars[0])-1;
        overflow = siguiente_asignacion(ac->aridad,ac->vars, assignment);
    }
    
    free(assignment);
}

abstracted_constraint_t * newAbstractedConstraint(short arity,
        short *variables, int * domsizes, value_tau_func_t *vtaus) {
    abstracted_constraint_t *ac;
    long int i;

    ac=mymalloc(1, sizeof(abstracted_constraint_t));
    ac->aridad=arity;
    ac->vars=mycalloc(arity, sizeof(short));
    if (arity > 0) {
        memcpy(ac->vars, variables, arity*sizeof(short));
    }
    ac->despl_var=mycalloc(arity, sizeof(int));

    if (arity>0) {
        ac->despl_var[0]=1;
        ac->ntuples=domsizes[ ac->vars[0] ];
    } else
        ac->ntuples=1; //The constant cost

    for (i=1; i<arity; i++) {
        ac->despl_var[i]=ac->despl_var[i-1]*domsizes[ ac->vars[i-1] ];
        ac->ntuples*=domsizes[ ac->vars[i] ];//num_bits;
    }

    ac->costs=mycalloc(ac->ntuples, sizeof(cost_t));
    for (i=0; i<ac->ntuples; i++) {
        ac->costs[i]=Top;
    }
    ac->sig_func= NULL;

    ac->vtaus = vtaus;
    ac->domsizes = domsizes;

    return ac;
}

void freeAbstractedConstraint(abstracted_constraint_t *ac) {
    if (ac->despl_var!=NULL) free (ac->despl_var);
    if (ac->costs!=NULL) free (ac->costs);
    if (ac->vars!=NULL) free (ac->vars);
    free (ac);
}

/*****************************************************************************
 * Test case functions.
 *****************************************************************************/
int testvTau(int val) {
    static int mapping[3] = {0,0,1};
    if (val >= 3) {
        printf("Wrong input for testvTau! val is %d\n", val);
        abort();
    }
    return mapping[val];
}

void TestGenerateAbstractedCostMatrix(CuTest *tc) {
    short vars[2] = {2,3};
    cost_t costs_c[6] = {1,2,5,3,1,3};
    cost_t costs_ac[4] = {Top, Top, Top, Top};
    int despl_var_c[2] = {1,3};
    int despl_var_ac[2] = {1,2};
    int domsizes[4] = {1,1,2,2};
    value_tau_func_t vtaus[4] = {NULL, NULL, testvTau, vtauID};
    
    nodofunc c = { .aridad = 2, .vars = NULL, .costs = NULL,
            .ntuples = 6, .despl_var = NULL, .sig_func = NULL };
    abstracted_constraint_t ac = {2, NULL, NULL, 4, NULL, 
            NULL, NULL, NULL};
    
    c.costs = costs_c;
    c.vars = vars;
    c.despl_var = despl_var_c;
    
    ac.costs = costs_ac;
    ac.vars = vars;
    ac.despl_var = despl_var_ac;
    ac.domsizes = domsizes;
    ac.vtaus = vtaus;
    
    
    int *gOldDomSizes = DomainSize;
    DomainSize = mycalloc(4, sizeof(int));
    DomainSize[0] = 1;
    DomainSize[1] = 1;
    DomainSize[2] = 3;
    DomainSize[3] = 2;
    
    generateAbstractedCostMatrix(&ac, &c);

    CuAssertIntEquals(tc, 1, ac.costs[0]);
    CuAssertIntEquals(tc, 5, ac.costs[1]);
    CuAssertIntEquals(tc, 1, ac.costs[2]);
    CuAssertIntEquals(tc, 3, ac.costs[3]);

    free(DomainSize);
    DomainSize = gOldDomSizes;
}
