#!/bin/sh

ninst=50
num=0
aux=0

ngen=4
n=12
male=4



while (( $male < $n )) ; do
    aux=`expr $n - $male`
    aux=`expr $aux % $male`
    num=0
    if [[ $aux == 0  ]] ; then
        zeros=""
	len=`expr length $male`
	if [[ $len == 1  ]] ; then
	    zeros="0"
	fi    
	basemale=male${zeros}${male}
	echo $basemale
	\rm -rf $basemale
	\mkdir ${basemale}
	while (( $num < $ninst )) ; do
	    ./pedigreeSim $ngen $n $male &> /dev/null
	    if [[ $num > 0 ]] ; then
		res0=`diff ped.pre  ${basemale}/ped${num}.pre`
		res1=`cat ped.errors | wc -l`
		if [[ $res1 < 3 ]] ; then 
			res0=""
		fi 
	    else 
		res0="OK"
		res1="OK"
	    fi
	    if [[ $res0 != "" ]] ; then
		num=`expr $num + 1`
		\mv ped.pre ${basemale}/ped${num}.pre
		\mv ped.errors ${basemale}/ped${num}.errors	
	    fi
	done
    fi	
    male=`expr $male + 1`
done
