Definition:
-----------
Given a set of 'n' goods presented in an auction, the bidders generate 'm' bids and each one is constituted by a price for a subset of goods. The Combinatorial Auction Problem consists in accepting a subset of  bids such that the benefits are maximized. It is a NP-Hard problem. Note that the same good can appear in different bids. Hence, only a bid containing each good can be accepted. 

Instances:
----------
We created combinatorial auction instances using the CATS (Combinatorial Auction Test Suite) free generator http://cats.stanford.edu/. It is able to generate problems with three different distributions: PATHS, SCHEDULE and REGIONS. Each one represents the particular distribution of a real life problem. For all the distributions, we generated instances with 60 goods and varying the number of bids.