

/* This is auto-generated code. Edit at your own peril. */

#include "CuTest.h"
#include "stdio.h"


extern void TestGenerateAbstractedCostMatrix(CuTest*);
extern void TestSolveDomainAbstractionBE(CuTest*);
extern void TestComputeBucketMsgSize(CuTest*);


void RunAllTests(void) 
{
    CuString *output = CuStringNew();
    CuSuite* suite = CuSuiteNew();


    SUITE_ADD_TEST(suite, TestGenerateAbstractedCostMatrix);
    SUITE_ADD_TEST(suite, TestSolveDomainAbstractionBE);
    SUITE_ADD_TEST(suite, TestComputeBucketMsgSize);

    CuSuiteRun(suite);
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);
}


