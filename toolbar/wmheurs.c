/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmheurs.c
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "wmauxiliar.h"
#include "wmtypes.h"
#include "wmheurs.h"

#define LC_CHECK 0 // set it to "1" if you wanna do checks to verify the consistency

int pruneTimes;

int itDijkstra;
int itDijkstra2;

extern void restoreState(problem * p, restoreStruct * r);

extern int update(problem * p,restoreStruct * r,int var,int val);

/* ARTIFICIAL INTELLIGENCE */

int nonBinaryClauses(problem *p,int i,int a)
{
	clause * c;
	ListD * relCla;
	ReferenceToClause * rc;
	
	if (a==TRUE)
	{

		relCla = p->variables[i].listLiterals;
	}
	else
	{
		relCla = p->variables[i].listNoLiterals;
	}

	setFirstLD(relCla);

	while (!endListLD(relCla))
	{

		rc=getActualReference(relCla);

		c=&(p->clauses[rc->indexToClause]);

		if (c->elim==FALSE)
		{
			return TRUE;
		}
		getNextLD(relCla);
	}
	return FALSE;
}

int numberBinaryClauses(problem *p,int i, int *v1, int *v2)
{
	int j,cont=0;
	
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE && p->isBinary[i][j] && p->ArrayOfDecision[j].not_considered_values==2 && i!=j)
	{
	
		cont++;
		//if(cont>2 && p->degreeB[i]<=2) printf("c FALLO %d %d\n",cont,p->degreeB[i]);
		if(cont>2) return NO_VALUE;
		else if(cont==1) *v1=j;
		else if(cont==2) *v2=j;
	
	}
	}
	//if(cont!=p->degreeB[i]) printf("c FALLO %d %d \n",cont,p->degreeB[i]);
	//if(cont==p->degreeB[i]) printf("c IGUAL %d %d",cont,p->degreeB[i]);
	return cont;
}

void eliminateBinaryClauses(problem *p, restoreStruct * r,int v1,int v2)
{
	int vaux;
	
	(*nopassBinaryJeroslow[HeurVar])(p,v1,v2);
	
	vaux=p->BinaryCosts[v1][v2][FALSE][FALSE];
	if(vaux>0)
	{
		p->BinaryCosts[v1][v2][FALSE][FALSE]-=vaux;
		p->BinaryCosts[v2][v1][FALSE][FALSE]-=vaux;
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1,v2,FALSE,FALSE,vaux));
	}
	
	vaux=p->BinaryCosts[v1][v2][FALSE][TRUE];
	if(vaux>0)
	{
		p->BinaryCosts[v1][v2][FALSE][TRUE]-=vaux;
		p->BinaryCosts[v2][v1][TRUE][FALSE]-=vaux;
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1,v2,FALSE,TRUE,vaux));
	}
	
	vaux=p->BinaryCosts[v1][v2][TRUE][FALSE];
	if(vaux>0)
	{
		p->BinaryCosts[v1][v2][TRUE][FALSE]-=vaux;
		p->BinaryCosts[v2][v1][FALSE][TRUE]-=vaux;
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1,v2,TRUE,FALSE,vaux));
	}
	
	vaux=p->BinaryCosts[v1][v2][TRUE][TRUE];
	if(vaux>0)
	{
		p->BinaryCosts[v1][v2][TRUE][TRUE]-=vaux;
		p->BinaryCosts[v2][v1][TRUE][TRUE]-=vaux;
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1,v2,TRUE,TRUE,vaux));
	}

	setIsBinaryPair(p,v1,v2,isBinaryPair(p,v1,v2));
	(*passBinaryJeroslow[HeurVar])(p,v1,v2);
}

int variableElimination(problem *p, restoreStruct * r)
{
	int i,c,v1,v2,a,b,d,min;
	
	for(i=0;i<p->totalVariables;i++)
	{
	if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2)
	{	
		if(nonBinaryClauses(p,i,FALSE)==FALSE && nonBinaryClauses(p,i,TRUE)==FALSE)
		{
		
			v1=NO_VALUE;v2=NO_VALUE;
			c=numberBinaryClauses(p,i,&v1,&v2);
			if(c!=NO_VALUE)
			{
				if(c==0) // Force some assignment, TRUE by default
				{
					if(p->UnaryCosts[i][FALSE]>0 && p->UnaryCosts[i][TRUE]>0)
						printf("c error\n");
					if(p->UnaryCosts[i][FALSE]==0) a=TRUE;
					else a=FALSE;
					insertElement(&r->pruneList,getNewNode2(&p->restoreList,i,a));
					p->ArrayOfDecision[i].usedValue[a]=TRUE;
					p->ArrayOfDecision[i].not_considered_values--;
					pushStack(p->Q,i);
					//if(unitClauseReduction(p,r,i,negVal(a))==FALSE) return FALSE;
					pruneTimes++;
				}
				else if(c==1) // Force some assignment, TRUE by default
				{
					findFullSupports(p,r,v1,i);
					eliminateBinaryClauses(p,r,i,v1);
					if(p->UnaryCosts[i][FALSE]==0) a=TRUE;
					else a=FALSE;
					insertElement(&r->pruneList,getNewNode2(&p->restoreList,i,a));
					p->ArrayOfDecision[i].usedValue[a]=TRUE;
					p->ArrayOfDecision[i].not_considered_values--;
					pushStack(p->Q,i);
					pruneTimes++;
				}
				else if(c==2) // Force some assignment, TRUE by default
				{				
					findFullSupports(p,r,v1,i);
					findFullSupports(p,r,v2,i);
					
					for(d=0;d<2;d++)
					{
					for(a=0;a<2;a++)
					{
					for(b=0;b<2;b++)
					{
					
					if(p->BinaryCosts[i][v1][d][a]>0 && p->BinaryCosts[i][v2][negVal(d)][b]>0)
					{
						min=minV(p->BinaryCosts[i][v1][d][a],p->BinaryCosts[i][v2][negVal(d)][b]);
					
						(*nopassBinaryJeroslow[HeurVar])(p,i,v1);
						p->BinaryCosts[i][v1][d][a]-=min;
						p->BinaryCosts[v1][i][a][d]-=min;
						insertElement(&r->ACList,getNewNode5(&p->restoreList,i,v1,d,a,min));
						setIsBinaryPair(p,i,v1,isBinaryPair(p,i,v1));
						(*passBinaryJeroslow[HeurVar])(p,i,v1);
						
						(*nopassBinaryJeroslow[HeurVar])(p,i,v2);
						p->BinaryCosts[i][v2][negVal(d)][b]-=min;
						p->BinaryCosts[v2][i][b][negVal(d)]-=min;
						insertElement(&r->ACList,getNewNode5(&p->restoreList,i,v2,negVal(d),b,min));
						setIsBinaryPair(p,i,v2,isBinaryPair(p,i,v2));
						(*passBinaryJeroslow[HeurVar])(p,i,v2);
						
						updateCurrent(p,v1,v2);
						
						(*nopassBinaryJeroslow[HeurVar])(p,v1,v2);
						p->BinaryCosts[v1][v2][a][b]+=min;
						p->BinaryCosts[v2][v1][b][a]+=min;
						insertElement(&r->ACList,getNewNode5(&p->restoreList,v1,v2,a,b,-min));
						setIsBinaryPair(p,v1,v2,isBinaryPair(p,v1,v2));
						(*passBinaryJeroslow[HeurVar])(p,v1,v2);
						
						changedCurrent(p,v1,v2);
						
					}
					
					}
					}
					}
					
					eliminateBinaryClauses(p,r,i,v1);
					eliminateBinaryClauses(p,r,i,v2);
					if(p->UnaryCosts[i][FALSE]==0) a=TRUE;
					else a=FALSE;
					insertElement(&r->pruneList,getNewNode2(&p->restoreList,i,a));
					p->ArrayOfDecision[i].usedValue[a]=TRUE;
					p->ArrayOfDecision[i].not_considered_values--;
					pushStack(p->Q,i);
					//if(unitClauseReduction(p,r,i,negVal(a))==FALSE) return FALSE;
					
					if(LcLevel==LC_AC)
					{
						findSupportsAC3(p,r,v2,v1);
						findSupportsAC3(p,r,v1,v2);
					}
					else
					{
					if(v1<v2)
					{
						findFullSupports(p,r,v1,v2);
						findSupportsAC3(p,r,v2,v1);
					}
					else
					{
						findFullSupports(p,r,v2,v1);
						findSupportsAC3(p,r,v1,v2);
					}
					}
					pruneTimes++;

				}

			}
		
		}
	
	
	}
	}
	
	return TRUE;

}


void hardBinaryResolution(problem * p, restoreStruct * r)
{
	int i,j,k,a,b,c,cuenta=0;
	for(i=0;i<p->totalVariables;i++)
	{
	if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2)
	{
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE && p->isBinary[i][j] && p->ArrayOfDecision[j].not_considered_values==2 && i!=j)
	{

	for(a=0;a<2;a++)
	{
	for(b=0;b<2;b++)
	{


		if((p->BinaryCosts[i][j][a][b]+p->LB)>=p->UB)
		{
		for(k=0;k<p->totalVariables;k++)
		{
		if(p->Result[k]==NO_VALUE && k!=i && k!=j && p->ArrayOfDecision[k].not_considered_values==2)
		{
			
			for(c=0;c<2;c++)
			{
				if((p->BinaryCosts[i][k][negVal(a)][c]+p->LB)>=p->UB && (p->BinaryCosts[j][k][b][c]+p->LB)<p->UB)
				{
					//updateCurrent(p,j,k);
					
					(*nopassBinaryJeroslow[HeurVar])(p,j,k);
					
					p->BinaryCosts[j][k][b][c]+=p->UB;
					p->BinaryCosts[k][j][c][b]+=p->UB;
					
					insertElement(&r->ACList,getNewNode5(&p->restoreList,j,k,b,c,-p->UB));
			
					setIsBinaryPair(p,j,k,isBinaryPair(p,j,k));
					
					(*passBinaryJeroslow[HeurVar])(p,j,k);
					
					pushStack(p->Q,j);
					pushStack(p->Q,k);
					pushStack(p->R,j);
					pushStack(p->R,k);
			
					cuenta++;
					//changedCurrent(p,j,k);
				
				}
				
			}
			
		
		}
		}
		}
	}
	}
		
	}
	}
	
	}
	}
	if(depth==0) printf("c Binary Resolution %d times\n",cuenta);
}



/*************************** HEURISTICS FOR VARIABLE SELECTION *********************************/
func_t HeurVarFuncWM[7];

/* PRECOMPUTED JEROSLOWS */

int emptyFunction1(problem *p,int i,int j)
{
	// Nothing to do
	// Used when Jeroslow is not used
	
	return TRUE;
}

int emptyFunction2(problem *p,int i)
{
	// Nothing to do
	// Used when Jeroslow is not used

	return TRUE;	
}

int nopassBinaryJeroslowExecute(problem *p,int i,int j)
{
	p->jeroslows[i]-= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
		
	p->jeroslows[j]-= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
      
	return TRUE;
}

int passBinaryJeroslowExecute(problem *p,int i,int j)
{	
	p->jeroslows[i]+= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
	
	p->jeroslows[j]+= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
	
	return TRUE;
}

int nopassJeroslowDeactivateExecute(problem *p,int i)
{
	int j;
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE)
	{
		p->jeroslows[j]-= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
	}
	}
	return TRUE;
}

int passJeroslowDeactivateExecute(problem *p,int i)
{	
	int j;
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE)
	{	
		p->jeroslows[j]+= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];

	}
	}
	return TRUE;
}



func_t nopassBinaryJeroslow[7] = {emptyFunction1,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute};

func_t passBinaryJeroslow[7] = {emptyFunction1,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute};

func_t nopassJeroslowDeactivate[7] = {emptyFunction2,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute};

func_t passJeroslowDeactivate[7] = {emptyFunction2,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute};

cost_WM iniVarJeroslow(int i, problem *p)
{
	// initialize variable 'i' with jeroslow values
	cost_WM res;
	ReferenceToClause * rc;
	clause * c;
	//NodeLD * lAux; // to have a second iterator.
	int j;
	res=0;

	// NON BINARY COSTS
	setFirstLD(p->variables[i].listLiterals);
	while (!endListLD(p->variables[i].listLiterals))
	{
		rc=getActualReference(p->variables[i].listLiterals);
		c=&(p->clauses[rc->indexToClause]);
		if(c->elim==FALSE && c->literalsWithoutAssign>2) // Only consider this clause if it is not eliminate previously (more than two literals)
			res+= (c->weight ) * p->pows[p->maximumArity-c->literalsWithoutAssign];
		getNextLD(p->variables[i].listLiterals);
	}

	setFirstLD(p->variables[i].listNoLiterals);
	while (!endListLD(p->variables[i].listNoLiterals))
	{
		rc=getActualReference(p->variables[i].listNoLiterals);
		c=&(p->clauses[rc->indexToClause]);
		if(c->elim==FALSE && c->literalsWithoutAssign>2) // Only consider this clause if it is not eliminate previously (more than two literals)
			res+= (c->weight ) * p->pows[p->maximumArity-c->literalsWithoutAssign];
		getNextLD(p->variables[i].listNoLiterals);
	}

	// BINARY COSTS
	
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE)
	{
		res+= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
	
	}
	}

	return res;	
}


void initializeJeroslow(problem *p)
{
	// Two sided-Jeroslow Heuristic to choice the next variable to consider	
	// Before entering the branch and bound, we must initilize the array where we will
	// compute incremental jeroslow

	int n_v;
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		p->jeroslows[n_v]+= iniVarJeroslow(n_v,p);
	}
	}
}

int WM_Jeroslow_like(problem *p)
{
	int n_v,max;
	cost_WM jAct,j;
	// returns the first variable with a domain of only one value
	// else the variable with higher jeroslow       

	j=0;
	max=0;
	
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		if (p->ArrayOfDecision[n_v].not_considered_values==1) return (n_v);
		
		jAct=p->jeroslows[n_v]+(p->UnaryCosts[n_v][FALSE]+ p->UnaryCosts[n_v][TRUE]) * p->pows[p->maximumArity-1];
		
		if (jAct>=j)
		{	
			j=jAct;
			max=n_v;
		}
	
	}
	}
	
	return max;
}


int WM_max_degree(problem *p)
{
	int jAct,n_v,k,max,v_max;
	// returns the first variable with a domain of only one value
	// else the variable with higher binary degree

	max=0;
	v_max=0;
	
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		if (p->ArrayOfDecision[n_v].not_considered_values==1) return (n_v);
		
		jAct=0;
		
		for(k=0;k<p->totalVariables;k++)
		{
		if(p->Result[k]==NO_VALUE && n_v!=k)
		{
			if(isBinaryPair(p,n_v,k)==TRUE)
			{
				jAct++;
			}
		}
		}
		
		if (jAct>=max)
		{	
			max=jAct;
			v_max=n_v;
		}
	
	}
	}
	
	//printf("\n Depth %d, var X%d Connectivity %d",depth, v_max,max);
	return v_max;
}



int WM_sort_var_lex(problem *p)
{
	// Returns the minimum variable of the list of not assigned variables.
	// Example: 23,34,56,7,12,2,12,11 -> It will select "2".
	
	int jAct,j;
	
	j=p->totalVariables;
	for(jAct=0;jAct<p->totalVariables;jAct++)
	{
	if(p->Result[jAct]==NO_VALUE)
	{
		if (p->ArrayOfDecision[jAct].not_considered_values==1) return (jAct);
		if (jAct<=j)
		{
			j=jAct;
		}
	}
	}
	return j;

}

func_t HeurVarFuncWM[] = {WM_sort_var_lex,WM_Jeroslow_like,WM_Jeroslow_like,WM_Jeroslow_like,WM_max_degree,WM_Jeroslow_like,WM_Jeroslow_like};

/*************************** HEURISTICS FOR VALUE SELECTION *********************************/

int WM_sort_val_lex(problem *p,restoreStruct *r,int var)
{
	// Returns the first value to consider. In this case
	// always false is the first, and True the second.
	
	return FALSE;
}

int WM_min_unary_cost(problem *p,restoreStruct *r,int var)
{
	// Returns the first value to consider. It will be the value with minimum unary cost.
	
	if(p->UnaryCosts[var][FALSE]<=p->UnaryCosts[var][TRUE]) return FALSE;
	return TRUE;
}

func_t HeurValFuncWM[2] = {WM_sort_val_lex,WM_min_unary_cost};

/*************************** LOCAL CONSISTENCY HEURISTICS *********************************/

/* FUNCTIONS TO MANTAIN NODE CONSISTENCY */

extern void WM_BB(problem *p,int b);

int unitClauseReduction(problem * p,restoreStruct * r,int var,int val)
{
	if(p->ArrayOfDecision[var].not_considered_values==1)
	{
		WM_BB(p,FALSE);
		return FALSE;
	}
	
	return TRUE;
}

int pruneVarUCR(problem *p, restoreStruct *r,int var)
{
	// Prune values from a variable. Returns FALSE if all values are pruned (do backtracking at the BB).
	// FALSE
	
	if(p->ArrayOfDecision[var].usedValue[FALSE]==FALSE)
	{
		if ((p->UnaryCosts[var][FALSE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,FALSE));
			p->ArrayOfDecision[var].usedValue[FALSE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			if(unitClauseReduction(p,r,var,TRUE)==FALSE) return FALSE;
			pruneTimes++;
		}
	}

	// TRUE
	if(p->ArrayOfDecision[var].usedValue[TRUE]==FALSE)
	{
		if ((p->UnaryCosts[var][TRUE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,TRUE));
			p->ArrayOfDecision[var].usedValue[TRUE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			if(unitClauseReduction(p,r,var,FALSE)==FALSE) return FALSE;
			pruneTimes++;
		}
	}

	if(p->ArrayOfDecision[var].not_considered_values==0)
	{
		return FALSE;
	}

	return TRUE;
}


int pruneVar(problem *p, restoreStruct *r,int var)
{
	// Prune values from a variable. Returns FALSE if all values are pruned (do backtracking at the BB).
	// FALSE
	
	if(p->ArrayOfDecision[var].usedValue[FALSE]==FALSE)
	{
		if ((p->UnaryCosts[var][FALSE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,FALSE));
			p->ArrayOfDecision[var].usedValue[FALSE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			pruneTimes++;
		}
	}

	// TRUE
	if(p->ArrayOfDecision[var].usedValue[TRUE]==FALSE)
	{
		if ((p->UnaryCosts[var][TRUE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,TRUE));
			p->ArrayOfDecision[var].usedValue[TRUE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			pruneTimes++;
		}
	}

	if(p->ArrayOfDecision[var].not_considered_values==0)
	{
		return FALSE;
	}

	return TRUE;
}

int pruneVariables(problem *p, restoreStruct *r)
{
	int n_v;
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		if (pruneVarUCR(p,r,n_v)==FALSE) return FALSE;
		//if (pruneVar(p,r,n_v)==FALSE) return FALSE;
	}
	}
	
	return TRUE;
}


void initRestoreStruct(problem * p, restoreStruct * r)
{
	createListRAux(&r->pruneList);
	createListRAux(&r->NCList);
	createListRAux(&r->ACList);
	createListRAux(&r->ternaryList);
	
	createListRAux(&r->satList);
	
	r->currentAssignments=0;
	r->actualLB=p->LB;
	r->new3SAT=0;

}

void initStack(problem * p, restoreStruct * r,stack *s)
{
	int n_v;
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		pushStack(s,n_v);
	}
	}
}

void initTStack(problem *p,restoreStruct *r)
{
	int i=0,j=0;
	
	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==NO_VALUE)
		{
			for(j=i+1;j<p->totalVariables;j++)
			{
				if(p->Result[j]==NO_VALUE)
				{
	
					if(p->BinaryCosts[i][j][FALSE][FALSE]>BOTTOM) insertChange(p,i,j,FALSE,FALSE);
					if(p->BinaryCosts[i][j][FALSE][TRUE]>BOTTOM) insertChange(p,i,j,FALSE,TRUE);
					if(p->BinaryCosts[i][j][TRUE][FALSE]>BOTTOM) insertChange(p,i,j,TRUE,FALSE);
					if(p->BinaryCosts[i][j][TRUE][TRUE]>BOTTOM) insertChange(p,i,j,TRUE,TRUE);

				}
			}
		}
	}
}

int checkNC(problem * p, int i)
{
	int tot=0;
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if((p->LB+p->UnaryCosts[i][FALSE])>=p->UB)	
		{
			printf("\nNode inconsistent for X%d=FALSE",i);
			return FALSE;
		}
		if(p->UnaryCosts[i][FALSE]==BOTTOM) tot++;	
	}
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if((p->LB+p->UnaryCosts[i][TRUE])>=p->UB)	
		{
			printf("\nNode inconsistent for X%d=TRUE",i);
			return FALSE;
		}
		if(p->UnaryCosts[i][TRUE]==BOTTOM) tot++;
	}

	if(tot>0)
		return TRUE;	
	else
	{
		printf("\nNode inconsistent for X%d",i);
		return FALSE;
	}
}



int checkNodeConsistency(problem * p)
{
	int n_v;
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{

		if(!checkNC(p,n_v)) return FALSE;
	}
	}
	return TRUE;
}

int projectUnary(problem *p, restoreStruct * r,int i)
{	
	// pass costs from Ci to Lower bound
	cost_WM min;

	if(p->UnaryCosts[i][p->SupU[i]]>BOTTOM)
	{
		if(p->ArrayOfDecision[i].not_considered_values == 2)
		{
			min=minV(p->UnaryCosts[i][FALSE],p->UnaryCosts[i][TRUE]);
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][FALSE]-= min;
				p->UnaryCosts[i][TRUE]-= min;
				
				if((p->UB-p->LB)==1)
				{
					p->stat.depth_back+=depth;
					p->stat.num_back++;
				}

				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,min));	
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,min));
				if(p->UnaryCosts[i][FALSE]==BOTTOM) p->SupU[i]=FALSE;
				else p->SupU[i]=TRUE;
				if((p->UB-p->LB)==1)
				{
					p->stat.num_diff1++;
					p->stat.depth_diff1+=depth;
					//if(LcLevel>LC_AC) LcLevel=LC_AC;
				}
				return TRUE;
			}
			
		}	
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
		{
			min=p->UnaryCosts[i][FALSE];
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][FALSE]-= min;
				if((p->UB-p->LB)==1)
				{
					p->stat.depth_back+=depth;
					p->stat.num_back++;
				}

				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,min));
				p->SupU[i]=FALSE;
				if((p->UB-p->LB)==1)
				{
					p->stat.num_diff1++;
					p->stat.depth_diff1+=depth;
					//if(LcLevel>LC_AC) LcLevel=LC_AC;
				}
				return TRUE;
			}
		}

		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)	
		{
			
			min=p->UnaryCosts[i][TRUE];
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][TRUE]-= min;
				if((p->UB-p->LB)==1)
				{
					p->stat.depth_back+=depth;
					p->stat.num_back++;
				}				
				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,min));
				p->SupU[i]=TRUE;
				if((p->UB-p->LB)==1)
				{
					p->stat.num_diff1++;
					p->stat.depth_diff1+=depth;
					//if(LcLevel>LC_AC) LcLevel=LC_AC;
				}

				return TRUE;
			}
		}
	}

	return FALSE;	
}



int binaryToUnaryAndNodeConsistency(problem *p,restoreStruct *r,int var,int val)
{
	int n_v;
	// Create all the lists for restoration of local consistency
	
	// FEDE
	
	r->NClimit=NO_VALUE;
	(*nopassJeroslowDeactivate[HeurVar])(p,var);

	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		if(p->isBinary[var][n_v]) p->degreeB[n_v]--;

		if(p->UnaryCosts[n_v][FALSE]==BOTTOM && (p->UnaryCosts[n_v][FALSE]+p->BinaryCosts[var][n_v][val][FALSE])>BOTTOM)
		{
			pushStack(p->R,n_v);
		}

		if(p->UnaryCosts[n_v][TRUE]==BOTTOM && (p->UnaryCosts[n_v][TRUE]+p->BinaryCosts[var][n_v][val][TRUE])>BOTTOM)
		{
			pushStack(p->R,n_v);
		}

		// We pass binary costs to unary
		p->UnaryCosts[n_v][FALSE]+=p->BinaryCosts[var][n_v][val][FALSE];
		p->UnaryCosts[n_v][TRUE]+=p->BinaryCosts[var][n_v][val][TRUE];
		projectUnary(p,r,n_v);

	}
	}
	return TRUE;
}



int restoreBinaryToUnaryLimited(problem *p,restoreStruct *r,int var,int val)
{
	int end=FALSE;
	
	int n_v;
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		p->UnaryCosts[n_v][FALSE]-=p->BinaryCosts[var][n_v][val][FALSE];
		p->UnaryCosts[n_v][TRUE]-=p->BinaryCosts[var][n_v][val][TRUE];
		if(r->NClimit==n_v) end=TRUE;
	}
	}

	(*passJeroslowDeactivate[HeurVar])(p,var);
	
	return TRUE;	
}

int executeNodeConsistency(problem *p, restoreStruct * r)
{
	// NC: Node Consistency
	int n_v;

	// Create all the lists for restoration of local consistency
	createListRAux(&r->pruneList);
	createListRAux(&r->NCList);
	createListRAux(&r->ACList);
	createListRAux(&r->ternaryList);
       
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		projectUnary(p,r,n_v);
		if(p->LB>=p->UB) return FALSE;
	}
	}
	
#if (LC_CHECK==1) 
	checkNodeConsistency(p);
#endif
	return TRUE;
}



void restoreNodeConsistency(problem *p, restoreStruct * r)
{
	NodeR *n;
	setFirstLR(&r->NCList);

	while(!endListLR(&r->NCList))	
	{
		n=extractActualR(&r->NCList);
		p->UnaryCosts[(int)n->val1][(int)n->val2]+= n->val3;
		insertElement(&p->restoreList,n);
	}
}


/* FUNCTIONS TO MANTAIN AC CONSISTENCY (AC3)*/

int orP(int a,int b)
{
	
	if((a==TRUE) || (b==TRUE)) return TRUE;
	
	else return FALSE;
	
}

int findSupportsAC3(problem *p,restoreStruct * r, int i, int j)
{

	// Find supports: Cij binary -> Ci unary + Project Ci
	cost_WM min;
	int flag=FALSE;
	
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][FALSE][FALSE],p->BinaryCosts[i][j][FALSE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-=min;
				p->BinaryCosts[i][j][FALSE][FALSE]-=min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));
				
				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);

				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
	}

	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][TRUE][FALSE],p->BinaryCosts[i][j][TRUE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE  && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));

				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
	}

	return orP(flag,projectUnary(p,r,i));
}

int findSupportsAC3Full(problem *p,restoreStruct * r, int i, int j)
{

	// Find supports: Cij binary -> Ci unary + Project Ci
	cost_WM min;
	int flag=FALSE;
	
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][FALSE][FALSE],p->BinaryCosts[i][j][FALSE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-=min;
				p->BinaryCosts[i][j][FALSE][FALSE]-=min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));
				
				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);

				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
	}

	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][TRUE][FALSE],p->BinaryCosts[i][j][TRUE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE  && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));

				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
	}
	
	changedCurrent(p,i,j);

	return orP(flag,projectUnary(p,r,i));
}


int existSupport(problem * p,int i,int j)
{
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{	
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][FALSE][FALSE]!=0 && p->BinaryCosts[i][j][FALSE][TRUE]!=0)
				
				return FALSE;
		}
		
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][FALSE][FALSE]!=0) return FALSE;
		}
			
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
				
			if(p->BinaryCosts[i][j][FALSE][TRUE]!=0) return FALSE;
		}		
	}
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][TRUE][FALSE]!=0 && p->BinaryCosts[i][j][TRUE][TRUE]!=0)
				
			  return FALSE;
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][TRUE][FALSE]!=0) return FALSE;
                }
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{

			if(p->BinaryCosts[i][j][TRUE][TRUE]!=0) return FALSE;
		}	
	}
	
	return TRUE;
}

void showAC3(problem * p, int i, int j)
{
	
	printf("\ni=FALSE: %d",p->ArrayOfDecision[i].usedValue[FALSE]);
	
	printf("\ni=TRUE: %d",p->ArrayOfDecision[i].usedValue[TRUE]);
	
	printf("\nj=FALSE: %d",p->ArrayOfDecision[j].usedValue[FALSE]);
	
	printf("\nj=TRUE: %d",p->ArrayOfDecision[j].usedValue[TRUE]);
	
	printf("\ni=FALSE -> j =FALSE :%lld",p->BinaryCosts[i][j][FALSE][FALSE]);
	
	printf("\ni=FALSE -> j =TRUE :%lld",p->BinaryCosts[i][j][FALSE][TRUE]);
	
	printf("\ni=TRUE -> j =FALSE :%lld",p->BinaryCosts[i][j][TRUE][FALSE]);
	
	printf("\ni=TRUE -> j =TRUE :%lld",p->BinaryCosts[i][j][TRUE][TRUE]);	
}



int checkAC3(problem * p, restoreStruct * r)
{
	int i,j;
	for(j=0;j<p->totalVariables;j++)
	{
	if(p->Result[j]==NO_VALUE)
	{
		if(!checkNC(p,j)) return FALSE;
		for(i=0;i<p->totalVariables;i++)
		{
		if(p->Result[i]==NO_VALUE)
		{
			if (existSupport(p,i,j)==FALSE)
			{
				printf("\nNot exists support between %d and %d",i,j);
				showAC3(p,i,j);
			}

		}
		}
	}
	}

	return TRUE;
}

int executeAC3(problem * p, restoreStruct * r)
{
	// AC: Arc consistency computed with the well-known AC3 algorithm
	int flag;
	int i,j;
	
	while(p->Q->n_elem>0)
	{

		flag=FALSE;
		
		j=popStack(p->Q);
		
		if(p->Result[j]==NO_VALUE)
		{
			for(i=0;i<p->totalVariables;i++)
			{
				if(p->Result[i]==NO_VALUE && p->isBinary[i][j])
				{
					flag = orP(flag,findSupportsAC3(p,r,i,j));
					if(p->LB>=p->UB) return FALSE;
				}
			}
			
			if(flag)
			{
				// Prune new values with the new low-bound
				if (pruneVariables(p,r)==FALSE) return FALSE;
			} // flag = TRUE
		}
	} // end while Q

	


#if (LC_CHECK == 1)

	if (LcLevel==LC_AC)
		
		checkAC3(p,r);
	
#endif
	return TRUE;
	
}

void restoreAC3(problem *p, restoreStruct * r)
{
	NodeR *n;

	setFirstLR(&r->ACList);
	while(!endListLR(&r->ACList))
	{
		n=extractActualR(&r->ACList);

		(*nopassBinaryJeroslow[HeurVar])(p,(int)n->val1,(int)n->val2);
		p->BinaryCosts[(int)n->val1][(int)n->val2][(int)n->val3][(int)n->val4]+= n->val5;
		p->BinaryCosts[(int)n->val2][(int)n->val1][(int)n->val4][(int)n->val3]+= n->val5;
		setIsBinaryPair(p,n->val1,n->val2,isBinaryPair(p,(int)n->val1,(int)n->val2));
		(*passBinaryJeroslow[HeurVar])(p,(int)n->val1,(int)n->val2);

		insertElement(&p->restoreList,n);
	}
}

void restoreTernary(problem *p, restoreStruct * r)
{

	// restore satisfied ternary clauses (or clauses with arity >=3).
	NodeR *n;

	setFirstLR(&r->ternaryList);
	while(!endListLR(&r->ternaryList))
	{
		n=extractActualR(&r->ternaryList);
		if(p->clauses[n->val1].elim==FALSE) nopassJeroslow(p,n->val1);
		p->clauses[n->val1].weight+=n->val2;
		insertElement(&p->restoreList,n);
	}
}



/* FUNCTIONS TO MANTAIN DAC CONSISTENCY */

void calculateDACValues(problem *p,int i,int j,cost_WM *eT,cost_WM *eF)
{
	cost_WM pT=0,pF=0;

	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pT=minV((p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE]),(p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE]));
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
			pT=p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE];
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pT=(p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE]);
	}
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pF=minV((p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE]),(p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE]));
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
			pF=p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE];
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pF=(p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE]);
	}
	if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE && p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eT=maxV(pT-p->BinaryCosts[i][j][TRUE][TRUE],pF-p->BinaryCosts[i][j][FALSE][TRUE]);
		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
			*eT=pT-p->BinaryCosts[i][j][TRUE][TRUE];
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eT=pF-p->BinaryCosts[i][j][FALSE][TRUE];
	}
	else *eT=0;
	if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE && p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eF=maxV(pT-p->BinaryCosts[i][j][TRUE][FALSE],pF-p->BinaryCosts[i][j][FALSE][FALSE]);
		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
			*eF=pT-p->BinaryCosts[i][j][TRUE][FALSE];
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eF=pF-p->BinaryCosts[i][j][FALSE][FALSE];
	}
	else *eF=0;
	
}

void executeExtend(problem *p,restoreStruct * r,int i,int j)
{
	// Find extensions: Cj unary -> Cij binary
	cost_WM k1=0,k2=0;

	if(p->UnaryCosts[j][FALSE]!=0 || p->UnaryCosts[j][TRUE]!=0)
	{
		updateCurrent(p,i,j);
		calculateDACValues(p,i,j,&k2,&k1);

		if(k1>0)
		{
			p->UnaryCosts[j][FALSE]=p->UnaryCosts[j][FALSE]-k1;
			insertElement(&r->NCList,getNewNode3(&p->restoreList,j,FALSE,k1));
			(*nopassBinaryJeroslow[HeurVar])(p,i,j);
			p->BinaryCosts[i][j][FALSE][FALSE]+= k1;
			p->BinaryCosts[i][j][TRUE][FALSE]+= k1;
			p->BinaryCosts[j][i][FALSE][FALSE]+= k1;
			p->BinaryCosts[j][i][FALSE][TRUE]+= k1;
			(*passBinaryJeroslow[HeurVar])(p,i,j);

			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,-k1));
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,-k1));
		}
	
		if(k2>0)			
		{
			p->UnaryCosts[j][TRUE]=p->UnaryCosts[j][TRUE]-k2;
			insertElement(&r->NCList,getNewNode3(&p->restoreList,j,TRUE,k2));

			(*nopassBinaryJeroslow[HeurVar])(p,i,j);
			p->BinaryCosts[i][j][FALSE][TRUE]+= k2;
			p->BinaryCosts[i][j][TRUE][TRUE]+= k2;
			p->BinaryCosts[j][i][TRUE][FALSE]+= k2;
			p->BinaryCosts[j][i][TRUE][TRUE]+= k2;
			(*passBinaryJeroslow[HeurVar])(p,i,j);
			
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,-k2));
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,-k2));
		}
	}	
}


int findFullSupports(problem *p,restoreStruct * r,int i,int j)
{
	// Find extensions: Cj unary -> Cij binary
	executeExtend(p,r,i,j);
	
	// Find supports: Cij binary -> Ci unary + Project Ci
	return findSupportsAC3Full(p,r,i,j);
}



void showDAC(problem * p, int i, int j)
{
	
	printf("\ni=FALSE: %d W:%lld",p->ArrayOfDecision[i].usedValue[FALSE],p->UnaryCosts[i][FALSE]);
	
	printf("\ni=TRUE: %d W:%lld",p->ArrayOfDecision[i].usedValue[TRUE],p->UnaryCosts[i][TRUE]);
	
	printf("\nj=FALSE: %d W:%lld",p->ArrayOfDecision[j].usedValue[FALSE],p->UnaryCosts[j][FALSE]);
	
	printf("\nj=TRUE: %d W:%lld",p->ArrayOfDecision[j].usedValue[TRUE],p->UnaryCosts[j][TRUE]);
	
	printf("\ni=FALSE -> j =FALSE :%lld",p->BinaryCosts[i][j][FALSE][FALSE]);
	
	printf("\ni=FALSE -> j =TRUE :%lld",p->BinaryCosts[i][j][FALSE][TRUE]);
	
	printf("\ni=TRUE -> j =FALSE :%lld",p->BinaryCosts[i][j][TRUE][FALSE]);
	
	printf("\ni=TRUE -> j =TRUE :%lld",p->BinaryCosts[i][j][TRUE][TRUE]);
}

int existFullSupport(problem * p,int i,int j)
{
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)	
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			if((p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE])!=0 && (p->BinaryCosts[i][j][FALSE][TRUE] +p->UnaryCosts[j][TRUE])!=0)
				
				return FALSE;			
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if((p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE])!=0) return FALSE;
			
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{		
				if((p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE])!=0) return FALSE;
		}		
	}
	
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			if((p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE])!=0 && (p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE])!=0)
				
				return FALSE;
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{	
			if((p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE])!=0) return FALSE;
		}	
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
				
		        if((p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE])!=0) return FALSE;
		}		
	}
	
	return TRUE;
}


int checkDAC(problem *p)
{
	
	int i,j;
	
	for(j=0;j<p->totalVariables;j++)
	{
		
		if(p->Result[j]==NO_VALUE) // only not assigned variables
		{
			if(!checkNC(p,j)) return FALSE;
			
			for(i=0;i<p->totalVariables;i++)
			{
				if(p->Result[i]==NO_VALUE) // only not assigned variables
				{
					if(p->order[i]<p->order[j]) // To mantain an order						
					{
						if (existFullSupport(p,i,j)==FALSE)
						{
							printf("\nNot exists full support between %d and %d",i,j);
							showDAC(p,i,j);							
						}
					}
					
				}
				
			}
			
		}
		
	}
	return TRUE;
}



int executeDAC(problem *p,restoreStruct * r)
{
	// DAC: Directed Arc-Consistency
	int j,i,k,flag=FALSE;
	
	while(p->R->n_elem>0)
	{
		
		j=popStackMaxDom(p->R);
		
		if(p->Result[j]==NO_VALUE)
		{
		
		if (pruneVar(p,r,j)==FALSE) return FALSE;
		// i<j
		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[i][j])
			{
				flag=orP(flag,findFullSupports(p,r,i,j));
				if(p->LB>=p->UB) return FALSE;
			}
		} // end for	
		}
	} // end while R
	
	if(flag)
	{
		if (pruneVariables(p,r)==FALSE) return FALSE;
	} // flag = TRUE
	
#if (LC_CHECK==1)
	
	if (LcLevel==LC_DAC)
		
		checkDAC(p);
	
#endif
	
	return TRUE;
}


/* FUNCTIONS TO MANTAIN FDAC CONSISTENCY */



int executeFDAC(problem *p,restoreStruct *r)
{

	// FDAC: Full Directed arc consistency

	int stop;
	stop=FALSE;

	while((p->Q->n_elem>0 || p->R->n_elem>0) && !stop)
	{

		stop= !executeAC3(p,r) || !executeDAC(p,r);

	}

#if(LC_CHECK==1)

	if (LcLevel==LC_FDAC && !stop)
	{
		checkDAC(p);
		checkAC3(p,r);
	}

#endif

	return(!stop);

}

/* FUNCTIONS TO MANTAIN HYPER BINARY RESOLUTION */


int getUnaryValue(problem *p,int v)
{
	if(p->UnaryCosts[v][FALSE]>0) return FALSE;
	else return TRUE;
}


void initStructuresDijkstra(problem *p,restoreStruct *r)
{
	int j;
	for(j=0;j<p->totalVariables;j++)
	{
		if(p->Result[j]==NO_VALUE && p->ArrayOfDecision[j].not_considered_values==2)
		{
			p->HR.n_d[j][FALSE]=NO_VALUE;
			p->HR.n_d[j][TRUE]=NO_VALUE;
			p->HR.distance[j][FALSE]=0;
			p->HR.distance[j][TRUE]=0;
		}
	}
}


int rebuildPath(problem *p,restoreStruct *r,int l,int vo,int vlo,int vd,int vld)
{
	cost_WM min;
	int vaux,prof;
	assignment v1,v2;

	// find minimum cost to move
	
	min=minV(p->UnaryCosts[vo][vlo],p->UnaryCosts[vd][negVal(vld)]);
	if(min>1)
	{
		v2.var=vd;
		v2.val=vld;
	
		v1.var=p->sPaths[vd][vld].var;
		v1.val=p->sPaths[vd][vld].val;
	
		while(v1.var!=-1 && min>1)
		{
			updateCurrent(p,v1.var,v2.var);
	
			//printf("\nB %d %d -> %d %d : %d\n",v2.var,v2.val,v1.var,negVal(v1.val),p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]);
			min=minV(min,p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]);
			
	
			v2.var=v1.var;
			v2.val=v1.val;
	
			vaux=v1.var;
	
			v1.var=p->sPaths[v1.var][v1.val].var;
			v1.val=p->sPaths[vaux][v1.val].val;
			
		}
	}

	//if(min>1) printf("\nHELLO\n");
	
	// move costs!!
	
	//min=1;
	v2.var=vd;
	v2.val=vld;

	v1.var=p->sPaths[vd][vld].var;
	v1.val=p->sPaths[vd][vld].val;

	
	prof=0;
	while(v1.var!=-1)
	{
		updateCurrent(p,v1.var,v2.var);

		//printf("\nB %d %d -> %d %d : %d\n",v2.var,v2.val,v1.var,negVal(v1.val),p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]);
		if(p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]<=0) prof=1;
		
		(*nopassBinaryJeroslow[HeurVar])(p,v1.var,v2.var);
		if( (p->BinaryCosts[v1.var][v2.var][v1.val][negVal(v2.val)]==0) && (p->UnaryCosts[v1.var][negVal(v1.val)]>0 || p->UnaryCosts[v2.var][v2.val]>0))
		{
			if(v1.var>v2.var)
			{
				pushStack(p->R,v1.var);
				
			}
			else
			{
				pushStack(p->R,v2.var);
			}

		}
		
		p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]-=min;
		p->BinaryCosts[v2.var][v1.var][v2.val][negVal(v1.val)]-=min;

		p->BinaryCosts[v1.var][v2.var][v1.val][negVal(v2.val)]+=min;
		p->BinaryCosts[v2.var][v1.var][negVal(v2.val)][v1.val]+=min;

		

		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1.var,v2.var,negVal(v1.val),v2.val,min));
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1.var,v2.var,v1.val,negVal(v2.val),-min));

		setIsBinaryPair(p,v1.var,v2.var,isBinaryPair(p,v1.var,v2.var));
		(*passBinaryJeroslow[HeurVar])(p,v1.var,v2.var);

		changedCurrent(p,v1.var,v2.var);

		v2.var=v1.var;
		v2.val=v1.val;

		vaux=v1.var;

		v1.var=p->sPaths[v1.var][v1.val].var;
		v1.val=p->sPaths[vaux][v1.val].val;
		
	}		

	p->UnaryCosts[vo][vlo]-=min;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,vo,vlo,min));

	p->UnaryCosts[vd][vld]+=min;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,vd,vld,-min));

	projectUnary(p,r,vd);

	//if(prof==1) getchar();

	return TRUE;
}


int rebuildPath2(problem *p,restoreStruct *r,int l,int vo,int vlo,int vd,int vld)
{
	cost_WM min;
	int vaux,prof;
	assignment v1,v2;
	
	// find minimum cost to move
	min=p->UnaryCosts[vo][vlo]/2;
	min=minV(min,p->BinaryCosts[vd][vo][negVal(vld)][negVal(vlo)]);
	if(min>1)
	{
		v2.var=vd;
		v2.val=vld;
	
		v1.var=p->sPaths[vd][vld].var;
		v1.val=p->sPaths[vd][vld].val;
	
		while(v1.var!=-1 && min>1)
		{
			updateCurrent(p,v1.var,v2.var);
	
			min=minV(min,p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]);
			
	
			v2.var=v1.var;
			v2.val=v1.val;
	
			vaux=v1.var;
	
			v1.var=p->sPaths[v1.var][v1.val].var;
			v1.val=p->sPaths[vaux][v1.val].val;
			
		}
	}
	
	
	// move costs!!
	
	//min=1;

	updateCurrent(p,vd,vo);
	if(p->BinaryCosts[vd][vo][negVal(vld)][negVal(vlo)]<=0) prof=1;


	(*nopassBinaryJeroslow[HeurVar])(p,vd,vo);
	if( (p->BinaryCosts[vd][vo][vld][negVal(vlo)]==0) && (p->UnaryCosts[vd][negVal(vld)]>0 || p->UnaryCosts[vo][vlo]>0) )
	{
		if(vd>vo)
		{
			pushStack(p->R,vd);
			
		}
		else
		{
			pushStack(p->R,vo);
		}

	}
	p->BinaryCosts[vd][vo][negVal(vld)][negVal(vlo)]-=min;
	p->BinaryCosts[vo][vd][negVal(vlo)][negVal(vld)]-=min;
	p->BinaryCosts[vd][vo][vld][vlo]+=min;
	p->BinaryCosts[vo][vd][vlo][vld]+=min;
	insertElement(&r->ACList,getNewNode5(&p->restoreList,vd,vo,negVal(vld),negVal(vlo),min));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,vd,vo,vld,vlo,-min));
	setIsBinaryPair(p,vd,vo,isBinaryPair(p,vd,vo));
	(*passBinaryJeroslow[HeurVar])(p,vd,vo);
	changedCurrent(p,vo,vd);

	v2.var=vd;
	v2.val=vld;

	v1.var=p->sPaths[vd][vld].var;
	v1.val=p->sPaths[vd][vld].val;
	//showUnary(p,vo,vlo);
	//showBinary(p,vo,vd,negVal(vlo),negVal(vld));
	
	
	prof=0;
	while(v1.var!=-1)
	{
		updateCurrent(p,v1.var,v2.var);


		if(p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]<=0) prof=1;
		
		(*nopassBinaryJeroslow[HeurVar])(p,v1.var,v2.var);
		if( (p->BinaryCosts[v1.var][v2.var][v1.val][negVal(v2.val)]==0) && (p->UnaryCosts[v1.var][negVal(v1.val)]>0 || p->UnaryCosts[v2.var][v2.val]>0))
		{
			if(v1.var>v2.var)
			{
				pushStack(p->R,v1.var);
				
			}
			else
			{
				pushStack(p->R,v2.var);
			}

		}
		
		p->BinaryCosts[v1.var][v2.var][negVal(v1.val)][v2.val]-=min;
		p->BinaryCosts[v2.var][v1.var][v2.val][negVal(v1.val)]-=min;

		p->BinaryCosts[v1.var][v2.var][v1.val][negVal(v2.val)]+=min;
		p->BinaryCosts[v2.var][v1.var][negVal(v2.val)][v1.val]+=min;

		//showBinary(p,v2.var,v1.var,v2.val,negVal(v1.val));
		

		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1.var,v2.var,negVal(v1.val),v2.val,min));
		insertElement(&r->ACList,getNewNode5(&p->restoreList,v1.var,v2.var,v1.val,negVal(v2.val),-min));

		setIsBinaryPair(p,v1.var,v2.var,isBinaryPair(p,v1.var,v2.var));
		(*passBinaryJeroslow[HeurVar])(p,v1.var,v2.var);		


		changedCurrent(p,v1.var,v2.var);

		v2.var=v1.var;
		v2.val=v1.val;

		vaux=v1.var;

		v1.var=p->sPaths[v1.var][v1.val].var;
		v1.val=p->sPaths[vaux][v1.val].val;
		//getchar();
		//prof++;
		
	}

	p->UnaryCosts[vo][vlo]-=min;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,vo,vlo,min));

	p->UnaryCosts[vo][negVal(vlo)]+=min;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,vo,negVal(vlo),-min));

	projectUnary(p,r,vo);

	//showUnary(p,vo,vlo);
	//getchar();
		


	if(prof==1)
	{
		printf("\nFALLO\n");
		getchar();
	}

	return TRUE;
}


int canBeExtendedPath(problem *p,restoreStruct *r,int vo,int vlo,int vd,int vld)
{
	int vaux,prof;
	assignment v1,v2;

	itDijkstra2++;

	v2.var=vd;
	v2.val=vld;

	v1.var=p->sPaths[vd][vld].var;
	v1.val=p->sPaths[vd][vld].val;

	prof=0;
	while(v1.var!=-1)
	{
		//printf("\nHOLA %d %d %d %d\n",v1.var,v2.var,negVal(v1.val),v2.val);
		if(p->isVisitedBinary[v1.var][v2.var][negVal(v1.val)][v2.val]==itDijkstra2)
		{
			return FALSE;
		}
		else
		{
			p->isVisitedBinary[v1.var][v2.var][negVal(v1.val)][v2.val]=itDijkstra2;
			p->isVisitedBinary[v2.var][v1.var][v2.val][negVal(v1.val)]=itDijkstra2;
		}

		v2.var=v1.var;
		v2.val=v1.val;

		vaux=v1.var;

		v1.var=p->sPaths[v1.var][v1.val].var;
		v1.val=p->sPaths[vaux][v1.val].val;
	} // end while
	//printf("\nADIOS");
	//if(prof==1) getchar();

	return TRUE;
}

int generatePaths(problem *p,restoreStruct *r,int l,int vi,int vli,int vd,int vld,int *t_p)
{
	int i,res;
	if(p->HR.distance[vd][vld]==0)
	{
		//printf(" END \n");
		p->sPaths[vd][vld].var=-1;
		p->sPaths[vd][vld].val=-1;
		if(canBeExtendedPath(p,r,vd,vld,vi,vli)==TRUE)
		{
			*t_p=*t_p+1;
			//if(*t_p>1) printf("\n MAS DE UN INTENTO %d \n",*t_p);
			rebuildPath(p,r,l,vd,vld,vi,vli);
			return TRUE;
		}
		else
		{
			*t_p=*t_p+1;
			return FALSE;
		}
	}
	else
	{
		//if(p->HR.n_d[vd][vld]>0) printf("\n PROF %d NUM %d",l,p->HR.n_d[vd][vld]);
		for(i=0;i<=p->HR.n_d[vd][vld] && *t_p<p->totalVariables*4;i++)
		{
			//printf(" %d-%d ",vd,vld);
			p->sPaths[vd][vld].var=p->HR.paths[vd][vld][i].var;
			p->sPaths[vd][vld].val=p->HR.paths[vd][vld][i].val;
			res=generatePaths(p,r,l-1,vi,vli,p->HR.paths[vd][vld][i].var,p->HR.paths[vd][vld][i].val,t_p);
			if(res==TRUE) return TRUE;
		}
		return FALSE;
	}

}

int canBeExtendedPath2(problem *p,restoreStruct *r,int vo,int vlo,int vd,int vld)
{
	int vaux,prof;
	assignment v1,v2;

	itDijkstra2++;

	v2.var=vd;
	v2.val=vld;

	v1.var=p->sPaths[vd][vld].var;
	v1.val=p->sPaths[vd][vld].val;

	prof=0;
	while(v1.var!=-1)
	{
		//printf("\nHOLA %d %d %d %d\n",v1.var,v2.var,negVal(v1.val),v2.val);
		if(p->isVisitedBinary[v1.var][v2.var][negVal(v1.val)][v2.val]==itDijkstra2)
		{
			return FALSE;
		}
		else
		{
			p->isVisitedBinary[v1.var][v2.var][negVal(v1.val)][v2.val]=itDijkstra2;
			p->isVisitedBinary[v2.var][v1.var][v2.val][negVal(v1.val)]=itDijkstra2;
		}

		v2.var=v1.var;
		v2.val=v1.val;

		vaux=v1.var;

		v1.var=p->sPaths[v1.var][v1.val].var;
		v1.val=p->sPaths[vaux][v1.val].val;
	} // end while
	//printf("\nADIOS");
	//if(prof==1) getchar();
	if(p->isVisitedBinary[vd][vo][negVal(vld)][negVal(vlo)]==itDijkstra2) return FALSE;

	return TRUE;
}


int generatePaths2(problem *p,restoreStruct *r,int l,int vi,int vli,int vd,int vld,int *t_p)
{
	int i,res;
	if(p->HR.distance[vd][vld]==0)
	{
		//printf(" END \n");
		p->sPaths[vd][vld].var=-1;
		p->sPaths[vd][vld].val=-1;
		if(canBeExtendedPath2(p,r,vd,vld,vi,vli)==TRUE)
		{
			*t_p=*t_p+1;
			//if(*t_p>1) printf("\n MAS DE UN INTENTO %d \n",*t_p);
			rebuildPath2(p,r,l,vd,vld,vi,vli);
			return TRUE;
		}
		else
		{
			*t_p=*t_p+1;
			return FALSE;
		}
	}
	else
	{
		//if(p->HR.n_d[vd][vld]>0) printf("\n PROF %d NUM %d",l,p->HR.n_d[vd][vld]);
		for(i=0;i<=p->HR.n_d[vd][vld] && *t_p<p->totalVariables*4;i++)
		{
			//printf(" %d-%d ",vd,vld);
			p->sPaths[vd][vld].var=p->HR.paths[vd][vld][i].var;
			p->sPaths[vd][vld].val=p->HR.paths[vd][vld][i].val;
			res=generatePaths2(p,r,l-1,vi,vli,p->HR.paths[vd][vld][i].var,p->HR.paths[vd][vld][i].val,t_p);
			if(res==TRUE) return TRUE;
		}
		return FALSE;
	}

}


int dijkstra_allPaths(problem *p,restoreStruct *r,int v)
{
	int l_inf,l_sup,next,future,i,pos,cVar,cVal,it,vals,vo,vlo,prof,t_p;

	itDijkstra++;
	
	l_inf=0;
	l_sup=0;
	future=1;

	initStructuresDijkstra(p,r);

	vo=v;
	vlo=getUnaryValue(p,v);
	p->toVisit[0].var=vo;
	p->toVisit[0].val=vlo;

	// Mark v as the starting node
	p->isVisited[v][FALSE]=itDijkstra;
	p->isVisited[v][TRUE]=itDijkstra;

	p->HR.n_d[v][FALSE]=0;
	p->HR.n_d[v][TRUE]=0;
	p->HR.paths[v][FALSE][p->HR.n_d[v][FALSE]].var=-1;
	p->HR.paths[v][FALSE][p->HR.n_d[v][FALSE]].val=-1;
	p->HR.paths[v][TRUE][p->HR.n_d[v][TRUE]].var=-1;
	p->HR.paths[v][TRUE][p->HR.n_d[v][TRUE]].val=-1;

	it=1;
	prof=1;
	while(future>0)
	{
		next=0;
		l_sup=l_inf+future;
		for(pos=l_inf;pos<l_sup;pos++)
		{
			cVar=p->toVisit[pos].var;
			cVal=p->toVisit[pos].val;

			if(p->BinaryCosts[cVar][vo][negVal(cVal)][negVal(vlo)]>BOTTOM && p->UnaryCosts[vo][vlo]>1)
			{
				t_p=0;
				if(generatePaths2(p,r,prof,cVar,cVal,cVar,cVal,&t_p)==TRUE) return TRUE;
			}

			for(i=0;i<p->totalVariables;i++)
			{
				if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2 && p->isBinary[cVar][i]==TRUE && i!=vo && i!=cVar)
				{

					for(vals=0;vals<2;vals++)
					{
					if(p->BinaryCosts[cVar][i][negVal(cVal)][vals]>BOTTOM)
					{
							if(p->isVisited[i][vals]!=itDijkstra)
							{
								// Encontrado primer nodo adyacente.
								p->HR.n_d[i][vals]++;
								p->HR.distance[i][vals]=prof;
								p->isVisited[i][vals]=itDijkstra;
								p->HR.paths[i][vals][p->HR.n_d[i][vals]].var=cVar;
								p->HR.paths[i][vals][p->HR.n_d[i][vals]].val=cVal;
								
								p->toVisit[l_sup+next].var=i;
								p->toVisit[l_sup+next].val=vals;
																
								if(p->UnaryCosts[i][negVal(vals)]>BOTTOM)
								{
									t_p=0;
									if(generatePaths(p,r,prof,i,vals,i,vals,&t_p)==TRUE) return TRUE;
								}
								it++;
								next++;
							}
							else if(p->isVisited[i][vals]==itDijkstra && p->HR.distance[i][vals]==prof)
							{
								// Encontrado segundo/tercero... nodos adyacentes m� cercanos.
								p->HR.n_d[i][vals]++;
								p->HR.paths[i][vals][p->HR.n_d[i][vals]].var=cVar;
								p->HR.paths[i][vals][p->HR.n_d[i][vals]].val=cVal;

								if(p->UnaryCosts[i][negVal(vals)]>BOTTOM)
								{
									t_p=0;
									if(generatePaths(p,r,prof,i,vals,i,vals,&t_p)==TRUE) return TRUE;
								}

							}
					}
					} // for vals
					
				}
			} // end for(i
		} // end for pos
		l_inf=l_inf+future;
		future=next;
		prof++;
	} // end while
	
	return FALSE;
}

int hyperBinaryResolution(problem *p,restoreStruct *r)
{
	int i,c=0,change;
	
	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2 && (p->UnaryCosts[i][FALSE]>BOTTOM || p->UnaryCosts[i][TRUE]>BOTTOM))
		{
			c++;
		}
	}
	if(c>=2)
	{
		change=TRUE;
		while(change==TRUE)
		{
			change=FALSE;
			for(i=0;i<p->totalVariables;i++)
			{
				if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2 && (p->UnaryCosts[i][FALSE]>BOTTOM || p->UnaryCosts[i][TRUE]>BOTTOM))
				{
					if(dijkstra_allPaths(p,r,i)==TRUE)
					{
						change=TRUE;
						if(p->LB>=p->UB) return FALSE;
					}
					
				}
			} // end for
			if(change==TRUE)
			{
				if (pruneVariables(p,r)==FALSE) return FALSE;
			}
		} // end while
	}
	return TRUE;
}

/* END -- FUNCTIONS TO MANTAIN HYPER BINARY RESOLUTION */


/* FUNCTIONS TO MANTAIN EDAC CONSISTENCY */

int calculateEpsilon(problem *p,int j,int k,int val)
{
	cost_WM res=0;
	if(p->ArrayOfDecision[k].not_considered_values==2)
	{
		res=minV((p->UnaryCosts[k][FALSE]+p->BinaryCosts[j][k][val][FALSE]),(p->UnaryCosts[k][TRUE]+p->BinaryCosts[j][k][val][TRUE]));
	}
	else if(p->ArrayOfDecision[k].usedValue[FALSE]==FALSE)
	{
		res=p->UnaryCosts[k][FALSE]+p->BinaryCosts[j][k][val][FALSE];
	}
	else if(p->ArrayOfDecision[k].usedValue[TRUE]==FALSE)
	{
		res=p->UnaryCosts[k][TRUE]+p->BinaryCosts[j][k][val][TRUE];
	}

	return res;
}


int condAdd(problem *p,int i)
{
	if(p->ArrayOfDecision[i].not_considered_values==2)
	{
		if(p->UnaryCosts[i][FALSE]+p->UnaryCosts[i][TRUE]>BOTTOM) return TRUE;
	}
	else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->UnaryCosts[i][FALSE]>BOTTOM) return TRUE;
	}
	else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->UnaryCosts[i][TRUE]>BOTTOM) return TRUE;
	}

	return FALSE;
}

void autoSetCondAdd(problem *p,int i)
{
	p->tmpArr[i]=condAdd(p,i);
	
}

void initConds(problem *p)
{
	int k;
	for(k=0;k<p->totalVariables;k++)
	{
		if(p->Result[k]==NO_VALUE)
		{
			autoSetCondAdd(p,k);
		}
	}
}

int execEAC(problem *p)
{
	if((p->UB-p->LB)==1) return FALSE;
	initConds(p);
	return TRUE;
}


int considerVar(problem *p,int i)
{
	return p->tmpArr[i];
}


int existsEpsilonSupport(problem *p,restoreStruct *r,int j)
{
	int k;
	cost_WM epsilonF=0,epsilonT=0;

	if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE) epsilonF+=p->UnaryCosts[j][FALSE];
	else epsilonF=-1;

	if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE) epsilonT+=p->UnaryCosts[j][TRUE];
	else epsilonT=-1;

	for(k=0;k<p->totalVariables;k++)
	{
		if(p->Result[k]==NO_VALUE)
		{
			if(p->order[k]!=p->order[j] && p->isBinary[j][k])
			{
				if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE) epsilonF+=calculateEpsilon(p,j,k,FALSE);
				if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE) epsilonT+=calculateEpsilon(p,j,k,TRUE);
			}
		}
	}

	if(p->ArrayOfDecision[j].not_considered_values==2)
	{
		if(epsilonF>0 && epsilonT>0)
		{
			printf("\nX%d:=FALSE (%lld-%lld)  X%d:=TRUE (%lld-%lld)\n",j,p->UnaryCosts[j][FALSE],epsilonF,j,p->UnaryCosts[j][TRUE],epsilonT);
			return FALSE;
		}
	}
	else
	{
		if(epsilonF==-1 && epsilonT>BOTTOM)
		{
			printf("X%d:=TRUE (%lld)",j,epsilonT);
			return FALSE;
		}
		else if(epsilonF>BOTTOM && epsilonT==-1)
		{
			printf("X%d:=FALSE (%lld)",j,epsilonF);
			return FALSE;
		}
	}


	return TRUE;
}

int checkEAC(problem *p,restoreStruct *r)
{
	int n_v;
	
	for(n_v=0;n_v<p->totalVariables;n_v++)
	{
	if(p->Result[n_v]==NO_VALUE)
	{
		existsEpsilonSupport(p,r,n_v);
	}
	}
	
	return TRUE;
}

int moveCosts(problem *p,restoreStruct *r,int j,int *flag)
{

	int i,k;
	
	for(k=p->order[j]-1;k>=0;k--)
	{
		i=p->order2[k];
		if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
		{
				*flag=orP(*flag,findFullSupports(p,r,j,i));
				if(p->LB>=p->UB) return FALSE;
				autoSetCondAdd(p,i);
		}
	}


	autoSetCondAdd(p,j);

	return TRUE;

}



int existentialSupport(problem *p,restoreStruct *r,int j,int *flag)
{
	int i,k;
	cost_WM epsilonF=0,epsilonT=0;

	if(p->ArrayOfDecision[j].not_considered_values==2)
	{
		epsilonF+=p->UnaryCosts[j][FALSE];

		epsilonT+=p->UnaryCosts[j][TRUE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					//epsilonF+=calculateEpsilon(p,j,k,FALSE);
					//epsilonT+=calculateEpsilon(p,j,k,TRUE);
					
					epsilonF+=calculateEpsilon(p,j,i,FALSE);
					epsilonT+=calculateEpsilon(p,j,i,TRUE);

			}
		}

		if(epsilonF>BOTTOM && epsilonT>BOTTOM) moveCosts(p,r,j,flag);

	}

	else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
	{
		epsilonF+=p->UnaryCosts[j][FALSE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					//epsilonF+=calculateEpsilon(p,j,k,FALSE);
					epsilonF+=calculateEpsilon(p,j,i,FALSE);
			}
		}


		if(epsilonF>BOTTOM) moveCosts(p,r,j,flag);
	}

	else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
	{
		epsilonT+=p->UnaryCosts[j][TRUE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					//epsilonT+=calculateEpsilon(p,j,k,TRUE);
					epsilonT+=calculateEpsilon(p,j,i,TRUE);
			}
		}


		if(epsilonT>BOTTOM) moveCosts(p,r,j,flag);
	}

	return TRUE;
}


int executeEAC(problem *p,restoreStruct *r)
{
	int i,flag;
	flag=FALSE;
	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==NO_VALUE)
		{
			if(!existentialSupport(p,r,i,&flag)) return FALSE;
		}
	}

	if(flag)
	{
		// Prune new values with the new low-bound
		if (pruneVariables(p,r)==FALSE) return FALSE;
	} // flag = TRUE


	return TRUE;
}



int executeEDAC(problem *p,restoreStruct *r)
{
	int stop;
	stop=FALSE;

	if(!stop && execEAC(p)==TRUE) stop=!executeEAC(p,r);
	
	do
	{
		do
		{
			if(!stop) stop=!executeDAC(p,r);
			if(!stop) stop=!executeAC3(p,r);
			if(!stop && execEAC(p)==TRUE) stop=!executeEAC(p,r);
		}
		while((p->Q->n_elem>0 || p->R->n_elem>0) && !stop);

		if(max_sat_rules==RULES_NRES)
		{
			if(!stop) stop=!hyperBinaryResolution(p,r);
		}
		
		
	}
	while((p->Q->n_elem>0 || p->R->n_elem>0) && !stop);
	

#if(LC_CHECK==1)

	if (LcLevel==LC_EDAC && !stop)
	{
		checkNodeConsistency(p);
		checkDAC(p);
		checkAC3(p,r);
		checkEAC(p,r);
	}

#endif
	return(!stop);
}


/* PATH CONSISTENCIES */
/* PIC FUNCTIONS*/
int condLC(problem *p)
{
	if(p->Q->n_elem>0 || p->R->n_elem>0) return TRUE;
	else return FALSE;
}

int translateTriangle(problem *p,restoreStruct *r,int i,int j,int k,int v1,int v2,int v3,int *flag)
{
	cost_WM minCost;
	int c;
	ReferenceToClause * rc;

	minCost=p->BinaryCosts[i][j][v1][v2];
	minCost=minV(minCost, p->BinaryCosts[i][k][v1][v3]);
	minCost=minV(minCost, p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]);

	if(minCost==0) printf("\n COST ZERO 1\n");

	(*nopassBinaryJeroslow[HeurVar])(p,i,j);
	(*nopassBinaryJeroslow[HeurVar])(p,i,k);
	(*nopassBinaryJeroslow[HeurVar])(p,j,k);
	
	p->BinaryCosts[i][j][v1][v2]=p->BinaryCosts[i][j][v1][v2]-minCost;
	p->BinaryCosts[j][i][v2][v1]=p->BinaryCosts[j][i][v2][v1]-minCost;
	setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,v1,v2,minCost));
	
	p->BinaryCosts[i][k][v1][v3]=p->BinaryCosts[i][k][v1][v3]-minCost;
	p->BinaryCosts[k][i][v3][v1]=p->BinaryCosts[k][i][v3][v1]-minCost;
	setIsBinaryPair(p,i,k,isBinaryPair(p,i,k));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,i,k,v1,v3,minCost));
	
	p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]=p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]-minCost;
	p->BinaryCosts[k][j][negVal(v3)][negVal(v2)]=p->BinaryCosts[k][j][negVal(v3)][negVal(v2)]-minCost;	
	setIsBinaryPair(p,j,k,isBinaryPair(p,j,k));
	insertElement(&r->ACList,getNewNode5(&p->restoreList,j,k,negVal(v2),negVal(v3),minCost));

	(*passBinaryJeroslow[HeurVar])(p,i,j);
	(*passBinaryJeroslow[HeurVar])(p,i,k);
	(*passBinaryJeroslow[HeurVar])(p,j,k);
	
	// (A,minCost)
	if(p->UnaryCosts[i][v1]==BOTTOM)
	{
		pushStack(p->R,i);
		pushStack(p->Q,i);
	}
	p->UnaryCosts[i][v1]+=minCost;
	insertElement(&r->NCList,getNewNode3(&p->restoreList,i,v1,-minCost));
	if(projectUnary(p,r,i) || (p->UnaryCosts[i][v1]+p->LB>=p->UB)) *flag=TRUE;
	if(p->LB>=p->UB) return FALSE;
		
	// (A v B v C,minCost)
	p->totalClauses++;
	if(p->totalClauses>p->maxClauses)
	{
		printf("\n You should solve this problem with EDAC -l4 option %d %d \n",p->totalClauses,p->maxClauses);
		exit(-1);
	}
	c=p->totalClauses-1;
	p->clauses[c].weight=minCost;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=3;
	p->clauses[c].literalsTotal=3;
	
	p->clauses[c].TLiterals[0].idVar=i;
	p->clauses[c].TLiterals[0].boolValue=v1;
	rc=createReference(c);
	if (p->clauses[c].TLiterals[0].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[1].idVar=j;
	p->clauses[c].TLiterals[1].boolValue=v2;
	rc=createReference(c);
	if (p->clauses[c].TLiterals[1].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[2].idVar=k;
	p->clauses[c].TLiterals[2].boolValue=v3;
	rc=createReference(c);
	if (p->clauses[c].TLiterals[2].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listLiterals,rc);
	passJeroslow(p,c);
	
	// (A v B v C,minCost)
	p->totalClauses++;
	c=p->totalClauses-1;	
	p->clauses[c].weight=minCost;
	p->clauses[c].elim=FALSE;
	p->clauses[c].terminator=NULL;
	p->clauses[c].literalsWithoutAssign=3;
	p->clauses[c].literalsTotal=3;
	
	p->clauses[c].TLiterals[0].idVar=i;
	p->clauses[c].TLiterals[0].boolValue=negVal(v1);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[0].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[0].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[1].idVar=j;
	p->clauses[c].TLiterals[1].boolValue=negVal(v2);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[1].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[1].idVar].listLiterals,rc);
	
	p->clauses[c].TLiterals[2].idVar=k;
	p->clauses[c].TLiterals[2].boolValue=negVal(v3);
	rc=createReference(c);
	if (p->clauses[c].TLiterals[2].boolValue==0) insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listNoLiterals,rc);
	else insertReference(p->variables[p->clauses[c].TLiterals[2].idVar].listLiterals,rc);
	passJeroslow(p,c);
	
	r->new3SAT+=2;
	return TRUE;
}


int checkPIC(problem *p,restoreStruct *r)
{
	int i,j,k,v1,v2,v3,counter;
	counter=0;

	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==-1 && p->ArrayOfDecision[i].not_considered_values==2)
		{
			for(v1=0;v1<2;v1++)
			{
				for(j=0;j<p->totalVariables;j++)
				{
					if(i!=j && p->Result[j]==-1 && p->ArrayOfDecision[j].not_considered_values==2)
					{
						for(v2=0;v2<2;v2++)
						{
							if(p->BinaryCosts[i][j][v1][v2]>BOTTOM)
							{
								for(k=0;k<p->totalVariables;k++)
								{
									if(k!=i && k!=j && p->Result[k]==-1 && p->ArrayOfDecision[k].not_considered_values==2)
									{
										for(v3=0;v3<2;v3++)
										{
											if(p->BinaryCosts[i][j][v1][v2]>BOTTOM && p->BinaryCosts[i][k][v1][v3]>BOTTOM && p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]>BOTTOM)
											{
												counter++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	if(counter>0) printf("\n PIC-inconsistencies:%d \n",counter);
	return counter;
}


int executePIC(problem *p,restoreStruct *r)
{
	int e,i,j,k,v1,v2,v3,counter,flag=FALSE;
	counter=0;
		
	for(e=0;e<p->cTri.triT;e++)
	{
		i=p->cTri.triStack[e].v1;
		j=p->cTri.triStack[e].v2;
		v1=p->cTri.triStack[e].v3;
		v2=p->cTri.triStack[e].v4;

		if(p->isBinary[i][j] && p->ArrayOfDecision[i].not_considered_values==2 && p->ArrayOfDecision[j].not_considered_values==2 && p->Result[i]==NO_VALUE && p->Result[j]==NO_VALUE)
		{
			for(k=0;k<p->totalVariables;k++)
			{
				if(k!=i && k!=j && p->Result[k]==NO_VALUE && p->ArrayOfDecision[k].not_considered_values==2 && p->isBinary[i][k] && p->isBinary[j][k])
				{
					v3=FALSE;
					if(p->BinaryCosts[i][j][v1][v2]>BOTTOM && p->BinaryCosts[i][k][v1][v3]>BOTTOM && p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]>BOTTOM)
					{
						if(translateTriangle(p,r,i,j,k,v1,v2,v3,&flag)==FALSE) return FALSE;
						counter++;
					}
					if(p->BinaryCosts[j][i][v2][v1]>BOTTOM && p->BinaryCosts[j][k][v2][v3]>BOTTOM && p->BinaryCosts[i][k][negVal(v1)][negVal(v3)]>BOTTOM)
					{
						if(translateTriangle(p,r,j,i,k,v2,v1,v3,&flag)==FALSE) return FALSE;
						counter++;
					}						
					
					if(p->BinaryCosts[k][i][v3][negVal(v1)]>BOTTOM && p->BinaryCosts[k][j][v3][negVal(v2)]>BOTTOM && p->BinaryCosts[i][j][v1][v2]>BOTTOM)
					{
						if(translateTriangle(p,r,k,i,j,v3,negVal(v1),negVal(v2),&flag)==FALSE) return FALSE;
						counter++;
					}
					
					if(p->BinaryCosts[i][j][v1][v2]>BOTTOM && p->BinaryCosts[i][k][v1][negVal(v3)]>BOTTOM && p->BinaryCosts[j][k][negVal(v2)][v3]>BOTTOM)
					{
						if(translateTriangle(p,r,i,j,k,v1,v2,negVal(v3),&flag)==FALSE) return FALSE;
						counter++;
					}

					if(p->BinaryCosts[j][i][v2][v1]>BOTTOM && p->BinaryCosts[j][k][v2][negVal(v3)]>BOTTOM && p->BinaryCosts[i][k][negVal(v1)][v3]>BOTTOM)
					{
						if(translateTriangle(p,r,j,i,k,v2,v1,negVal(v3),&flag)==FALSE) return FALSE;
						counter++;
					}

					if(p->BinaryCosts[k][i][negVal(v3)][negVal(v1)]>BOTTOM && p->BinaryCosts[k][j][negVal(v3)][negVal(v2)]>BOTTOM && p->BinaryCosts[i][j][v1][v2]>BOTTOM)
					{
						if(translateTriangle(p,r,k,i,j,negVal(v3),negVal(v1),negVal(v2),&flag)==FALSE) return FALSE;
						counter++;
					}
				}
			}
		}
	}

	p->cTri.triT=0;
	
	if(flag)
	{
		// Prune new values with the new low-bound
		if (pruneVariables(p,r)==FALSE) return FALSE;

	} // flag = TRUE

	return TRUE;
}
int executeEDAC_PIC(problem *p,restoreStruct *r)
{

	int ret=TRUE,first=TRUE;

	first=TRUE;
	while(first==TRUE || condLC(p) )
	{
	
		while(first==TRUE || condLC(p) )
		{		
			while(first==TRUE || condLC(p) )
			{
				first=FALSE;
				ret=executeEDAC(p,r);
				if(!ret) return ret;
				
				ret=executePIC(p,r);
				if(!ret) return ret;
			}
		}

	}
#if(LC_CHECK==1)
	if(ret==TRUE && (LcLevel==LC_EDAC_PIC || LcLevel==LC_HYPER))
	{
		checkPIC(p,r);
		checkAC3(p,r);
		checkEAC(p,r);
		checkDAC(p);
	}
#endif		
	return ret;
}

/* Not used functions*/


int generateUnary(problem *p,restoreStruct *r)
{
	int i,j,k,v1,v2,v3,counter,flag=FALSE;

	counter=0;
	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==-1 && p->ArrayOfDecision[i].not_considered_values==2)
		{
			for(v1=0;v1<2;v1++)
			{
				for(j=0;j<p->totalVariables;j++)
				{
					if(i!=j && p->Result[j]==-1 && p->ArrayOfDecision[j].not_considered_values==2 && p->isBinary[i][j])
					{
						for(v2=0;v2<2;v2++)
						{
							if(p->BinaryCosts[i][j][v1][v2]>BOTTOM)
							{
								for(k=0;k<p->totalVariables;k++)
								{
									if(k!=i && k>j && p->Result[k]==-1 && p->ArrayOfDecision[k].not_considered_values==2 && p->isBinary[i][k] && p->isBinary[j][k])
									{
										for(v3=0;v3<2;v3++)
										{
											if(p->BinaryCosts[i][j][v1][v2]>BOTTOM && p->BinaryCosts[i][k][v1][v3]>BOTTOM && p->BinaryCosts[j][k][negVal(v2)][negVal(v3)]>BOTTOM)
											{
												if(translateTriangle(p,r,i,j,k,v1,v2,v3,&flag)==FALSE) return FALSE;
												counter++;
											}
										}
									}

								}
							}
						}
					}
				}
			}
		}
	}
	if(flag)
	{
		// Prune new values with the new low-bound
		if (pruneVariables(p,r)==FALSE) return FALSE;
	} // flag = TRUE

		
	return TRUE;
}


int GeneralizedArcConsistency(problem *p,restoreStruct *r)
{

int c,lit,i,v,enc;

cost_WM min;

for(c=0;c<p->totalClauses;c++)
{
if(p->clauses[c].elim == FALSE && p->clauses[c].GenLC==TRUE && p->clauses[c].weight>=p->UB && p->clauses[c].literalsWithoutAssign>2)
{
	for(i=0;i<p->totalVariables;i++)
	{
		min=p->UB;
		if(p->Result[i]==NO_VALUE && p->ArrayOfDecision[i].not_considered_values==2)
		{
		for(v=0;v<2;v++)
		{
		
		enc=TRUE;
		for(lit=0;lit<p->clauses[c].literalsTotal && enc==TRUE;lit++)
		{
		
		if(p->Result[p->clauses[c].TLiterals[lit].idVar]==NO_VALUE)
		{
			if(p->BinaryCosts[i][p->clauses[c].TLiterals[lit].idVar][v][p->clauses[c].TLiterals[lit].boolValue]>0)
			{
				min=minV(min,p->BinaryCosts[i][p->clauses[c].TLiterals[lit].idVar][v][p->clauses[c].TLiterals[lit].boolValue]);
			}
			else
			{
				enc=FALSE;
			}
		}
		} // for lit
		
		if(enc==TRUE)
		{
		
			pushStack(p->R,i);
			pushStack(p->Q,i);
			
			p->UnaryCosts[i][v]+=min;
			
			insertElement(&r->NCList,getNewNode3(&p->restoreList,i,v,-min));
			
			projectUnary(p,r,i);
			
			if(p->LB>=p->UB) return FALSE;
			
				for(lit=0;lit<p->clauses[c].literalsTotal;lit++)
				{
				if(p->Result[p->clauses[c].TLiterals[lit].idVar]==NO_VALUE)
				{
					if(p->BinaryCosts[i][p->clauses[c].TLiterals[lit].idVar][v][p->clauses[c].TLiterals[lit].boolValue]>0)
					{
					
(*nopassBinaryJeroslow[HeurVar])(p,i,p->clauses[c].TLiterals[lit].idVar);

p->BinaryCosts[i][p->clauses[c].TLiterals[lit].idVar][v][p->clauses[c].TLiterals[lit].boolValue]-=min;
p->BinaryCosts[p->clauses[c].TLiterals[lit].idVar][i][p->clauses[c].TLiterals[lit].boolValue][v]-=min;

setIsBinaryPair(p,i,p->clauses[c].TLiterals[lit].idVar,isBinaryPair(p,i,p->clauses[c].TLiterals[lit].idVar));

insertElement(&r->ACList,getNewNode5(&p->restoreList,i,p->clauses[c].TLiterals[lit].idVar,v,p->clauses[c].TLiterals[lit].boolValue,min));


(*passBinaryJeroslow[HeurVar])(p,i,p->clauses[c].TLiterals[lit].idVar);

					}
				}
				
				}
		} // enc
		
				
		} // for v
		
		} 
	}
}
}

return TRUE;
}

int GeneralizedNodeConsistency(problem *p,restoreStruct *r)
{
	int c,l,conta;
	cost_WM min;
	for(c=0;c<p->totalClauses;c++)
	{
		if(p->clauses[c].elim==FALSE && p->clauses[c].GenLC==TRUE && p->clauses[c].weight>=p->UB && p->clauses[c].literalsWithoutAssign>2)
		{
			min=p->UB;
			conta=0;
			for(l=0;l<p->clauses[c].literalsTotal;l++)
			{
				if(p->Result[p->clauses[c].TLiterals[l].idVar]==NO_VALUE && p->ArrayOfDecision[p->clauses[c].TLiterals[l].idVar].not_considered_values==2)
				{
					if(p->UnaryCosts[p->clauses[c].TLiterals[l].idVar][p->clauses[c].TLiterals[l].boolValue]>0)
					{
						min=minV(min,p->UnaryCosts[p->clauses[c].TLiterals[l].idVar][p->clauses[c].TLiterals[l].boolValue]);
						conta++;
					}
				}
			}
			if(conta==p->clauses[c].literalsWithoutAssign)
			{
				//printf("c Node Consistency %d\n",c);
				p->LB=p->LB+min;
				
				if(p->LB>=p->UB) return FALSE;
	
				for(l=0;l<p->clauses[c].literalsTotal;l++)
				{
					p->UnaryCosts[p->clauses[c].TLiterals[l].idVar][p->clauses[c].TLiterals[l].boolValue]-=min;
					insertElement(&r->NCList,getNewNode3(&p->restoreList,p->clauses[c].TLiterals[l].idVar,p->clauses[c].TLiterals[l].boolValue,min));
				}

			}
		}
	}
	if(p->LB>=p->UB) return FALSE;
	if (pruneVariables(p,r)==FALSE) return FALSE;
	return TRUE;
}


/* LOCAL CONSISTENCY ENFORCE */

int WM_NC(problem *p,restoreStruct *r)
{
#if (LC_CHECK==1)
	checkNodeConsistency(p);
#endif
	return TRUE;
}

int WM_AC3(problem *p,restoreStruct *r)
{
	return executeAC3(p,r);
}


int WM_DAC(problem *p,restoreStruct *r)
{
	return executeDAC(p,r);
}


int WM_FDAC(problem *p,restoreStruct *r)
{
	return executeFDAC(p,r);
}

int WM_EDAC(problem *p,restoreStruct *r)
{
	return executeEDAC(p,r);
}

int WM_EDAC_PIC(problem *p,restoreStruct *r)
{
	return executeEDAC_PIC(p,r);
}

func_t LcFuncWM[7] = {WM_NC,WM_AC3,WM_DAC,WM_FDAC,WM_EDAC,WM_EDAC_PIC};
