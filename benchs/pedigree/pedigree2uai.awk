
# goal: convert a pedigree in pre format into MPE in ergo UAI format with additional evidence file

# usage: awk -f pedigree2uai.awk pedigree.pre [number of alleles] [genotyping error rate] [alleleprior]
# create two files: pedigree.pre.uai and pedigree.pre.uai.evid

# example: awk -f pedigree2uai.awk eye.pre 6 0.05 0
# create two files: eye.pre.uai and eye.pre.uai.evid

# Prior on allele frequency distribution:
# alleleprior = 0 : uniform allele probability distribution
# alleleprior = 1 : allele probability distribution read from pedigree data

# warning! allele numbers greater than "number of alleles" parameter are renamed starting from 1, following the order of occurence of each allele in the file

function min(x,y) {
    if (x < y) return(x); 
    else return(y);
}

function max(x,y) {
    if (x > y) return(x); 
    else return(y);
}

BEGIN {
	error = 0;
	if (ARGC != 5) {print "Wrong command line! Usage: awk -f pedigree2uai.awk pedigree.pre [number of alleles] [genotyping error rate] [alleleprior]"; error=1; exit;}
	uaifilename = ARGV[1] ".uai";
	evidfilename = ARGV[1] ".uai.evid";
    nballele = ARGV[2];
	if (nballele <= 0) {print "Wrong number of alleles (must be >0)!!!";  error=1; exit;}
    ARGV[2] = "";
    errorg = ARGV[3];
    ARGV[3] = "";
    alleleprior = ARGV[4];
    ARGV[4] = "";
#    printf("/* NBALLELE=%d */\n", nballele);
    domsize = nballele * (nballele + 1) / 2;
    mendel = "";
    mendelsize = 0;	    
	allele = 0;
    for (i=1; i<= nballele; i++) {
	for (j=i; j <= nballele; j++) {
		allelenum[i "," j] = allele;
		allelenum[j "," i] = allele;
		allele++;
	    for (k=1; k <= nballele; k++) {
		for (l=k; l <= nballele; l++) {
		    if (i <= k) a = i "/" k;
		    else a = k "/" i;
		    if (i <= l) b = i "/" l;
		    else b = l "/" i;
		    if (j <= k) c = j "/" k;
		    else c = k "/" j;
		    if (j <= l) d = j "/" l;
		    else d = l "/" j;
		    pa = 0.25;
		    pb = 0.25;
		    pc = 0.25;
		    pd = 0.25;
		    ison = 0;
		    for (m=1; m <= nballele; m++) {
			for (n=m; n <= nballele; n++) {
			    ison++;
			    p = 0;
			    e = m "/" n;
			    if (e == a) p += pa;
			    if (e == b) p += pb;
			    if (e == c) p += pc;
			    if (e == d) p += pd;
			    if (ison==1) mendel = mendel "" p;
			    else mendel = mendel " " p;
			    mendelsize++;
			}
		    }
		    mendel = mendel "\n";
		}
	    }
	}
    }
#    print mendel;
#    print mendelsize;

    num = 0;
    genotyping = 0;
	name[0] = -1;
	invname[0] = -1;
	alleles[0] = 0;
    allele = 0;
}

NF==7 {
    num++;
    name[num] = $2;
	invname[$2] = num-1;
    pedigree[num] = $1;
    father[num] = $3;
    mother[num] = $4;
    sex[num] = $5;
	if ($6 > nballele) {
		if (!($6 in alleles)) {
			allele++;
			alleles[$6] = allele;
		}
		$6 = alleles[$6];
	}
	if ($7 > nballele) {
		if (!($7 in alleles)) {
			allele++;
			alleles[$7] = allele;
		}
		$7 = alleles[$7];
	}
	if ($6 > nballele || $7 > nballele) {print "Wrong allele number " $6,$7 " (must be inside [1," nballele "] for individual " $2;  error=1; exit;} 
	if (($6 == 0 && $7 > 0) || ($6 > 0 && $7==0)) {print "Wrong individual half-typed " $2;  error=1; exit;} 
    if ($6 <= $7) {
		allele1[num] = $6;
		allele2[num] = $7;
    } else {
		allele1[num] = $7;
		allele2[num] = $6;
    }
    Freq[allele1[num]]++;
    Freq[allele2[num]]++;
    if (allele1[num] > 0) genotyping++;
}

END {
	if (!error) {
#		for (f in Freq) {print f,Freq[f],genotyping*2,Freq[f]/genotyping/2;}
		print "BAYES" > uaifilename;
		print num + genotyping >> uaifilename;
		printf("%d", domsize) >> uaifilename;
		for (i=2; i<=num; i++) {
			printf(" %d", domsize) >> uaifilename;
		}
		for (; i<=num + genotyping; i++) {
			printf(" %d", domsize) >> uaifilename;
		}
		print "" >> uaifilename;
		print num + genotyping >> uaifilename;
		for (i=1; i<=num; i++) {
			if (!(father[i] in invname) || !(mother[i] in invname)) {print "Undefined parents of individual " name[i];  exit;} 
			if (father[i]!=0 && mother[i]!=0) {
				print 3, invname[father[i]], invname[mother[i]], invname[name[i]] >> uaifilename;
			} else if (father[i]==0 && mother[i]==0) {
				print 1, invname[name[i]] >> uaifilename;
			} else {
				if (father[i]!=0) print 2, invname[father[i]], invname[name[i]] >> uaifilename;
				else print 2, invname[mother[i]], invname[name[i]] >> uaifilename;
			}
		}
		genotyping = 0;
		for (i=1; i<=num; i++) {
			if (allele1[i] > 0) {
				print 2,invname[name[i]],num+genotyping >> uaifilename;
				genotyping++;
			}
		}
		print "" >> uaifilename;
		for (i=1; i<=num; i++) {
			if (father[i]!=0 && mother[i]!=0) {
				print mendelsize >> uaifilename;
				print mendel >> uaifilename;
			} else if (father[i]==0 && mother[i]==0) {
				print domsize >> uaifilename;
				for (x=1;x<= nballele; x++) {
					for (y=x; y <= nballele; y++) {
						if (!(x==1&&y==1)) printf(" ") >> uaifilename;
						printf("%g", (alleleprior==0)?(((x!=y)?2.:1.)/(nballele * nballele)):(((x!=y)?2.:1.) * Freq[x] * Freq[y] / (genotyping * genotyping * 4))) >> uaifilename;
					}
				}
				print "" >> uaifilename;
				print "" >> uaifilename;
			} else {
				print domsize * domsize >> uaifilename;
				for (k=1; k<=nballele; k++) {
					for (l=k; l<=nballele; l++) {
						for (m=1; m <= nballele; m++) {
							for (n=m; n <= nballele; n++) {
								p = 0; 	
								if(m==k || m==l || n==k || n==l) {
									if(k == l)  p += 1. / nballele;
									else 		p += 1. / (nballele + nballele - 1); 
								}
								if (!(m==1&&n==1)) printf(" ") >> uaifilename;
								printf("%g", p) >> uaifilename;
							}
						}
						print "" >> uaifilename;
					}
				}
				print "" >> uaifilename;
			}
		}
		for (i=1; i<=num; i++) {
			if (allele1[i] > 0) {
				print domsize * domsize >> uaifilename;
				for (k=1; k <= nballele; k++) {
					for (l=k; l <= nballele; l++) {
						for (m=1; m <= nballele; m++) {
							for (n=m; n <= nballele; n++) {
								if (!(m==1&&n==1)) printf(" ") >> uaifilename;
								if (k "" l == m "" n) printf("%g", (1 - errorg)) >> uaifilename;
								else printf("%g", errorg / (domsize - 1)) >> uaifilename;
							}
						}
						print "" >> uaifilename;
					}
				}
				print "" >> uaifilename;
			}
		}

		print genotyping > evidfilename;
		for (i=1; i<=num; i++) {
			if (allele1[i] > 0) {
				print invname[name[i]], allelenum[allele1[i] "," allele2[i]] >> evidfilename;
			}
		}
	}
}
