/*
 * Purpose: Implementation of astar search, using the global structures for
 * bucket elimination.
 * 
 * author Paul Maier
 *
 */
#include <string.h>
#include "DAsearch.h"
#include "heap.h"
#include "domainAbstractionBE.h"

const short no_solution = -1;
const full_assignment_t NO_SOLUTION = &no_solution;

// max. computed heuristic value during a search 
int MaxH = BOTTOM;

int cmpAssignments(void *p1, void *p2) {
    search_assignment_t *at1 = (search_assignment_t *)p1;
    search_assignment_t *at2 = (search_assignment_t *)p2;
        
    return at1->estimate - at2->estimate;
}

void expand(heap *pQ, astar_costfunc_t deltaG, astar_costfunc_t H,
        bucket *buckets, search_assignment_t *at) {
    static int fullAssignmentSize = 0;

    if (fullAssignmentSize == 0) {
    	// compute max. size of a single assignment
    	fullAssignmentSize = NVar*sizeof(NVar);
    }
    
	int i = 0;
    int nextVar = 0;
    cost_t g = 0, h = 0, f = 0;
    short posVar[NVar];
    
    
    for (i = 0; i < NVar; ++i) {
        // look for the earliest variable which is unassigned: this is the 
        // next variable to instantiate
        if (at->values[i] == -1) {
            nextVar = i;
            break;
        }
    }
        
    // check Queue size against hard memory bound
    if (pQ->cp*fullAssignmentSize > HARD_MEM_BOUND) {
        printf("Queue hit the hard size bound of %d Bytes\n", HARD_MEM_BOUND);
        printf("No. elements in queue: %d\n", pQ->cp);
        abort();
    }

    /*
     * About posVar:
     * This array maps variable indicies to indicies in an arbitrary assignment
     * array. E.g., say you have problem variables 0,1,2,3 and you have an
     * assignment to variables 1 and 2, which you want to evaluate. Said 
     * assignment is stored in a length 2 array *at*. Now how do you know which 
     * array element corresponds to which variable? That's posVar's job: 
     * For variable i posVar returns the correct index into the assignment array
     * at, that is, you can access the value of var. 1 in this assignment through
     * at[posVar[1]]. Accordingly, at[posVar[2]] returns the value of var. 2 in 
     * assignment at.
     * 
     * Here within A* search however, all partial assignments are stored as 
     * fixed length arrays and their indicies correspond to the variable 
     * indicies. Hence, posVar is simply the identity mapping. It's still needed
     * because the evaluation functions (see evalua() in be.c, for example)
     * require such a mapping.
     */
    //array which maps the indicies to themselves -> ID TODO: optimize?
    for (i = 0; i < NVar; ++i) {
        posVar[i] = i;
    }
    int val = 0;
    for (val = 0; val < DomainSize[nextVar]; ++val) {
        search_assignment_t *newAt = mycalloc(1, sizeof(search_assignment_t));
        newAt->values = mycalloc(NVar, sizeof(short));
        memcpy(newAt->values, at->values, NVar*sizeof(short));
        newAt->values[nextVar] = val; 

        g = at->cost + deltaG(buckets[nextVar], newAt->values, posVar);
        h = H(buckets[nextVar], newAt->values, posVar);
        MaxH = MAX(h, MaxH);
        f = g + h;

        if (g > Top) g = Top;
        if (f > Top) f = Top;
        newAt->cost = g;
        newAt->estimate = f;
        
        HeapInsert(pQ, (void *)newAt);
    }
}

/**
 * Test if the passed assignment is a goal assigment. Right now this is true if
 * all variables in the assignment are instantiated. Variables which are not yet
 * instantiated must have the value -1. 
 */
bool_t isGoalAssignment(search_assignment_t *at) {
    int i = 0;
    for (i = 0; i < NVar; ++i) if (at->values[i] == -1) return FALSE;

    return TRUE;
}
/**
 * Generate and return the optimal solution from the buckets. The solution is 
 * allocated by the function and the pointer is given to the caller (i.e. she
 * doesn't borrow it).
 * 
 * This function implements A* search
 * 
 * PRE: buckets is an array of buckets which contains at least NVar buckets
 */
full_assignment_t generateSolutionAStar(bucket *buckets, astar_costfunc_t deltaG,
        astar_costfunc_t H) {
    full_assignment_t solution = 
        (full_assignment_t) mycalloc(NVar, sizeof(short));
    heap Q;
    search_assignment_t *initialAssignment = 
        mycalloc(1, sizeof(search_assignment_t)); //TODO: free this somewhere!
    initialAssignment->values = mycalloc(NVar, sizeof(short));
    initialAssignment->cost = 0;
    initialAssignment->estimate = 0;
    
    MaxH = BOTTOM;
    
    // initialize the values of the initial assignment with -1
    int i = 0;
    for (i = 0; i < NVar; i++) {
        initialAssignment->values[i] = -1;
    }
    
    HeapCreate(&Q, PQUEUE_SIZE, cmpAssignments);
    HeapInsert(&Q, (void *)initialAssignment);
    
    
    search_assignment_t *currentAssignmt = NULL;
    while( (currentAssignmt = HeapLookup(&Q)) != NULL ) {
        //currentAssignmt = (search_assignment_t *)HeapLookup(&Q);
        HeapDelete(&Q);
        
        if (Verbose > 1) {
            printf("\ncur. At. cost/est: %d/%d\n", (int) currentAssignmt->cost,
                    (int) currentAssignmt->estimate);
            int v = 0;
            for (v = 0; v < NVar; ++v) {
                printf("%d ", currentAssignmt->values[v]);
            }
            printf("\n");
        }
        
        if (isGoalAssignment(currentAssignmt)) {

            if (Verbose) {
//              printQ(&Q);
                printf("\n");
                printf("cost/est. of solution: %d/%d\n",
						(int)currentAssignmt->cost,
						(int)currentAssignmt->estimate);
                printf("queue size: %d elements\n", Q.cp);
                printf("MaxH: %d\n", MaxH);
            }
            memcpy(solution, currentAssignmt->values, NVar*sizeof(short));
            return solution;
        }
        expand(&Q, deltaG, H, buckets, currentAssignmt);
    }
    
    HeapDestroy(&Q);
    free(initialAssignment);
    free(solution);
    return NO_SOLUTION;
}





/**
 * This recursive function expands nodes in a depth first manner. *cost* 
 * indicates the cost for the partial assignment up to now, and thus the minimum
 * cost to be expected of all childs of the current node. *bab->top* is the cost
 * of the last found solution. Only childs with an expected cost less than top
 * are expanded. If a new solution is found, top is set to the cost of this 
 * solution. This way, top is always the cost of the best solution found so 
 * far. 
 * 
 * Considering variable ordering, this algorithm expands the variables in the 
 * order that they appear in the global array *DomainSize*, i.e. domain size 
 * *DomainSize[0]* is the first variable, *DomainSize[1]* the second and so 
 * forth. The variables may have been reordered prior to calling this algorithm
 * by MBE or BEDA (which uses the same ordering heuristic as MBE). In this case,
 * the variable values of the solution need to be mapped back onto the original
 * variable indicies (which appearantly the structs *estruct_a_fich* and 
 * *fich_a_estruct* in be_format.c are for).
 */
void recursiveExpand(bab_solver_t bab, int cost, int nextVar,
        full_assignment_t partialAssignment) {
    
    if (nextVar == NVar) { 
        //all variables assigned -> we got a solution
        //nextVar coincides with the number of assigned variables
        
        //if the solution is better than the current one, store it
        if (cost < bab->top) {
            bab->top = cost;
            memcpy(bab->currentBestSolution, partialAssignment, NVar
                    *sizeof(partialAssignment[0]));
        }
    } else {
        int val;
        for (val = 0; val < DomainSize[nextVar]; val++) {

            partialAssignment[nextVar] = val;
            int newCost = cost+ bab->deltaG(bab->buckets[nextVar],
                    partialAssignment, bab->posVar);
            int bottom = newCost + bab->H(bab->buckets[nextVar],
                    partialAssignment, bab->posVar);
            
            if (bottom < bab->top) {
                recursiveExpand(bab, newCost, nextVar+1, partialAssignment);
            }
        }
    }
}



full_assignment_t generateSolutionBAB(bucket *buckets, int top,
        astar_costfunc_t deltaG, astar_costfunc_t H) {
    int i = 0;
    short posVar[NVar];
    struct bab_solver_t_ bab;
    full_assignment_t partialAssignment = mycalloc(NVar, sizeof(short));

    for (i = 0; i < NVar; ++i) {
        posVar[i] = i;
    }

    bab.currentBestSolution = mycalloc(NVar, sizeof(short));
    bab.buckets = buckets;
    bab.posVar = posVar;
    bab.top = top;
    bab.deltaG = deltaG;
    bab.H = H;

    recursiveExpand(&bab, BOTTOM, 0, partialAssignment);
    free(partialAssignment);
    return bab.currentBestSolution;
}


/****************************** helpers ***************************************/


void printQ(heap *pQ) {
    printf("Contents of Q:\n");
    int i = 0;
    for (i = 0; i < pQ->cp; ++i) {
        search_assignment_t *at = (search_assignment_t*)pQ->ap[i];
        printf("cost: %d  est: %d\n", (int)at->cost, (int)at->estimate);
        int v = 0;
        for (v = 0; v < NVar; ++v) {
            printf("%d ", at->values[v]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
}
