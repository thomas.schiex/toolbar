#!/bin/sh

# usage: bktree.sh k s h d t(%) nbinst

AWKPATH=../translators
n=1
while expr $n \<= $6 ; do
  rndkbtree kbtree$1_$2_$3_$4_$5_$n.ds $1 $2 $3 $4 $5 1
  awk -f ../translators/ds2cpi.awk kbtree$1_$2_$3_$4_$5_$n.ds | awk -f ../translators/cp2wcsp.awk > kbtree$1_$2_$3_$4_$5_$n.wcsp
  n=`expr $n + 1`
done
