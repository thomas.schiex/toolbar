/* ---------------------------------------------------------------------

  TOOLBAR - A constraint optimization toolbox



  File: tbstream.c

  $Id$



  Authors: 

     Simon de Givry (1), Federico Heras,(2) 

     Javier Larrosa (2), Thomas Schiex (1)



     (1) INRA, Biometry and AI Lab. Toulouse, France

     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain



  Copyright 2003 by the authors.

  This file is distributed under the Artistic License (See LICENSE file)

  --------------------------------------------------------------------- */

#include <stdio.h>

#include <stdlib.h>

#include "wcsp.h"

#include "solver.h"


myStream * createStream()
{
	myStream *s;
	s = (myStream *) malloc (sizeof(myStream));
	s->str_content = (int *) malloc(sizeof(int)*NVar);
	s->str_stack= (int *) malloc(sizeof(int)*NVar);

	s->str_n_elem=0;
	s->str_head=0;
	return s;
}

void destroyStream(myStream *s)
{
	free(s->str_content);
	free(s->str_stack);
	free(s);
}

int stream_size(myStream *s){return(s->str_n_elem);}



int stream_empty(myStream *s){return(s->str_n_elem==0);}


int stream_pop_last_in(myStream *s)
{

	int temp;

	s->str_n_elem--;

	temp=s->str_stack[(s->str_head+s->str_n_elem)%NVar];

	s->str_content[temp]=FALSE;

	return(temp);

}



int stream_pop_first_in(myStream *s)

{

	int temp;

	temp=s->str_stack[s->str_head];

	s->str_n_elem--;

	s->str_content[temp]=FALSE;

	s->str_head=(s->str_head+1)%NVar;

	return(temp);

}



int stream_pop_random(myStream *s)

{

	int elem,i,temp;

	elem=rand()%s->str_n_elem;

	temp=s->str_stack[elem];

	s->str_content[temp]=FALSE;

	for(i=elem;i<s->str_n_elem-1;i++) s->str_stack[i]=s->str_stack[i+1];

	s->str_n_elem--;

	return(temp);

}





int stream_pop_max(myStream *s)
{
	int elem,i,j,k;
	elem=s->str_stack[0];
	j=0;
	for(i=1;i<s->str_n_elem;i++)
	{
		k=s->str_stack[i];
		if(order[elem]<order[k])
		{elem=k;j=i;}
	}
	s->str_content[elem]=FALSE;
	for(i=j;i<s->str_n_elem-1;i++)s->str_stack[i]=s->str_stack[i+1];
	s->str_n_elem--;
	return(elem);
}

int stream_pop_min(myStream *s)
{
	int elem,i,j,k;
	elem=s->str_stack[0];
	j=0;
	for(i=1;i<s->str_n_elem;i++)
	{
		k=s->str_stack[i];
		if(order[elem]>order[k])
		{elem=k;j=i;}
	}
	s->str_content[elem]=FALSE;
	for(i=j;i<s->str_n_elem-1;i++)s->str_stack[i]=s->str_stack[i+1];
	s->str_n_elem--;
	return(elem);
}








int stream_pop_min_dom(myStream *s)

{

	int elem,dom_elem,i,j,k;

	elem=s->str_stack[0];

	j=0;

	dom_elem=List_n_of_elem(&feasible_values[elem]);

	for(i=1;i<s->str_n_elem;i++)

	{

		k=s->str_stack[i];

		if(dom_elem>List_n_of_elem(&feasible_values[k]))

		{elem=k;j=i;dom_elem=List_n_of_elem(&feasible_values[k]);}

	}

	s->str_content[elem]=FALSE;

	for(i=j;i<s->str_n_elem-1;i++)s->str_stack[i]=s->str_stack[i+1];

	s->str_n_elem--;

	return(elem);

}



int stream_pop_max_dom(myStream *s)

{

	int elem,dom_elem,i,j,k;

	elem=s->str_stack[0];

	j=0;

	dom_elem=List_n_of_elem(&feasible_values[elem]);

	for(i=1;i<s->str_n_elem;i++)

	{

		k=s->str_stack[i];

		if(dom_elem<List_n_of_elem(&feasible_values[k]))

		{elem=k;j=i;dom_elem=List_n_of_elem(&feasible_values[k]);}

	}

	s->str_content[elem]=FALSE;

	for(i=j;i<s->str_n_elem-1;i++)s->str_stack[i]=s->str_stack[i+1];

	s->str_n_elem--;

	return(elem);

}





void stream_ini(myStream *s)
{

	int i;

	for(i=0;i<NVar;i++)s->str_content[i]=FALSE;

	s->str_n_elem=0;

	s->str_head=0;

}



void stream_push(myStream *s,int i)

{

	if(!s->str_content[i])

	{

		s->str_content[i]=TRUE;

		s->str_stack[(s->str_head+s->str_n_elem)%NVar]=i;

		s->str_n_elem++;

	}

}

void fillStream(myStream *s)
{
	int i;
	//stream_ini(s);
	for(i=0;i<NVar;i++)
	{
		stream_push(s,i);
	}
}



/***********************************************/

/* min heap */

heap *createHeap()
{
	heap *h;
	h = (heap *) malloc (sizeof(heap));
	h->stream2=(int *) malloc(sizeof(int)*NVar);
	h->stream2_n=0;
	return h;
}

void destroyHeap(heap *h)
{
	free(h->stream2);
	free(h);
}

void heap_push(heap *h, int i)
{
	if(h->stream2[order[i]]==-1){h->stream2[order[i]]=i; h->stream2_n++;}
}

int heap_existsValue(heap *h, int i)
{
	if(h->stream2[order[i]]==-1) return FALSE;
	else return TRUE;
}


void heap_show(heap *h)
{
	int i;
	printf("\nHEAP:");
	for(i=0;i<NVar;i++)
	{
		if(h->stream2[i]!=-1)
		{
			printf(" %d ",h->stream2[i]);
		}
	}
	printf("\n");

}

int heap_pop(heap *h)
{

	int i,k;
	for(i=0;i<NVar;i++)
	{
		if(h->stream2[i]!=-1)
		{
			h->stream2_n--;
			k=h->stream2[i];
			h->stream2[i]=-1;
			return(k);
		}
	}
	printf("error stream2 pop\n");
	return(-1);
}

int heap_pop2(heap *h)

{

	int i,k;
	for(i=NVar-1;i>=0;i--)
	{
		if(h->stream2[i]!=-1)
		{
			h->stream2_n--;
			k=h->stream2[i];
			h->stream2[i]=-1;
			return(k);
		}
	}
	printf("error streamR pop\n");
	return(-1);
}




int heap_empty(heap *h)

{return(h->stream2_n==0);}



void heap_ini(heap *h)
{
	int i;
	for(i=0;i<NVar;i++) h->stream2[i]=-1;
	h->stream2_n=0;
}


void fillHeap(heap *h)
{
	int i;
	heap_ini(h);
	for(i=0;i<NVar;i++)
	{
		heap_push(h,i);
	}
}


myStream ** createStreamCopies()
{
	int i;
	myStream **s;
	s = (myStream **) malloc (sizeof(myStream *)*NVar);
	for(i=0;i<NVar;i++)
	{
		s[i]=createStream();
	}
	return s;
}

void destroyStreamCopies(myStream **s)
{
	int i;
	for(i=0;i<NVar;i++)
	{
		destroyStream(s[i]);
	}
	free(s);
}
