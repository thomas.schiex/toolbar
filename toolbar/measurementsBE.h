#ifndef MEASUREMENTSBE_H_
#define MEASUREMENTSBE_H_


// counter for the number of messages which are computed during bucket elimination
extern int NComputedMessages; 

/*
 * This array stores the message size for each bucket, computed via 
 * 
 * nTuples = dx1 * dx2 * ... * dxv
 * 
 * with x1, ..., xv the variables in the scope of the message. Note that these 
 * are the sizes of the non-abstracted messages. If abstraction is applied 
 * (which is the case if nTuples > maxTuples) the actual size is smaller.
 */   
extern int *BucketMsgSizes;
extern int *BucketAbstractMsgSizes;
extern int **MiniBucketMsgSizes;

int computeBucketMsgSize(bucket *b, int bucketVar);
void initMeasurementStructures();
void freeMeasurementStructures();

#endif /*MEASUREMENTSBE_H_*/
