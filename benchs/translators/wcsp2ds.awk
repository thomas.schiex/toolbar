
# Translator from wcsp format to ds format

# Usage: awk -f wcsp2ds.awk problem.wcsp > problem.ds

# See the file ds_format.txt for a description of the ds format

# Note: the awk variable TABUMODE can be modified in order to try to reduce the size of the result
# TABUMODE = 0 (default value):  constraints are expressed by their list of permitted tuples
# TABUMODE = 1                :  constraints are expressed by their list of forbidden tuples

function min(x,y) {
    if (x < y) return(x); 
    else return(y);
}

function max(x,y) {
    if (x > y) return(x); 
    else return(y);
}

function perror(i,message) {
  print message;
  print "line " NR ": " $0;
  error = i;
  exit(i);
}

BEGIN {
    TABUMODE = 0; # default value is false (0)
    RS = "@"; # special character that should never appear in the wcsp file
    Bottom = 0;
    NbConstr = 0;
}

{
 # generate problem name
 name = $1;
 nbvar = $2;
 nbval = $3;
 nbconstr = $4;
 ub = $5;
 if (TABUMODE) {
   print name,"valued_tabu";
 } else {
   print name,"valued";
 }
 # generate variables and domains
 for (i=0; i<nbvar; i++) {
   domsize[i] = $(6+i);
   printf("%d", i);
   for (a=0; a<domsize[i]; a++) {
     printf(" %d", a);
   }
   print "";
 }
 # generate constraints
 pos = 6 + nbvar;
 while (pos <= NF) {
   arity = $pos;
   defcost = min(ub, $(pos + arity + 1));
   nbtuple = $(pos + arity + 2);
   if (arity == 0 || nbtuple == 0) {
     Bottom = min(ub, Bottom + defcost);
   } else {
     cartesianproduct = 1;
     for (k=1; k<=arity; k++) {
       cartesianproduct *= domsize[$(pos + k)];
     }
     delete tuplecosts;
     for (t=0; t<cartesianproduct; t++) {
       tuplecosts[t] = defcost;
     }
     mincost = defcost;
     secondmincost = 1e99;
     for (p = pos + arity + 3; p < pos + arity + 3 + nbtuple * (arity + 1); p += arity + 1) {
       cost = min(ub, $(p + arity));
       costs[cost] = 1;
       myindex = $p;
       for (k=1; k<arity; k++) {
	 myindex *= domsize[$(pos + k + 1)];
	 myindex += $(p + k);
       }
       tuplecosts[myindex] = cost;
       mincost = min(mincost, cost);
       if (cost != mincost) {
	 secondmincost = min(secondmincost, cost);
       }
     }
     if (defcost != mincost) {
       secondmincost = min(secondmincost, defcost);
     }
     while (secondmincost != 1e99) {
       print "#";
       printf("c%d %d %s", NbConstr, secondmincost - mincost,"extension");
       NbConstr++;
       for (k=1; k<=arity; k++) {
	 printf(" %d", $(pos + k));
       }
       print "";
       newmincost = 1e99;
       newsecondmincost = 1e99;
       for (t=0; t<cartesianproduct; t++) {
	 if ((!TABUMODE && tuplecosts[t] == mincost) ||
	     (TABUMODE && tuplecosts[t] > mincost)) {
	   tuple = "";
	   myindex = t;
	   for (k=arity; k>1; k--) {
	     val = (myindex % domsize[$(pos + k)]);
	     tuple = val " " tuple;
	     myindex = (myindex - val) / domsize[$(pos + k)];
	   }
	   print myindex " " tuple;
	 }
	 if (tuplecosts[t] == mincost) {
	   tuplecosts[t] -= mincost;
	 } else {
	   tuplecosts[t] -= secondmincost;
	 }
	 newmincost = min(newmincost, tuplecosts[t]);
	 if (newmincost != tuplecosts[t]) {
	   newsecondmincost = min(newsecondmincost, tuplecosts[t]);
	 }
       }
       Bottom = min(ub, Bottom + mincost);
       mincost = newmincost;
       secondmincost = newsecondmincost;
     }
     if (mincost != 1e99) Bottom = min(ub, Bottom + mincost);   
   }
   pos += arity + 3 + nbtuple * (arity + 1);
 }
 # generate Bottom zero arity constraint
 # by adding a unary constraint on the first variable with weight equal to Bottom
 if (Bottom > 0) {
   print "#";
   print "_Bottom_",Bottom,"extension",0;
   if (TABUMODE) {
     for (a=0; a<domsize[0]; a++) {
       print a;
     }
   }
 }
 # generate instancy block (all the constraints are included)
 printf("% 0");
 for (c=0; c<NbConstr; c++) {
   printf(" c%d", c);
 }
 if (Bottom > 0) printf(" _Bottom_");
 print "";
}
