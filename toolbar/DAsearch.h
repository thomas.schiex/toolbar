#ifndef ASTAR_H_
#define ASTAR_H_

#include <stdlib.h>
#include <stdio.h>
#include "wcsp.h"
#include "be.h"
#include "domainAbstractionBE.h"

// hard upper bound for the queue size: if this is hit, the program is aborted
#define HARD_QUEUE_BOUND 10000000  
#define HARD_MEM_BOUND 1500000000  

typedef cost_t (*astar_costfunc_t)(bucket, full_assignment_t, short *);

// structure to store data for a branch-and-bound run
typedef struct bab_solver_t_ {
    full_assignment_t currentBestSolution;
    int top;
    astar_costfunc_t deltaG;
    astar_costfunc_t H;
    bucket *buckets;
    short *posVar;
} * bab_solver_t;


extern const full_assignment_t NO_SOLUTION;
extern int MaxH;

full_assignment_t generateSolutionAStar(bucket *buckets, astar_costfunc_t deltaG,
        astar_costfunc_t H);
full_assignment_t generateSolutionBAB(bucket *buckets, int top,
        astar_costfunc_t deltaG, astar_costfunc_t H);
#endif /*ASTAR_H_*/
