#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "wcsp.h"
#include "be.h"
#include "CuTest.h"

/*
 * Purpose: This module provides a number of variables and functions to store
 * and compute measurements for MBE and BEDA, such as the number of messages
 * generated during bucket elimination. 
 * 
 * CVS-ID: $ID: $
 * SVN-ID: $ID $
 * 
 * author maierpa
 *
 */


// counter for the number of messages which are computed during bucket elimination
int NComputedMessages = 0;

/*
 * This array stores the message size for each bucket, computed via 
 * 
 * nTuples = dx1 * dx2 * ... * dxv
 * 
 * with x1, ..., xv the variables in the scope of the message. Note that these 
 * are the sizes of the non-abstracted messages. If abstraction is applied 
 * (which is the case if nTuples > maxTuples) the actual size is smaller.
 */   
int *BucketMsgSizes = NULL;
int *BucketAbstractMsgSizes = NULL;
int **MiniBucketMsgSizes = NULL;


int computeBucketMsgSize(bucket *b, int bucketVar) {
	nodofunc *node;
	bool_t original = FALSE;
	int msgSize = 1;
	double logMsgSize = 0.0;
	double maxLog = log2i(INT_MAX);
	bool_t *inScope;

	
	if ((node = b->primera_func_original)!=NULL) { 
		original=TRUE;
	} else {
		node = b->primera_func_anadida;
		original=FALSE;
	}

	if (node == NULL) {
		//bucket is empty, msg size is 0
		return 0;
	}
	
	inScope = mycalloc (NVar, sizeof(bool_t));	
	
	while (node!=NULL) {
		int i;
		for (i = 0; i<node->aridad; i++) {
			if (!inScope[node->vars[i]] && node->vars[i] != bucketVar) {
				
				logMsgSize = logMsgSize < 0.001 ? log2i(DOMSIZE(node->vars[i])) :
							 logMsgSize + log2i(DOMSIZE(node->vars[i]));
				
				if (logMsgSize > maxLog) {
					return INT_MAX;
				} else {
					msgSize = msgSize == 0 ? DOMSIZE(node->vars[i]) : 
											 msgSize * DOMSIZE(node->vars[i]);
				}
				inScope[node->vars[i]] = TRUE;
			}
		}
		if (node->sig_func==NULL && original) {
			node=b->primera_func_anadida;
			original=FALSE;
		} else node=node->sig_func;
	}
	
	return msgSize;
}


void initMeasurementStructures() {
	NComputedMessages = 0;
	BucketMsgSizes = mycalloc(NVar, sizeof(int));
	BucketAbstractMsgSizes = mycalloc(NVar, sizeof(int));
	MiniBucketMsgSizes = mycalloc(NVar, sizeof(int *));
}

void freeMeasurementStructures() {
	if (BucketMsgSizes != NULL) free(BucketMsgSizes);
	if (BucketAbstractMsgSizes != NULL) free(BucketAbstractMsgSizes);
	
	if (MiniBucketMsgSizes != NULL) {
		int i = 0;
		for (; i < NVar; i++) {
			if (MiniBucketMsgSizes[i] != NULL)
				free(MiniBucketMsgSizes[i]);
		}
		
		free(MiniBucketMsgSizes); 
	}

	BucketMsgSizes = NULL;
	BucketAbstractMsgSizes = NULL;
	MiniBucketMsgSizes = NULL;
}

/*****************************************************************************
 * Test case functions.
 *****************************************************************************/

void TestComputeBucketMsgSize(CuTest *tc) {
	
	
	bucket b = {3, NULL, NULL, NULL};
	DOMSIZE(0) = 3;
	DOMSIZE(1) = 4;
	DOMSIZE(2) = 2;
	int msgSizeRef = DOMSIZE(0)*DOMSIZE(1);
	
	nodofunc origC01 = { .aridad = 2, .vars = NULL, .costs = NULL, .ntuples = 12,
			.despl_var = NULL, .sig_func = NULL };
	nodofunc origC12 = { .aridad = 2, .vars = NULL, .costs = NULL, .ntuples = 8,
			.despl_var = NULL, .sig_func = NULL };
	nodofunc addedC1 = { .aridad = 1, .vars = NULL, .costs = NULL, .ntuples = 4,
			.despl_var = NULL, .sig_func = NULL };
	
	short vars01[2] = {0, 1}; 
	origC01.vars = vars01;
	origC01.sig_func = &origC12;
	
	short vars12[2] = {1, 2};
	origC12.vars = vars12;
	
	short vars1[1] = {1};
	addedC1.vars = vars1;
	
	b.primera_func_original = &origC01;
	b.ultima_func_original = &origC12;
	b.primera_func_anadida = &addedC1;
	
	CuAssertIntEquals(tc, msgSizeRef, computeBucketMsgSize(&b, 2));
}
