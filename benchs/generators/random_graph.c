#include <stdio.h>
#include <stdlib.h>

#define MAX_TRIES 1000
#define TRUE 1
#define FALSE 0

// Random Connected Graph generator. If after 1000 tries it is not able to create a connected graph randomly,
// the output will be empty, else a random graph in dimacs format is returned.
// by Federico Heras (fheras@lsi.upc.edu) and Jordi Planes (jplanes@diei.udl.es).
// to execute in linux: ./random_graph [num_nodes] [num_edges] [seed] > output_file

// Data structures

typedef struct
{
	int a;
	int b;
} edge;

typedef struct
{
	int num_nodes;
	int num_edges;
	int **matrix;
	edge *arrayEdges;
	int *connec;
	int totalEdges;
	int remainEdges;
} problem;

/* Procedures to create and destroy data structures */

void createProblem(problem *p)
{
	int i,j,c;

	p->matrix=(int **)malloc(sizeof(int *)*p->num_nodes);
	if(p->matrix==NULL)
		exit(-1);
		
	p->totalEdges=p->num_nodes*p->num_nodes;
	
	p->arrayEdges=(edge *)malloc(sizeof(edge *)*p->totalEdges);
	if(p->arrayEdges==NULL)
		exit(-1);
		
	p->connec=(int *)malloc(sizeof(int *)*p->num_nodes);
	if(p->connec==NULL)
		exit(-1);

	c=0;
	for(i=0;i<p->num_nodes;i++) 
	{ 
		p->connec[i]=FALSE;
		
		p->matrix[i]=(int *)malloc(sizeof(int)*p->num_nodes);
		if(p->matrix[i]==NULL) exit(-1);

		for (j=i+1;j<p->num_nodes;j++)
		{
			p->matrix[i][j]=FALSE;
			p->arrayEdges[c].a=i;
			p->arrayEdges[c].b=j;
			c++;
		}

	}
	
	p->totalEdges=c;
	p->remainEdges=c;
}

void destroyProblem(problem *p)
{
	int k;
	
	for(k=0; k<p->num_nodes; k++) 
	{ 
		free(p->matrix[k]);
	}

	free(p->matrix);
	free(p->arrayEdges);
	free(p->connec);
}

/* Auxiliary functions */

void addEdge(problem *p,int x, int y)
{
	if ( x < y )
	{
		p->matrix[x][y]=TRUE;
	}
	else p->matrix[y][x]=TRUE;
} 


int edgeExists( problem *p, int x, int y )
{
	if ( x < y )
	{
		return p->matrix[x][y];
	}
	return p->matrix[y][x];
} 


int genRand(int max)
/* returns random number in range of 0 to max-1 */
{
   int n;
   n=rand()/(int)(((unsigned)RAND_MAX + 1) / max);  /* n is random number in range of 0 - max-1 */
   return(n);
}

int alreadyInList( int *list, int length, int element )
{

 int i;
 
 for( i = 0; i < length; i++ )
 {
   	if( list[ i ] == element )
   	{
   		return TRUE;
	}
 }
 
 return FALSE;
 
}

/* Main functions */

int verifyConnectivity(problem *p)
{
	int *nodes_to_visit = malloc( sizeof( int ) * p->num_nodes ), length = 0;
	int node, neighbour = p->arrayEdges[0].a;
	
	nodes_to_visit[ length++ ] = neighbour;
	for( node=0; node < length && length < p->num_nodes; node++ )
	{
		for( neighbour=0; neighbour<p->num_nodes; neighbour++ )
		{                
			if ( edgeExists( p, nodes_to_visit[node], neighbour ) && !alreadyInList( nodes_to_visit, length, neighbour ) )
			{
				nodes_to_visit[ length++ ] = neighbour;
			}
		}
	}
	free(nodes_to_visit);
	if(length==p->num_nodes) return TRUE;
	return FALSE;
} 


void generateGraph(problem *p)
{
	
	int i,iRnd;
	for(i=0;i<p->num_edges;i++) 
	{
		iRnd=genRand(p->remainEdges);
		
		addEdge(p,p->arrayEdges[iRnd].a,p->arrayEdges[iRnd].b);
				
		if(iRnd!=p->remainEdges-1);
		{			
			p->arrayEdges[iRnd].a=p->arrayEdges[p->remainEdges-1].a;
			p->arrayEdges[iRnd].b=p->arrayEdges[p->remainEdges-1].b;
		}
		
		p->remainEdges--;
	}
}

void writeGraph(problem *p)
{
	int i,j;
	printf("c Graph with %d nodes and %d edges, Max edges:%d\n",p->num_nodes,p->num_edges,p->totalEdges);
	printf("p edge %d %d\n",p->num_nodes,p->num_edges);
	
	for(i=0;i<p->num_nodes;i++) 
	{ 
		for (j=i+1;j<p->num_nodes;j++)
		{
			if(p->matrix[i][j]==TRUE)
			{
				printf("e %d %d\n",(i+1),(j+1));
			}
		}
	}
	
	
}

int generateProblem(problem *p,char **argv)
{
	int seed,c=0,vc;
	
	p->num_nodes = strtol(argv[1],NULL,0);
	
	p->num_edges = strtol(argv[2],NULL,0);
	
	seed = strtol(argv[3],NULL,0);
	
	srand(seed);
	
	do
	{
		
		createProblem(p);
		
		generateGraph(p);
		
		vc=verifyConnectivity(p);
		
		if (vc==TRUE)
		{
			printf("c Generated after %d tries\n",c+1);
			writeGraph(p);
		}
		
		destroyProblem(p);
		
		c++;
	}
	while (vc==FALSE && c<MAX_TRIES);
	
	return TRUE;

}

int main(int argc,char ** argv)
{
	problem p;
	
	if(argc!=4)
	{
		printf("\n Incorrect number of parameters. Usage: random_graph num_nodes num_edges seed \n");
		exit(-1);
	}
	
	generateProblem(&p,argv);
	
	return TRUE;
}
