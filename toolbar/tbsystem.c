/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbsystem.c
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
/* tbsystem.c
   contains codes which depends on the operating system */

// IN TBSYSTEM.H YOU MUST UNCOMMENT YOUR COMPILATION VARIABLE

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include "wcsp.h"
#include "tbsystem.h"

#ifdef WINDOWS
// WINDOWS IMPLEMENTATION

#define _WIN32_WINNT 0x0400

#include <windows.h>
#include <winbase.h>
#include <process.h> // Visual C++ 6.0 compile with: /MT /D "_X86_" /c


/* --------------------------------------------------------------------
// Random generator: seed initialisation.  If seed = 0, generates a
// random seed from pid and time else use the given seed
// -------------------------------------------------------------------- */
void SeedInit (int seed)
{
  unsigned int heure;
  
  if (!seed)
  {
    heure = (unsigned int) time(NULL);
    srand(heure);
  }
  else
    srand(seed);
}

/* --------------------------------------------------------------------
// Timer management functions
// -------------------------------------------------------------------- */
double cpuTime()
{
  return (double) (clock() / CLOCKS_PER_SEC);
}


static HANDLE thetimer;
static LARGE_INTEGER deadline;
static int tot;
static int acum;
static int act;

const int unitsPerSecond=10*1000*1000; // 100 nano seconds

unsigned __stdcall TF(void* arg) {
  HANDLE timerAux=(HANDLE) arg;
  int enlarge=0;

  while (TRUE)
  {
		WaitForSingleObject(timerAux,INFINITE);
		act=act+acum;
		if(act>tot)
		{
			if(Verbose)
			{
				printf("\nTime limit expired... Aborting...\n");
			}
			timeOut();
			exit(0);
		}
		else
		{
			if((act+acum)>tot)
				enlarge = (tot-act);
			else
				enlarge = (acum);

			deadline.QuadPart = -(enlarge * unitsPerSecond);

			SetWaitableTimer(thetimer,&deadline,1000,0,0,FALSE);

		}

  } // while

}

void timer(int t)
{
	if(t>0) // Only set the timer if user has specified a limit
	{
		tot=t;
		acum=12;
		act=0;
		thetimer = CreateWaitableTimer(NULL, FALSE, NULL);
		if (!thetimer)
		{
			printf("CreateWaitableTimer failed (%d)\n", GetLastError());
			exit(-1);
		}

		if(tot<acum) deadline.QuadPart = -(tot * unitsPerSecond);
		else
		{
			if(tot%acum==0) tot++;
			deadline.QuadPart = -(acum * unitsPerSecond);
		}

		if(!SetWaitableTimer(thetimer,&deadline,1000,0,0,FALSE))
		{
		  printf("\nUnexpected error...");
		  printf("SetWaitableTimer failed (%d)\n", GetLastError());
		}
		else
			_beginthreadex(0,0,TF,(void*) thetimer,0,0);
	}
}

void timerStop()
{
	printf("\nStop");
  CancelWaitableTimer(thetimer);
}

#endif

#ifdef UNIX 
// UNIX IMPLEMENTATION
#include <unistd.h> 
#include <sys/time.h>
#include <sys/times.h>

/* --------------------------------------------------------------------
// Random generator: seed initialisation.  If seed = 0, generates a
// random seed from pid and time else use the given seed
// -------------------------------------------------------------------- */
void SeedInit (int seed)
{
  unsigned int heure;
  
  if (!seed) {
    heure = (unsigned int) time(NULL);
    srand(heure * getpid());
  }
  else
    srand(seed);
}

/* --------------------------------------------------------------------
// Timer management functions
// -------------------------------------------------------------------- */

/* return CPU time in seconds */

double cpuTime()
{
  static struct tms buf;

  times(&buf);
  return ((double) (buf.tms_utime+buf.tms_stime+buf.tms_cutime+buf.tms_cstime)) / ((double) sysconf(_SC_CLK_TCK));
}


void timeout(int sig)
{
  if(Verbose)
	printf("\nTime limit expired... Aborting...\n");
  timeOut();
  exit(0);
}

static struct itimerval thetimer = { {0, 0}, {0, 0} };

/* set a timer (in seconds) */
void timer(int t)
{
 signal(SIGVTALRM, timeout);
 thetimer.it_interval.tv_sec = 0;
 thetimer.it_interval.tv_usec = 0;
 thetimer.it_value.tv_sec = t;
 thetimer.it_value.tv_usec = 0;
 setitimer(ITIMER_VIRTUAL, &thetimer, NULL);
}

/* stop the current timer */
void timerStop()
{
 thetimer.it_value.tv_sec = 0;
 thetimer.it_value.tv_usec = 0;
 setitimer(ITIMER_VIRTUAL, &thetimer, NULL);
}

#endif

#ifdef NO_OS
// IMPLEMENTATION FOR ANY OPERATIVE SYSTEM

/* --------------------------------------------------------------------
// Random generator: seed initialisation.  If seed = 0, generates a
// random seed from pid and time else use the given seed
// -------------------------------------------------------------------- */
void SeedInit (int seed)
{
  unsigned int heure;
  
  if (!seed)
  {
    heure = (unsigned int) time(NULL);
    srand(heure);
  }
  else
    srand(seed);
}

/* --------------------------------------------------------------------
// Timer management functions
// -------------------------------------------------------------------- */
double cpuTime()
{
  return (double) (clock() / CLOCKS_PER_SEC);
}

void timer(int t)
{
	//Ignored
}

void timerStop()
{
	//Ignored
}

#endif

