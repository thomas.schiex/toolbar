BEGIN {
  nb = 0;
  sum = 0;
}


FNR == NR && FNR > 1 {
  nb++;
  w[nb] = $2;
}

FNR != NR && FNR == 1 {
  nb = 0;
  nbvar = $1;
  nbclause = $2;
  printf("p cnf %d %d\n",nbvar+nbclause,nbclause);
}

FNR != NR && FNR > 1 {
  nb++;
  for (i=3; i<=NF; i++) {
    printf("%d ", $i);
  }
  printf("-%d 0\n", nb+nbvar);
}
