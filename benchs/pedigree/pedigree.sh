#!/bin/sh

# convert pedigree from .pre format into .cp, .wcsp and .ergo formats

# .cp is only an intermediate format used to produce .wcsp
# .wcsp is toolbar format for finding the minimum number of typing errors
# .ergo is toolbar format for finding the most probable haplotype

# usage: pedigree.sh problem.pre

phase=0
ub=0
errorate=0.05

name=`basename $1 .pre`

allele=`./nballeles.sh $1 | tail -1`

echo "$name $allele"

awk -f pedigree2cp.awk $1 $allele $phase $ub > ${name}.cp
gawk -f cp2wcsp.awk ${name}.cp > ${name}.wcsp
awk -f pedigree2ergo.awk $1 $allele $errorate > ${name}.ergo

# show initial propagation
#toolbar -v3 -n0 ${name}.wcsp -u1 -o2 -p3 | awk -f solution2cp.awk ${name}.cp -
# show all satisfiable solutions
#toolbar -v ${name}.wcsp -a -u1 -o2 -p3 | awk -f solution2cp.awk ${name}.cp -
# count the number of satisfiable solutions
#toolbar ${name}.wcsp -a -u1 -o2 -p3
# show all globally consistent values
#toolbar ${name}.wcsp -u1 -o6 -p3 -v | awk -f solution2cp.awk ${name}.cp -
# find one optimal solution
#toolbar -v ${name}.wcsp -o2 -p3 | awk -f solution2cp.awk ${name}.cp -
