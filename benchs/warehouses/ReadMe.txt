Uncapacitated warehouse location problems

These data files come from the test problem sets VII, X, XIII and A to
C  in  Table 2  of  J.E.Beasley  "Lagrangean  heuristics for  location
problems"  European  Journal of  Operational  Research, vol.65,  1993,
pp383-399. (see http://www.brunel.ac.uk/depts/ma/research/jeb/orlib/uncapinfo.html)

See also the capXX.dat files  and a problem description as proposed by
Brahim Hnich  in the CSPLib  (see http://4c.ucc.ie/~tw/csplib/ problem
#34).

warehouse1.dat comes from an Eclipse solver benchmark.
warehouse0.dat comes from Brahim Hnich's problem description.

Some optimal values of the original test problem sets:
Data file   Optimal solution value
cap71       932615.750
cap72       977799.400
cap73       1010641.450
cap74       1034976.975
cap101      796648.437
cap102      854704.200
cap103      893782.112
cap104      928941.750
cap131      793439.562
cap132      851495.325
cap133      893076.712
cap134      928941.750
capa        17156454.478
capb        12979071.582
capc        11505594.329

Note that the original instances use real numbers for the supply costs
that have been converted into integers by multiplying every cost by 10
(except for  capa, capb  and capc) and  then, removing  the fractional
part.   Thus,  the resulting  optimum  of  capXX.wcsp,  divided by  10
(except for  capa, capb and  capc), is a  lower bound of  the original
instance.
