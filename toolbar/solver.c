/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id$

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "tbcheckLC.c"
#include "tbelim.h"
#include "tbsingleton.h"

#define AC_CHECK 0 /* set to "1" if you want to check the enforced consistency at every node */

void pause()
{
  char pause;
  printf("pause...\n");
  scanf("%c",&pause);
}

int *currentArity, *assignments;
double *H;
int *HeurValRND;
int **current_domains;
List *feasible_values, *feasible_values2;
int num_future, depth, *degree, *order, *order2, *order3;
int *future_var;
cost_t *DeltaU, ***DeltaB;
int *SupU, ***SupB;
int  **SupU2, **degree2;
cost_t **du2,*l_b2;
int  **future_var2;
int **fut_var_r2;
List *l1, *l2, *l3, *l4, *l5, *l6;


myStream *Q,*Rdac,*P,*S; // for alternating ordering

char *SaveFileName = NULL;

void AllocateGlobalVariables()
{
	int i,j;

	 Q=createStream();
	 Rdac=createStream();
	 P=createStream();
	 S=createStream();

	 currentArity=mymalloc(NConstr,sizeof(int));
	 assignments=mymalloc(NVar,sizeof(int));
	 H=mymalloc(NVar,sizeof(double));
	 HeurValRND=mymalloc(NValues,sizeof(double));
	 degree=mymalloc(NVar,sizeof(int));
	 order=mymalloc(NVar,sizeof(int));
	 order2=mymalloc(NVar,sizeof(int));
	 order3=mymalloc(NVar,sizeof(int));
	 future_var=mymalloc(NVar,sizeof(int));
	 DeltaU=mymalloc(NVar,sizeof(cost_t));
	 SupU=mymalloc(NVar,sizeof(int));
	 l_b2=mymalloc(NVar+1,sizeof(cost_t));

	 current_domains=mymalloc(NVar,sizeof(int *));
	 for(i=0; i<NVar; i++) current_domains[i]=mymalloc((DOMSIZE(i)+1),sizeof(int));
	 du2=mymalloc(NVar+1,sizeof(cost_t *));
	 for(i=0; i<NVar+1; i++) du2[i]=mymalloc(NVar,sizeof(cost_t));
	 SupU2=mymalloc(NVar+1,sizeof(int *));
	 for(i=0; i<NVar+1; i++) SupU2[i]=mymalloc(NVar,sizeof(int));
	 degree2=mymalloc(NVar+1,sizeof(int *));
	 for(i=0; i<NVar+1; i++) degree2[i]=mymalloc(NVar,sizeof(int));
	 future_var2=mymalloc(NVar+1,sizeof(int *));
	 for(i=0; i<NVar+1; i++) future_var2[i]=mymalloc(NVar,sizeof(int));
	 fut_var_r2=mymalloc(NVar+1,sizeof(int *));
	 for(i=0; i<NVar+1; i++)  fut_var_r2[i]=mymalloc(NConstr,sizeof(int));


	DeltaB=mymalloc(NVar,sizeof(cost_t **));
	for(i=0; i<NVar; i++)
	{
	  DeltaB[i]=mymalloc(NVar,sizeof(cost_t *));
		for(j=0; j<NVar; j++) DeltaB[i][j]=mymalloc(DOMSIZE(i),sizeof(cost_t));
	}
	SupB=mymalloc(NVar,sizeof(int **));
	for(i=0; i<NVar; i++)
	{
		SupB[i]=mymalloc(NVar,sizeof(int *));
		for(j=0; j<NVar; j++) SupB[i][j]=mymalloc(DOMSIZE(i),sizeof(int));
	}

	feasible_values=mymalloc(NVar,sizeof(List));
	feasible_values2=mymalloc(NVar,sizeof(List));
	l1=mymalloc(NVar+1,sizeof(List));
	l2=mymalloc(NVar+1,sizeof(List));
	l3=mymalloc(NVar+1,sizeof(List));
	l4=mymalloc(NVar+1,sizeof(List));
	l5=mymalloc(NVar+1,sizeof(List));
	l6=mymalloc(NVar+1,sizeof(List));

	if ((Options & Options & OPTIONS_SINGLETON_PRE) ||
	    (Options & Options & OPTIONS_SINGLETON_BB)) {
	  VarOrdering=mymalloc(NVar,sizeof(int));
	  R=mymalloc(NVar*NValues,sizeof(int));
	  Z=mymalloc(NVar*NValues*NValues,sizeof(VarVal_t));
	}
}

void FreeGlobalVariables()
{
  int i,j;

  destroyStream(Q);
  destroyStream(Rdac);
  destroyStream(S);
  destroyStream(P);

  free(currentArity); free(assignments); free(H);
  free(HeurValRND);
  free(degree); free(order); free(order2); free(order3);
  free(future_var);
  free(DeltaU);  free(SupU);  free(l_b2);

  for(i=0; i<NVar; i++) free(current_domains[i]);
  free(current_domains);
  for(i=0; i<NVar+1; i++) free(du2[i]);
  free(du2);
  for(i=0; i<NVar+1; i++) free(SupU2[i]);
  free(SupU2);
  for(i=0; i<NVar+1; i++) free(degree2[i]);
  free(degree2);
  for(i=0; i<NVar+1; i++) free(future_var2[i]);
  free(future_var2);
  for(i=0; i<NVar+1; i++) free(fut_var_r2[i]);
  free(fut_var_r2);

  for(i=0; i<NVar; i++) {
    for(j=0; j<NVar; j++) free(DeltaB[i][j]);
    free(DeltaB[i]);
  }
  free(DeltaB);
  for(i=0; i<NVar; i++) {
    for(j=0; j<NVar; j++) free(SupB[i][j]);
    free(SupB[i]);
  }
  free(SupB);

  free(feasible_values);  free(feasible_values2);
  free(l1);  free(l2);  free(l3);
  free(l4);  free(l5);  free(l6);

  if ((Options & Options & OPTIONS_SINGLETON_PRE) ||
	    (Options & Options & OPTIONS_SINGLETON_BB)) {
    free(VarOrdering); free(R); free(Z);
  }
}

void var2(int rest, int *v1, int *v2)
{
  /*Pre: rest is a constraint with two future variables*/
  /*Post: v1 and v2 are the two future variables of rest*/

  int j,k;
  *v1=-1;*v2=-1;

  for(k=0;k<ARITY(rest);k++)
    {
      j=SCOPEONE(rest,k);
      if(assignments[j]==-1){
	if(*v1==-1) {*v1=j;}
	else{*v2=j;k=ARITY(rest);}
      }
    }
}


void update_degree(int j)
/* updates the degree of all future variables, after the assignment of variable j*/
{
  int i;
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && ISBINARY(i,j)) degree[i]--;
}

void sort_var_fullSupport()
{
  /* currently the order is lexicographic */
  int i;
  for(i=0;i<NVar;i++)  {order[i]=i;order2[i]=i;}

}


void sort_fut_var()
{
	int i,j;
	j=0;
	for(i=0;i<NVar;i++) if(assignments[i]==-1) future_var[j++]=i;
}


void prune_value(int h, int l)
/* prunes (h,l) and updates structures accordingly
 The current value of feasible_values[h] must be "l"*/
{
	List_delete_node(&feasible_values[h]); /* removes l from h's domain*/
	current_domains[h][l]=1; /* updates current_domains */
	List_insert2(&l1[depth],h,l); /* records the pruning for future context restoration */
	if (CDISJ && R[h * NValues + l] == Success) {
	  Z[ZIndex].thevar = h;
	  Z[ZIndex++].theval = l;
	}
}

int prune_variable(int h, int *stop)
/* traverses the list of feasible values of variable h and prunes those that have
 * become unfeasible.
 * Returns TRUE if some value is pruned.  */
{
  int l,pruned;

  pruned=FALSE;
  List_reset_traversal(&feasible_values[h]);
  while(!List_end_traversal(&feasible_values[h]))
    {
      l=List_consult1(&feasible_values[h]);
      if(Bottom+CostU(h,l)>=Top)
	  {
		  prune_value(h, l);
		  pruned=TRUE;
	  }
      else{List_next(&feasible_values[h]);}
    }
  if(List_empty(&feasible_values[h]))*stop=TRUE;
  return(pruned);
}

int look_ahead(int var, int val)
/* look_ahead the current assignment (var,val) and record changes for
 * future context restoration */
{
  int j,val2,stop,flag;
  cost_t temp;

 stop=FALSE;
 stream_ini(Q); /* initialize the stream */
 stream_ini(Rdac); /* initialize the stream */
 stream_ini(P); /* initialize the stream */
 stream_ini(S); /* initialize the stream */
 for(j=0;j<NVar;j++) if(assignments[j]==-1 && ISBINARY(var,j))
 {
	 flag=FALSE;
	 List_reset_traversal(&feasible_values[j]);
	 while(!List_end_traversal(&feasible_values[j]))
	 {
		 val2=List_consult1(&feasible_values[j]);
		 List_next(&feasible_values[j]);
		 temp=CostB(var,val,j,val2);
		 if(CostU(j,val2)== BOTTOM && temp>BOTTOM) flag=TRUE;
		 if(temp>BOTTOM)
		 {
		   /* printf("FC: (%d,%d) -> (%d,%d) += %d\n", var,val,j,val2,temp); */
		   UNARYCOST(j,val2)+=temp; /* propagates assignment */
		   List_insert3pc(&l2[depth],j,val2,0,temp); /* records the change for context restoration */
		 }
	 }
	 if(flag)
	 {
		 stream_push(S,j);
		 stream_push(Rdac,j);
	 }
       	 stop=!findSupportU(j,&flag);
	 if(stop) j=NVar;
 }

 for(j=0;j<NVar;j++){
   if(stop) j=NVar;
   else{
     if(assignments[j]==-1 && prune_variable(j,&stop)) {stream_push(Q,j);}
   }
 }

 // if(!stop)
 //   for(j=0;j<NVar;j++)
 //      if(assignments[j]==-1 && prune_variable(j,&stop))
 //	  {stream_push(Q,j);}
 return(!stop);
}

int projectB(int i)
     /*Projects all constraints that have become binary after the assignment of variable i*/
{
  int j,v1,v2,a,b,sa, sb, mod,addedBinary;
  cost_t inc;
  int flag1,flag2;

  for(j=0;j<NConstr;j++)if(inScope(i,j) && currentArity[j]==2)
    {
      mod=FALSE; addedBinary=FALSE;
      var2(j,&v1,&v2);
      if(!ISBINARY(v1,v2))
	{createBinaryConstraint(v1,v2);addedBinary=TRUE;}
      flag1=FALSE; flag2=FALSE;
      List_reset_traversal(&feasible_values[v1]);
      while(!List_end_traversal(&feasible_values[v1]))
	{
	  a=List_consult1(&feasible_values[v1]);
	  List_next(&feasible_values[v1]);
	  assignments[v1]=a;
	  sa=SupB[v1][v2][a];
	  if(addedBinary && (current_domains[v2][sa]!=-1 ||
			     CostU(v2,sa)))flag1=TRUE;
	  List_reset_traversal(&feasible_values[v2]);
	  while(!List_end_traversal(&feasible_values[v2]))
	    {
	      b=List_consult1(&feasible_values[v2]);
	      List_next(&feasible_values[v2]);
	      assignments[v2]=b;
	      sb=SupB[v2][v1][b];
	      if(addedBinary && (current_domains[v1][sb]!=-1 ||
				 CostU(v1,sb)))flag2=TRUE;
	      inc=cost(j,assignments);
	      if(inc>BOTTOM)
		{
		  mod=TRUE;
		  if(v1<v2) BINARYCOST(v1,v2,a,b)+=inc;
		  else BINARYCOST(v2,v1,b,a)+=inc;
		  List_insert4(&l6[depth],v1,v2,a,b);List_next(&l6[depth]);
		  List_insert3pc(&l6[depth],0,0,0,inc);List_next(&l6[depth]);
		  if(sa==b)flag1=TRUE;
		  if(sb==a)flag2=TRUE;
		}
	    }
	}
      if(mod)
	{
	  if(flag1) stream_push(Q,v2);
	  if(flag2) stream_push(Q,v1);

	if(LcLevel==LC_EDAC)
	{
		if(order[v1]<order[v2] && (current_domains[v2][SupU[v2]]!=-1 || CostU(v1,SupB[v2][v1][SupU[v2]])>BOTTOM || CostB(v2,SupU[v2],v1,SupB[v2][v1][SupU[v2]])>BOTTOM))
		{
		stream_push(S,v2);
		}
		else if(order[v1]>order[v2]  && (current_domains[v1][SupU[v1]]!=-1 || CostU(v2,SupB[v1][v2][SupU[v1]])>BOTTOM || CostB(v1,SupU[v1],v2,SupB[v1][v2][SupU[v1]])>BOTTOM))
		{
		stream_push(S,v1);
		}
	}

	if(flag1 && order[v1]<order[v2])
	{
			stream_push(Rdac,v2);
	}
	if(flag2 && order[v2]<order[v1])
	{
			stream_push(Rdac,v1);
	}


	  if(addedBinary)
	    {
	      degree[v1]++; degree[v2]++;
	      List_insert4(&l5[depth],v1,v2,0,0);
	    }
	}
      else
	{
	  if(addedBinary) destroyBinaryConstraint(v1,v2);
	}
      assignments[v1]=-1;
      assignments[v2]=-1;
    }
  return TRUE;
}

void save_parameters1()
{
  int j;
  for(j=0;j<NVar;j++)
    {
      du2[depth][j]=DeltaU[j];SupU2[depth][j]=SupU[j];
      degree2[depth][j]=degree[j];
      future_var2[depth][j]=future_var[j];
    }
  l_b2[depth]=Bottom;
}


void restore_parameters1()
{
  int j;
  for(j=0;j<NVar;j++)
    {
      DeltaU[j]=du2[depth][j]; SupU[j]=SupU2[depth][j];
      degree[j]=degree2[depth][j];
      future_var[j]=future_var2[depth][j];
    }
  Bottom=l_b2[depth];
}


void BB()
{
  int ii;
  int i; /* current variable */
  int l; /* current value */
  int j,i2,i3;
  FILE *sol;

  if(num_future==0)
    {
      if(Bottom<Top)
	{
	  if (AllSolutions) {
	    NumberOfSolutions++;
	    if (Verbose) {
	      printf("New solution %d: ", NumberOfSolutions); PRINTCOST(Bottom); printf(" (%lu nodes, %.2lf secs)\n%d solution: ",NNodes,cpuTime(),NumberOfSolutions);
	      for (i=0; i<NVar; i++) {
		printf("%d ", assignments[i]);
	      }
	      printf("\n");

	    }
	  } else {
	    Top=Bottom; /* The current distance becomes the new upper bound*/
	    if (Verbose) {
	      printf("New Top: "); PRINTCOST(Top); printf(" (%lu nodes, %.2lf secs)\n",NNodes,cpuTime());
	    }
	    for(i=0;i<NVar;i++){BestSol[i]=assignments[i]; }
	    if (WriteSolution) {
	      sol = fopen("sol", "w");
	      if(sol != NULL) {
		for (ii=0; ii<NVar; ii++) {
		  fprintf(sol," %d", BestSol[ii]);
		}
		fprintf(sol,"\n");
		fclose(sol);
	      }
	    }
	  }
	}
    }
  else
    {
      if(small_degree(&l,&i,&i2,&i3))elim(l,i,i2,i3);
      else
	{
      /* always select unassigned variables with only one value first */
      for (i=0; i<NVar; i++) {
	if (assignments[i]==-1 && List_n_of_elem(&feasible_values[i])==1) {
	  goto skipvarchoice;
	}
      }
      i=(*HeurVarFunc[HeurVar])(); /* i becomes the new current variable */
    skipvarchoice:

      num_future--;       /* one more variable is assigned */
      depth++;
      assignments[i]=1000;
      sort_fut_var();
      assignments[i]=-1;
      update_degree(i);
      save_parameters1();
      for(j=0;j<NConstr;j++)
	{fut_var_r2[depth][j]=currentArity[j]; if(inScope(i,j))currentArity[j]--;}
      List_create(&feasible_values2[i]);
      (*HeurValFunc[HeurVal])(i);
      List_reset_traversal(&feasible_values2[i]);

      while(!List_end_traversal(&feasible_values2[i]))
	{
	  l=List_consult1(&feasible_values2[i]); /* l is the new current value*/
	  List_next(&feasible_values2[i]);
	  if (Verbose>=2) {
	    if (Verbose>=3) showCosts();
	    printf("[%d,", depth);
	    PRINTCOST(Bottom);
	    printf(",");
	    PRINTCOST(Top);
	    printf(",%d] x%d = %d\n", numberOfValues(), i, l);
	  }
	  assignments[i]=l;                /* l is assigned to i */
	  if (NNodes >= NodeLimit) nodeOut();
	  NNodes++;
	  List_create(&l1[depth]); List_create(&l2[depth]);
	  List_create(&l3[depth]); List_create(&l4[depth]);
	  List_create(&l5[depth]); List_create(&l6[depth]);
	  Bottom+=CostU(i,l);         /* Computes current distance */

	  if(Bottom<Top && look_ahead(i,l) && projectB(i) &&
	     (*LcFunc[LcLevel])() && (Options==0 || metaLC(FALSE, Options & OPTIONS_DOMINANCE_BB, Options & OPTIONS_SINGLETON_BB, RestrictedSingletonNbVar)))
	    {
#if (AC_CHECK == 1)
	      (*CheckACFunc[LcLevel])();
#endif
	      BB();
	    }
	  restore_context();
	  restore_parameters1();
	  List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
	  List_dispose_memory(&l3[depth]); List_dispose_memory(&l4[depth]);
	  List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]);

	}
      assignments[i]=-1; /* deassignes current variable */
      for(j=0;j<NConstr;j++){currentArity[j]=fut_var_r2[depth][j];}
      List_dispose_memory(&feasible_values2[i]);
      num_future++; /* current variable becomes future */
      depth--;
	}
    }
}


void ini_variables()
{ int i,l,j;

  AllocateGlobalVariables();
  List_allocate_dynamic_memory();
  for(i=0;i<NConstr;i++)
    {
      currentArity[i]=ARITY(i);
    }
  num_future=NVar;
  depth=0;
  for(i=0;i<NVar;i++){assignments[i]=-1;}
  sort_fut_var();
  for(i=0;i<NVar;i++)
    {
      for(j=0;j<DOMSIZE(i);j++){current_domains[i][j]=-1;}
    }
  for(i=0;i<NVar;i++){current_domains[i][DOMSIZE(i)]=NVar+1;}
  for(i=0;i<NVar;i++)
    {
      List_create(&feasible_values[i]);
      for(j=0;j<DOMSIZE(i);j++)List_insert1(&feasible_values[i],j);
    }

  for(i=0;i<NVar;i++){DeltaU[i]=BOTTOM;}
  for(i=0;i<NVar;i++)for(j=0;j<NVar;j++)for(l=0;l<DOMSIZE(i);l++)
  {DeltaB[i][j][l]=BOTTOM;}

  for(i=0;i<NVar;i++){degree[i]=0;}
  for(i=0;i<NVar;i++)for(j=i+1;j<NVar;j++) if(ISBINARY(i,j))
    {
      degree[i]++;degree[j]++;
    }

  /* Initialize supports to the dummy value "m" */
  for(i=0;i<NVar;i++){SupU[i]=DOMSIZE(i);}
  for(i=0;i<NVar;i++)for(j=0;j<NVar;j++)
    {
      /*
      if(ISBINARY(i,j))
	{for(l=0;l<DOMSIZE(i);l++) SupB[i][j][l]=DOMSIZE(j);}
      else
	{for(l=0;l<DOMSIZE(i);l++) SupB[i][j][l]=0;}
	*/
	for(l=0;l<DOMSIZE(i);l++) SupB[i][j][l]=0;
    }


  sort_var_fullSupport(); /* sorts variables for full supports*/
  (*HeurVar2Func[HeurVar])(); /*sorts variables for static variable selection heuristics*/
  if ((Options & OPTIONS_SINGLETON_PRE) || (Options & OPTIONS_SINGLETON_BB))
    for(i=0;i<NVar;i++) VarOrdering[order3[i]] = i;
}

void destroy_variables()
{
  int i;
  for(i=0;i<NVar;i++)List_dispose_memory(&feasible_values[i]);
  List_deallocate_dynamic_memory();
  FreeGlobalVariables();
}

void prune_this_value(int h, int l);
void solve()
{
  FILE* fileHandle;
  int i,stop,flag,j,l;
  cost_t initialtop;
  if (RestrictedSingletonNbVar > NVar) RestrictedSingletonNbVar = NVar;
  ini_variables();

  stop=FALSE;
  if (Verbose) printf("Initial number of values: %d\n",numberOfValues());
  for(i=0;i<NVar;i++)if(!findSupportU(i,&flag)){stop=TRUE; i=NVar;}
  if(!stop)
    {
      List_create(&l1[depth]); List_create(&l3[depth]); List_create(&l4[depth]);
      stream_ini(Rdac); for(i=0;i<NVar;i++) stream_push(Rdac,i);
      stream_ini(Q); for(i=0;i<NVar;i++) stream_push(Q,i);
      stream_ini(S); for(i=0;i<NVar;i++) stream_push(S,i);
      if((*LcFunc[LcLevel])() && (ElimLevel<0 || elimAtRoot()) && (Options==0 || metaLC(TRUE, (Options & OPTIONS_DOMINANCE_PRE) || (Options & OPTIONS_DOMINANCE_BB), (Options & OPTIONS_SINGLETON_PRE) || (Options & OPTIONS_SINGLETON_BB), NVar)))
	{
#if (AC_CHECK == 1)
	  (*CheckACFunc[LcLevel])();
#endif
	  if (Options & OPTIONS_GLOBALINVCONSISTENCY_PRE) {
	    printf("Global inverse consistency:\n");
	    initialtop = Top;
	    for(i=0;i<NVar;i++) {
	      for (l=0; l <DOMSIZE(i); l++) {
		if (current_domains[i][l]==-1) {
		  if (Verbose) printf("Try x%d = %d\n", i, l);
		  Top = initialtop;
		  BestSol[0] = -1;

		  num_future--;       /* one more variable is assigned */
		  depth++;
		  assignments[i]=1000;
		  sort_fut_var();
		  assignments[i]=-1;
		  save_parameters1();
		  for(j=0;j<NConstr;j++)
		    {fut_var_r2[depth][j]=currentArity[j]; if(inScope(i,j))currentArity[j]--;}

		  assignments[i]=l;                /* l is assigned to i */
		  List_create(&l1[depth]); List_create(&l2[depth]);
		  List_create(&l3[depth]); List_create(&l4[depth]);
		  List_create(&l5[depth]); List_create(&l6[depth]);

		  Bottom+=CostU(i,l);         /* Computes current distance */
		  if (Bottom<Top && look_ahead(i,l) && projectB(i) && (*LcFunc[LcLevel])()) {
		    BB();
		  }
		  restore_context();
		  restore_parameters1();
		  List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
		  List_dispose_memory(&l3[depth]); List_dispose_memory(&l4[depth]);
		  List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]);

		  assignments[i]=-1; /* deassignes current variable */
		  for(j=0;j<NConstr;j++){currentArity[j]=fut_var_r2[depth][j];}
		  num_future++; /* current variable becomes future */
		  depth--;

		  if (BestSol[0] == -1) {
		    prune_this_value(i, l);
		    stream_push(Q,i);
		    if (CostU(i,l) == BOTTOM)
		      {
			stream_push(Rdac,i);
		      }
		    /* enforce local consistency */
		    for(j=0;j<NVar;j++) {
		      if(assignments[j]==-1 && !findSupportU(j,&flag)) {
			printf("Domain wipe out!\n");
			goto StopSolve;
		      }
		    }
		    if (!(*LcFunc[LcLevel])()) {
		      printf("Domain wipe out!\n");
		      goto StopSolve;
		    }
		  }
		}
	      }
	    }
	    printf("Number of values after preprocessing: %d\n",numberOfValues());
	    if (Verbose) showCosts();
	    Top = Bottom;
	  } else {
	    if (Verbose) printf("Number of values after preprocessing: %d\n",numberOfValues());
	    if (SaveFileName != NULL) {
	      fileHandle = fopen(SaveFileName, "w");
	      if(fileHandle == NULL) {
		perror("Error: bad output filename (impossible to save the current problem after preprocessing)!"); 
		abort();
	      }
	      switch (SaveFileFormat) {
	      case FORMAT_WCSP: 
		saveWCSP(fileHandle);
		break;
	      case FORMAT_WCNF: 
		saveWCNF(fileHandle);
		break;
	      default: 
		perror("Error: unknown output format (impossible to save the current problem after preprocessing)!"); 
		abort();;
	      }
	      exit(0);
	    }
	    if (NodeLimit != 0) BB();
	    else {
	      if (Verbose >= 3) showCosts();
	      Top = Bottom;
	    }
	  }
	}
    StopSolve:
      List_dispose_memory(&l1[depth]);List_dispose_memory(&l3[depth]);
      List_dispose_memory(&l4[depth]);
    }
  destroy_variables();
}
