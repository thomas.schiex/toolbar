## usage: awk -f extractwcsp.awk problem.wcsp problem.wcsp var1 var2 ... > subproblem.wcsp

BEGIN {
  newn = 0;
  newd = 0;
  newe = 0;
  for (i=3; i<ARGC; i++) {
    vars[ARGV[i]] = newn;
    ARGV[i] = "";
    newn++;
  }
  ARGC = 3;
}

NR==1 {
  name = $1;
  n = $2;
  d = $3;
  e = $4;
  ub = $5;
}

NR==2 {
  domain = "";
  for (i=0; i<n; i++) if (i in vars) {
    domain = domain " " $(i+1);
    if ($(i+1) > newd) newd = $(i+1);
  }
  newe = 0;
  tuple = 0;
  ok = 0;
}

FNR >= 3 {
  if (tuple==0) {
    ok = 1;
    arity = $1;
    for (a=2; a<=arity+1; a++) {
      if (!($a in vars)) ok = 0;
    }
    if (arity==0) ok = 0;
    tuple = $NF;
    if (ok) {
      newe++;
      for (a=2; a<=arity+1; a++) {
	$a = vars[$a];
      }
    }
  } else {
    tuple--;
  }
  if (ok && FNR != NR) print $0;
}

FNR != NR && FNR == 1 {
  print name,newn,newd,newe,ub;
  print substr(domain,2);
  tuple = 0;
  ok = 0;
}
