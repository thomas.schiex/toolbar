#! /usr/bin/tcsh

# Translate uncapacitated warehouse location problems in wcsp format

# Warehouse location problem from OR-Library
# http://www.brunel.ac.uk/depts/ma/research/jeb/orlib/uncapinfo.html

foreach f ( cap*.txt )
    awk -f warehouse.awk $f >! ${f:r}.wcsp
end
