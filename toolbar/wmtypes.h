/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmtypes.h
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */

#include "wmlist.h"



#define MEM_FACT 1000 // Factor to increment memory allocation of restoration structures

#define MEM_CLA_FACT 10 // Factor of 3-ary clauses allowed. 

#define WM_MAX_ARITY 50 // Max arity for clauses.

typedef long long int cost_WM;


typedef struct
{
	unsigned int depth_diff1;
	unsigned int num_diff1;
	unsigned int depth_back;
	unsigned int num_back;
	unsigned int num_unary;
	
} Statistics;


typedef struct
{

	int size;

	int * content;

	int * elems;

	int n_elem;

} stack;



typedef struct
{
	int boolValue;

	int idVar;
} literal;



typedef struct
{
	int indexToClause;

} ReferenceToClause;





typedef struct
{

	cost_WM weight;

	int literalsWithoutAssign;

	int elim;

	NodeLD * terminator;

	literal termLit;

	literal termLit2;

	literal TLiterals[WM_MAX_ARITY];

	int literalsTotal;
	
	int GenLC;

} clause;





typedef struct

{

	int indexToVariable;



} ReferenceToVariable;





typedef struct

{

	ListD * listLiterals;

	ListD * listNoLiterals;



} variable;



typedef struct
{

	int *usedValue;

	int not_considered_values;

} decision;

typedef struct
{
	int v1;
	int v2;
	int v3;
	int v4;
} triangleElem;

typedef struct
{
	long long int node;
	long long int it;
} insertedTriangle;

typedef struct 
{
	triangleElem *triStack;
	int triT;
	int triMax;
	int current[4];
	int actualIteration;
} triangleStruct;

typedef struct
{
	int var;
	int val;
} assignment;

typedef struct
{
	assignment ***paths;
	int **distance;
	int **n_d;
} hyperRes;


typedef struct
{

	clause * clauses; // Array of clauses

	int totalClauses; // UpperBound of array of clauses
	
	int maxClauses;

	variable * variables; // Array of variables
	
	assignment * assignments; // Array containing the current assigments in the order they were executed.

	int totalVariables; // UpperBound of array of variables

	decision * ArrayOfDecision; // Array for decide the variable to take at any iteration

	cost_WM ** UnaryCosts; // Array to manage unary costs

	cost_WM ****BinaryCosts; // Array to manage binary costs

	ListD * notAssignedVars; // list of not yet assigned variables.

	ListD * notAssignedCla;

	cost_WM UB; // Upper Bound for wmaxsat

	cost_WM LB; // Low Bound for wmaxsat

	int * Result;

	int * bestResult;

	cost_WM totalWeight;

	int maximumArity;

	unsigned long totTime;

	char fileName[MAX_CARS];

	ListR restoreList;  // List wich nodes will be used to save and load restoration information
	
	NodeLD * restoreTernaryClauses;

	stack *Q; // This stack contains all variables to check AC local consistency

	stack *R; // This stack contains all variables to check DAC local consistency

	cost_WM * pows; // Array to save the pows

	cost_WM * jeroslows; // Array to save the jeroslow heuristic for each variable

	int * tmpArr; // For an efficient EDAC

	int * order;

	int * order2;

	int **isBinary; // To avoid visiting binary costs with cost 0.

	int *SupU;

	triangleStruct cTri;
	
	Statistics stat;
	
	int iniLC;
	
	int degreeB[3000];


	// HyperResolution
	
	int AC_WCSP;
	
	int max_nonbin;
	
	int **isVisited;

	int ****isVisitedBinary;
	
	assignment *toVisit;

	assignment **sPaths;

	hyperRes HR;

} problem;



typedef struct

{

	int var;

	int val;

} PrunedValue;





typedef struct

{

	int var;

	int val;

	int qty;

} DacPrunedValue;



typedef struct

{

	int var1;

	int var2;

	int val1;

	int val2;

	int qty;

} DacPrunedValue2;



typedef struct
{
	ListR pru;

	cost_WM actualLB;

	int updatePrune;

	ListR satList;

	ListR pruneList;

	ListR NCList;

	ListR ACList;

	ListR ternaryList;

	int NClimit;
	
	int new3SAT;
	
	int currentAssignments;

} restoreStruct;





// Functions for literal Lists

void showLiterals(problem *p,int i);

void showLiteral(literal * li);

literal * getActualLiteral(ListS * l);

literal * getLiteral(NodeLS * n);

void insertLiteral(ListS * l, literal * li);

literal * findFirstVariableWithoutAssign(problem *p, int c,int *pos);

literal * findSecondVariableWithoutAssign(problem *p, int c,int *pos);

void passJeroslow(problem *p,int c);

void nopassJeroslow(problem *p,int c);



// Functions for references to clauses

void insertReference(ListD * l, ReferenceToClause * c);

ReferenceToClause * getActualReference(ListD * l);

ReferenceToClause * getReference(NodeLD * n);

void showReferences(ListD * l);

void showReference(ReferenceToClause * rc);

ReferenceToClause * createReference(int index);

void clearReferences(ListD * l);


// functions for references to not assigned clauses

void insertReferenceCla(ListD * l, ReferenceToClause * rc);

ReferenceToClause * getActualReferenceCla(ListD * l);

ReferenceToClause * getReferenceCla(NodeLD * n);

void showReferencesCla(ListD * l);

void showReferenceCla(ReferenceToClause * rc);

ReferenceToClause * createReferenceCla(int index);

void clearReferencesCla(ListD * l);

void createListOfReferencesClauses(problem * p);



// functions for references to variables

void insertReferenceVar(ListD * l, ReferenceToVariable * rv);

ReferenceToVariable * getActualReferenceVar(ListD * l);

ReferenceToVariable * getReferenceVar(NodeLD * n);

void showReferencesVar(ListD * l);

void showReferenceVar(ReferenceToVariable * rv);

ReferenceToVariable * createReferenceVar(int index);

void clearReferencesVar(ListD * l);

void createListOfReferencesVariables(problem * p);



// Functions for clauses

void createArrayClauses(problem * p,int num_cla);

void clearArrayClauses(problem * p);

void showClauses(problem * p);



// Functions for variables

void createArrayVariables(problem * p,int num_var);

void clearArrayVariables(problem *p);

void createArrayOfDecision(problem * p);

void showArrayOfDecision(problem * p);

void showVariables(problem * p);

void clearArrayOfDecision(problem *p);





// Functions for array of results

void createArrayResults(problem *p);

void destroyArrayResults(problem *p);

void showArrayResults(problem *p);



// Functions for array of Best results

void createArrayBestResults(problem *p);

void destroyArrayBestResults(problem *p);

void showArrayBestResults(problem *p);

void saveBestResult(problem *p);



// Functions for array of prune

void createUnaryCosts(problem *p);

void showUnaryCosts(problem *p);



// Functions for BinaryCosts



void createBinaryCosts(problem *p);

void destroyBinaryCosts(problem *p);

void showBinaryCosts(problem *p);



// Functions for problems

void initProblem(problem *p);

void showProblem(problem * p);

void showProblemResults(problem * p);

void clearProblem(problem * p);

int readProblem(FILE *f, problem *p,int isCnf);



void showRestoreInformation();



// Functions for stacks



stack * iniStack(int size);

void showStack(stack *s);

void pushStack(stack *s,int i);

int popStack(stack *s);

int popStackMinDom(stack *s);

int popStackMaxDom(stack *s);

void clearStackValues(stack *s);

void clearStack(stack *s);



// Funtions for array of pows



void createArrayPows(problem *p);

void destroyArrayPows(problem *p);

void showArrayPows(problem *p);



// Functions for array of jeroslows



void createArrayJeroslows(problem *p);

void destroyArrayJeroslows(problem *p);

void showArrayJeroslows(problem *p);

// Functions for MAX-SAT RULES
int sign(literal *res1);

int compClauses(problem *p,int refCla1,int refCla2,int i,literal *l1,literal *l2);

// Functions for isBinary Tables
void updateIsBinaryTable(problem *p);

int isBinaryPair(problem *p,int i, int j);

void setIsBinaryPair(problem *p,int i, int j,int b);

void insertChange(problem *p,int a,int b,int v1,int v2);
void updateCurrent(problem *p,int a,int b);
void changedCurrent(problem *p,int a,int b);
void printProblem(problem *p);
void printProblemCNF(problem *p);

void showTernary(problem *p,int c,int b);
void showBinary(problem *p,int v,int v2,int val,int val2);
void showUnary(problem *p,int v,int val);
