#ifndef DOMAINABSTRACTIONBE_H_
#define DOMAINABSTRACTIONBE_H_

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "wcsp.h"
#include "be.h"

// careful: if the number of abstraction functions is chosen wrongly the 
// heuristic might not be admissible anymore
#define DEFAULT_NUMBER_ABSTRACTION_FUNCTIONS 1
#define MAX_TUPLES 1000
#define PQUEUE_SIZE 5000 // initial size of the queue


typedef short * full_assignment_t;

typedef struct search_assignment_t_ {
    full_assignment_t values;
    cost_t cost;  // real cost of this assignment
    cost_t estimate; // estimated cost to the full assignment through this assignment
} search_assignment_t;


/*
 * The abstracted constraint is a set of assignment -> cost mappings where the 
 * assignments are tuples of abstract values, i.e. sets of concrete values. Here,
 * abstracted constraints are implemented as multidimensional matrices just like
 * the concrete constraints (nodofunc). Infact, the structure uses a nodofunc
 * constraint to store the cost matrix and all other constraint relevant 
 * information like e.g. arity.
 * How does this work? Variable values here are simply represented through 
 * indicies, i.e. domains always are of the form 0..x with x given by the wcsp
 * file. Now, abstracting a constraint means converting the set of allowed tuples
 * to an aquivalent set of tuples consisting of abstracted values. So for
 * example the original values 0,1,2 might be abstracted to one value av(0,1,2),
 * av being short for 'abstract value'. Assuming that some constraint c allows
 * tuple (0,1,3,1) with cost 1, which means c( (0,1,3,1) ) = 1, then the 
 * according abstracted constraint ac allows the tuple 
 * 
 * ( av(0,1,2), av(0,1,2), av(3), av(0,1,2) ) with cost 1
 * 
 * Suppose there is another tuple with c( (1,2,3,2) ) = 0. The abstraction would 
 * actually yield the same tuple as above in the abstracted constraint. If both
 * concrete tuples are contained in the concrete constraint we get one abstract
 * tuple in the abstracted constraint with 2 cost values. In this case, 
 * the minimum of the two costs is taken. This is actually the standard case 
 * since the abstraction would be of no use if it wouldn't reduce the number of
 * tuples.
 * Note that we assumed here that the same abstraction function is used for
 * all variables. One could easily use different abstraction functions for each
 * variable. 
 * 
 * The abstract values are also represented simply through indicies. Suppose we 
 * have concrete values D = {0,1,2,3} which get abstracted to av(0,1) and 
 * av(2,3). The first abstract value is represented by 0 and the second by 1, 
 * yielding the new abstracted domain aD = {0,1}. By doing so the struct nodofunc
 * can be used to store the abstracted constraints. Of course care must be taken
 * to differientiate  between concrete and abstract constraints, this is why
 * for abstract constraints the type below is defined. It consists of the 
 * elements of nodofunc storing the actual constraint (whose variable 
 * instantiations are to be interpreted as abstract values) and additionally a 
 * vector of abstraction functions (which containts an abstraction function for 
 * each variable of the constraint) which map concrete values to the abstract 
 * values employed in this abstracted constraint.
 */
typedef int (*value_tau_func_t)(int);

typedef struct abstracted_constraint_t_ {
    // constraint function nodofunc
    short aridad; /* arity of the cost function*/
    short *vars; /* scope */
    cost_t *costs; /* cost matrix */
    unsigned int ntuples;      /* number of tuples */
    int *despl_var; /* shift to be done for each variable value in order to obtain the position in costs to a given tuple */
    void *sig_func; /* next function */
    //end constraint function nodofunc
    
    value_tau_func_t *vtaus; //value abstraction functions
    int *domsizes; // sizes of the abstracted domains
} abstracted_constraint_t;

typedef struct dynamic_tau_data_t_ {
	value_tau_func_t *vtaus;
	int *domsizes;
} dynamic_tau_data_t;

typedef abstracted_constraint_t* (*tau_func_t)(nodofunc *, 
		   									   dynamic_tau_data_t *);

full_assignment_t solveDomainAbstractionBE();

#endif /*DOMAINABSTRACTIONBE_H_*/
