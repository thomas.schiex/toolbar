/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id$

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

#ifndef TBSINGLETON_H
#define TBSINGLETON_H

typedef struct {int thevar; int theval;} VarVal_t;
extern int *VarOrdering;
extern int CDISJ;
extern int Success;
extern int *R;
extern VarVal_t *Z;
extern int ZIndex;

extern int metaLC(int iterated, int dominance, int singleton, int nbselectedvar);
extern void showCosts();
extern int numberOfValues();

#endif
