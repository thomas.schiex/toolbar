#!/bin/bash

# auto-generates the moduloN_vtau.c module plus header. 

N=$1 # the max modulo value
VTAU_MAX_MODULO=$N
ModuloN_vtaus_size=$VTAU_MAX_MODULO+1

echo '

/* This is auto-generated code. */

#include "moduloN_vtau.h"

' > moduloN_vtau.c

echo "

/* This is auto-generated code. */
#ifndef MODULON_VTAU_H_
#define MODULON_VTAU_H_

#include \"domainAbstractionBE.h\"

#define VTAU_MAX_MODULO $VTAU_MAX_MODULO

" > moduloN_vtau.h

i=0
for((i=1; i <= VTAU_MAX_MODULO; i++))
do

    echo \
    "
int vtauMod$i(int v) {
    return v%$i;
}

    " >> moduloN_vtau.c	

    echo "int vtauMod$i(int v);" >> moduloN_vtau.h

done

echo \
"
value_tau_func_t ModuloN_vtaus[$ModuloN_vtaus_size] = {
NULL, //the first element is NULL, so that the modulo functions start at 1" \
>> moduloN_vtau.c

for((i=1; i < VTAU_MAX_MODULO; i++))
do

echo "vtauMod$i," >> moduloN_vtau.c

done

echo "vtauMod$VTAU_MAX_MODULO};" >> moduloN_vtau.c

echo \
"
extern value_tau_func_t ModuloN_vtaus[];
" >> moduloN_vtau.h

echo \
"
#endif /*MODULON_VTAU_H_*/
" >> moduloN_vtau.h
