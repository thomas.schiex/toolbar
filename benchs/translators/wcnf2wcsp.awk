
# Translate in wcsp format from a MAX-SAT problem in cnf format (with unit clause weights)
# convention: 1: true , 0: false

# See the file cnf_format.ps for a description of the cnf format

# Warning! Each clause must be defined on a single line with a zero at the end

# Preprocessing:
# - Tautologies ("x and not(x)") are removed
# - Duplicate literals are simplified ("x and x" replaced by "x")

# Remark: some variables may be not involved in any clauses.

# Usage: awk -f cnf2wcsp.awk file.cnf > file.wcsp

BEGIN {
    ok = 0;
    wcsp = "";
}

ok && !/^c/ && NF >= 2 && $NF == 0 {
    delete vars;
    removed = 0;
    scope = "";
    tuple = "";
    arity = 0;
    weight=$1;
    for (i=2; i<NF; i++) {
	if ($i >= 1) {
	    if ($i in vars) {
		if (vars[$i] == -1) {
		    removed = 1; # remove a tautology
		} # else do not duplicate the variable
	    } else {
		vars[$i] = 1;
		arity++;
		scope = scope " " ($i - 1);
		tuple = tuple "" 0 " ";
	    }
	} else {
	    if ((- $i) in vars) {
		if (vars[(- $i)] == 1) {
		    removed = 1; # remove a tautology
		} # else do not duplicate the variable
	    } else {
		vars[(- $i)] = -1;
		arity++;
		scope = scope " " (- $i - 1);
		tuple = tuple "" 1 " ";
	    }
	}
    }
    if (!removed && arity > 0) {
	nbconstr++;
	wcsp = wcsp "" arity "" scope " 0 1\n" tuple "" weight "\n";
    }
}

/^p/ {
    nbvar = $3;
    nbclause = $4;
    ok = 1;
}

END {
    print FILENAME,nbvar,2,nbconstr,2147483647;
    printf("2");
    for (i=1; i<nbvar; i++) {
	printf(" 2");
    }
    print "";
    printf("%s", wcsp);
}

